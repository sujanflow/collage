//
//  AdManager.h
//  Xmas Photo Sticker Booth
//
//  Edited by shourav on Dec 4,2015.
//  Copyright (c) 2015 SHOURAV. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <GoogleMobileAds/GADInterstitial.h>
#import <GoogleMobileAds/GADInterstitialDelegate.h>
#import <GoogleMobileAds/GADBannerView.h>
/*
#import <Chartboost/Chartboost.h>
#import <Chartboost/CBNewsfeed.h>
#import <Chartboost/CBAnalytics.h>
#import <StoreKit/StoreKit.h>
*/

#define admob_interstitial @"ca-app-pub-8340924500267413/6310811383"
#define admob_bottom_banner @"ca-app-pub-8340924500267413/5572444788"


//#define FlurryKey @"648N2HF3DH6S8WGCXM2Z"


@interface AdManager : NSObject<GADInterstitialDelegate>//,ChartboostDelegate>

+ (id)sharedInstance;
-(void)showAdmobFullscreen;
-(void)showAdmobSplahAd;
-(GADBannerView*)adMobBannerWithAdUnitID:(NSString*)adUnitID andOrigin:(CGPoint)origin;
- (GADRequest *)adMobrequest;
/*
-(void)initializeChartboost; 
- (void)showChartboostInterstitial;
- (void)showChartboostMoreApps;
- (void)cacheChartboostInterstitial;
- (void)cacheChartboostMoreApps;
- (void)showChartboostNewsfeed;
- (void)cacheChartboostRewardedVideo;
- (void)showChartboostRewardedVideo;
- (void)showChartboostNotificationUI;
- (void)showChartboostSupport:(id)sender;
- (void)trackChartboostnAppPurchase:(NSData *)transactionReceipt product:(SKProduct *)product;
*/

@property(nonatomic, strong) GADInterstitial *interstitial;
@end
