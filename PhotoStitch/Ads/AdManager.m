//
//  AdManager.m
//  Xmas Photo Sticker Booth
//
//  Edited by shourav on Dec 4,2015.
//  Copyright (c) 2015 SHOURAV. All rights reserved.
//


#import "AdManager.h"
#import "MainMenuAppDelegate.h"

@interface AdManager ()
@end

@implementation AdManager



- (GADRequest *)adMobrequest
{
    GADRequest *request = [GADRequest request];
    
    request.testDevices = @[
                            
                            @"ca-app-pub-8340924500267413/6310811383"
                            @"ca-app-pub-8340924500267413/5572444788"
                            ];
    return request;
}

-(void)showAdmobFullscreen{
    self.interstitial = [GADInterstitial alloc];
    self.interstitial.delegate = self;
    self.interstitial.adUnitID = admob_interstitial;
    [self.interstitial loadRequest:[self adMobrequest]];
//    [self.interstitial presentFromRootViewController:[self topViewController]];
}

-(void)showAdmobSplahAd{
    GADInterstitial *splashInterstitial_ = [[GADInterstitial alloc] init];
    splashInterstitial_.adUnitID = admob_interstitial;
    [splashInterstitial_ loadRequest:[self adMobrequest]];
}

-(GADBannerView*)adMobBannerWithAdUnitID:(NSString*)adUnitID andOrigin:(CGPoint)origin
{
    
    
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?kGADAdSizeLeaderboard:kGADAdSizeBanner origin:origin];
    
    bannerView.adUnitID = adUnitID;
    NSString *sourceString = [[NSThread callStackSymbols] objectAtIndex:1];
    
    NSCharacterSet *separatorSet = [NSCharacterSet characterSetWithCharactersInString:@" -[]+?.,"];
    NSMutableArray *array = [NSMutableArray arrayWithArray:[sourceString  componentsSeparatedByCharactersInSet:separatorSet]];
    [array removeObject:@""];
    
    bannerView.rootViewController = [array objectAtIndex:3];
    
    [bannerView loadRequest:[self adMobrequest]];
    
    return bannerView;
}

#pragma mark GADInterstitialDelegate implementation

- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
    
    if ([self topViewController].isBeingPresented || [self topViewController].isBeingDismissed) {
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self interstitialDidReceiveAd:interstitial];
        });
    }
    else {
        [self.interstitial presentFromRootViewController:[self topViewController]];
    }
    
}


- (void)interstitial:(GADInterstitial *)interstitial
didFailToReceiveAdWithError:(GADRequestError *)error {
    
    
    
}


+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    
    return _sharedObject;
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

@end
