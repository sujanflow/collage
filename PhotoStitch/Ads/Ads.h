//
//  Ads.h
//  piceditor7
//
//  Created by Kevin Chee on 25/03/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALSdk.h"

@interface Ads : NSObject

// AppLovin
+ (void)appLovinInit;
+ (void)appLovinInterstitialRequest;

@end
