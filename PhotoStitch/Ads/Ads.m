//
//  Ads.m
//  piceditor7
//
//  Created by Kevin Chee on 25/03/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import "Ads.h"
#import "ALInterstitialAd.h"

@implementation Ads

/**
 ** appDelegate:applicationDidFinishLaunching - Call appLovinInit
 ** Go to 'Build Settings', search for 'Other Linker Flags' and add -all_load -ObjC
 ** Launch interstitial - Call appLovinInterstitialRequest
 */
#pragma mark AppLovin
+ (void)appLovinInit
{
    [ALSdk initializeSdk];
}

+ (void)appLovinInterstitialRequest
{
    [ALInterstitialAd showOver:[[UIApplication sharedApplication] keyWindow]];
}

@end
