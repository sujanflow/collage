//
//  AdsManager.h
//
//  Edited by shourav on Dec 4,2015.
//  Copyright (c) 2015 SHOURAV. All rights reserved.
//

#import <Foundation/Foundation.h>

//#import "cocos2d.h"


#define ADS_MANAGER [AdsManager sharedManager]


@interface AdsManager : NSObject  {
    
    
    BOOL revmobBannerShowedInsteadMobclix;
    
        

}

@property (nonatomic) BOOL adsEnabled;
@property (nonatomic) BOOL fullscreenAdsCanBeShowed;
@property (nonatomic) BOOL bannerCanBeShowed;

+ (AdsManager*)sharedManager;

- (void)showChartboostInterstitial;
- (void)showChartboostMoreApps;

- (void)showMobclixBanner;
- (void)hideMobclixBanner;
- (void)showMobclixFullscreen;

- (void)showRevmobFullScreen;
- (void)showRevmobBanner;
- (void)hideRevmobBanner;



@end
