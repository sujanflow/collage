//
//  AdsManager.m
//
//  Edited by shourav on Dec 4,2015.
//  Copyright (c) 2015 SHOURAV. All rights reserved.
//
//  version 1.3
//  - added TestFlight
//  - added Flurry
//  - added MobclixFullScreenAdViewController

#import "AdsManager.h"
#import "MainMenuAppDelegate.h"

#define ROOT_VIEW_CONTROLLER            (CCDirectorIOS*)[CCDirector sharedDirector]

#define IS_ADS_ENABLED_DEFAULT          YES
#define IS_CHARTBOOST                   YES
#define IS_CHARTBOOST_MOREAPPS          YES
#define IS_MOBCLIX                      YES
#define IS_MOBCLIX_FULLSCREEN           NO
#define IS_MOBCLIX_BANNER               NO
#define IS_REVMOB                       YES
#define IS_REVMOB_BANNER                NO
#define IS_REVMOB_INSTEAD_MOBCLIX       NO
#define IS_TAPJOY                       YES
#define IS_LOCAL_NOTIFICATIONS          YES
#define IS_TESTFLIGHT                   YES
#define IS_FLURRY                       YES

#if PAID_VERSION

    #define FLURRY_ID                       @"X4VZZ55KY3FPN434RF65"
    #define REVMOB_APP_ID                   @""
    #define CHARTBOOST_APP_ID               @""
    #define CHARTBOOST_APP_SIGNATURE        @""
    #define MOBCLIX_APP_ID                  @""
    #define TAPJOY_APP_ID                   @""
    #define TAPJOY_SECRET_KEY               @""
    #define TESTFLIGHT_TEAM_KEY             @""

#else

    #define FLURRY_ID                       @"X4VZZ55KY3FPN434RF65"
    #define REVMOB_APP_ID                   @""
    #define CHARTBOOST_APP_ID               @""
    #define CHARTBOOST_APP_SIGNATURE        @""
    #define MOBCLIX_APP_ID                  @""
    #define TAPJOY_APP_ID                   @""
    #define TAPJOY_SECRET_KEY               @""
    #define TESTFLIGHT_TEAM_KEY             @""


#endif

// PUSHWOOSH: In your Info.plist add the following key Pushwoosh_APPID with your Pushwoosh Application ID string value

#define BANNER_RECT_IPHONE              CGRectMake([CCDirector sharedDirector].winSize.width-320, [CCDirector sharedDirector].winSize.height-50, 320, 50)
#define BANNER_RECT_IPAD                CGRectMake([CCDirector sharedDirector].winSize.width-768, [CCDirector sharedDirector].winSize.height-90, 768, 90)
#define REVMOB_ORIENTATIONS             UIInterfaceOrientationLandscapeLeft, UIInterfaceOrientationLandscapeRight, nil

static AdsManager* _sharedManager=nil;

@interface AdsManager(private)
@end

@implementation AdsManager

@synthesize adsEnabled, fullscreenAdsCanBeShowed, bannerCanBeShowed;

+ (AdsManager*)sharedManager {
    if (!_sharedManager) {
        _sharedManager = [[AdsManager alloc] init];
    }
    return _sharedManager;
}

- (id)init
{
   
    
    if ((self=[super init])) {
        
        [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:IS_ADS_ENABLED_DEFAULT] forKey:@"adsEnabled"]];
        adsEnabled = [[[NSUserDefaults standardUserDefaults] valueForKey:@"adsEnabled"] boolValue];
        
        
        fullscreenAdsCanBeShowed = YES;
        bannerCanBeShowed = YES;
        revmobBannerShowedInsteadMobclix = YES;
        
        
       
        [self scheduleNotification];
        [self initFlurry];
    }
    return self;
}

- (void)dealloc
{
    
    //[super dealloc];
}

- (void)setAdsEnabled:(BOOL)adsEnabled_
{
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:adsEnabled_] forKey:@"adsEnabled"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    adsEnabled = adsEnabled_;
}

- (BOOL)canShowFullscreenAd
{
    return adsEnabled && fullscreenAdsCanBeShowed;
}

- (BOOL)canShowBanner
{
    return adsEnabled && bannerCanBeShowed;
}




#pragma mark CHARTBOOST 



// Called before requesting an interestitial from the back-end
- (BOOL)shouldRequestInterstitial:(NSString *)location
{
    return [self canShowFullscreenAd];
}

// Called when an interstitial has been received, before it is presented on screen
// Return NO if showing an interstitial is currently innapropriate, for example if the user has entered the main game mode.
- (BOOL)shouldDisplayInterstitial:(NSString *)location
{
    return [self canShowFullscreenAd];
}

// Called when an interstitial has been received and cached.
- (void)didCacheInterstitial:(NSString *)location
{
    if([self canShowFullscreenAd]) {
       
    }
}

// Called when an interstitial has failed to come back from the server
- (void)didFailToLoadInterstitial:(NSString *)location
{
    NSLog(@"Chartboost didFailToLoadInterstitial");
}

- (void)showChartboostInterstitial
{
    if([self canShowFullscreenAd]) {
        
    }
}

- (void)showChartboostMoreApps
{
    if(IS_CHARTBOOST_MOREAPPS) {
      
    }
}

//#pragma mark MOBCLIX
//
//- (void)initMobclix
//{
//    if(IS_MOBCLIX) {
//        
//        if(adsEnabled) {
//
//            [Mobclix startWithApplicationId:MOBCLIX_APP_ID];
//            
//            if(IS_MOBCLIX_FULLSCREEN) {
//                mobclixFullScreenAdViewController = [[MobclixFullScreenAdViewController alloc] init];
//                mobclixFullScreenAdViewController.delegate = self;
//                [mobclixFullScreenAdViewController requestAd];
//            }
//            
//            if(IS_MOBCLIX_BANNER) {
//                mobclixAdView = IS_IPHONE
//                        ? [[MobclixAdViewiPhone_320x50 alloc] initWithFrame:BANNER_RECT_IPHONE]
//                        : [[MobclixAdViewiPad_728x90 alloc] initWithFrame:BANNER_RECT_IPAD];
//                mobclixAdView.delegate = self;
//                [mobclixAdView setHidden:YES];
//                [[ROOT_VIEW_CONTROLLER view] addSubview:mobclixAdView];
//            }
//        }
//    }
//}
//
//- (void)adViewDidFinishLoad:(MobclixAdView*)adView
//{
//    [self showMobclixBanner];
//}
//
//- (void)adView:(MobclixAdView*)adView didFailLoadWithError:(NSError*)error
//{
//    NSLog(@"Mobclix didFailLoadWithError:%@", [error localizedDescription]);
//    [adView cancelAd];
//    
//    if(IS_REVMOB_BANNER && IS_REVMOB_INSTEAD_MOBCLIX && !revmobBannerShowedInsteadMobclix) {
//        revmobBannerShowedInsteadMobclix = YES;
//        [self hideMobclixBanner];
//        [self showRevmobBanner];
//    }
//}
//
//- (void)fullScreenAdViewController:(MobclixFullScreenAdViewController*)fullScreenAdViewController didFailToLoadWithError:(NSError*)error
//{
//    NSLog(@"Mobclix fullScreenAdViewController:%@", [error localizedDescription]);
//}
//
//- (void)showMobclixBanner
//{
//    if([self canShowBanner]) {
//        [mobclixAdView resumeAdAutoRefresh];
//        [mobclixAdView setHidden:NO];
//    }
//}
//
//- (void)hideMobclixBanner
//{
//    [mobclixAdView setHidden:YES];
//    [mobclixAdView pauseAdAutoRefresh];
//}
//
//- (void)showMobclixFullscreen
//{
//    if(IS_MOBCLIX_FULLSCREEN && [self canShowFullscreenAd]) {
//        if([mobclixFullScreenAdViewController hasAd]) {
//            [mobclixFullScreenAdViewController displayRequestedAdFromViewController:ROOT_VIEW_CONTROLLER];
//        }
//    }
//}

//#pragma mark REVMOB
//
//- (void)initRevMob
//{
//    if(IS_REVMOB) {
//        [RevMobAds startSessionWithAppID:REVMOB_APP_ID];
//        [RevMobAds session].userInterests = @[@"games", @"racing", @"car", @"action", @"adventure"];
//        //See more at: http://www.imcaesar.com/how-to-increase-your-app-ecpm-using-data-targeted-ads/#sthash.bQ1Vm4o5.dpuf
//
////        [[RevMobAds revMobAds] initWithAppId:REVMOB_APP_ID delagate:self];
//    }
//}
//
//- (void)showRevmobFullScreen
//{
//    if([self canShowFullscreenAd]) {
//        [[RevMobAds session] showFullscreen];
//    }
//}
//
//- (void)showRevmobBanner
//{
//    if([self canShowBanner]) {
//        [[RevMobAds session] showBanner];
////        [[RevMobAds session] showBannerAdWithFrame:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) ? BANNER_RECT_IPHONE : BANNER_RECT_IPAD withDelegate:self withSpecificOrientations:REVMOB_ORIENTATIONS];
//    }
//}
//
//- (void)hideRevmobBanner
//{
//    [[RevMobAds session] hideBanner];
//}
//
//- (void)revmobAdDidFailWithError:(NSError *)error
//{
//    NSLog(@"RevMob revmobAdDidFailWithError:%@", [error localizedDescription]);
//}
//
//#pragma mark TAPJOY
//
//- (void)initTapjoy
//{
//    if(IS_TAPJOY) {
//        [TapjoyConnect requestTapjoyConnect:TAPJOY_APP_ID secretKey:TAPJOY_SECRET_KEY];
//    }
//}

#pragma mark LOCAL_NOTIFICATIONS

- (void)localNotification:(NSDate*)date text:(NSString*)text
{
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    notif.fireDate = date;
    notif.timeZone = [NSTimeZone defaultTimeZone];
    notif.alertBody = text;
    notif.alertAction = @"Neon Chase";
    notif.soundName = UILocalNotificationDefaultSoundName;
    notif.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    //[notif release];
}

- (void)scheduleNotification 
{
    if(IS_LOCAL_NOTIFICATIONS) {
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        CGFloat DAY = 60.0f*60.0f*24.0f;
        
        [self localNotification:[NSDate dateWithTimeInterval:DAY*3 sinceDate:[NSDate date]] text:@"You're missing out on Animal face cam, Jump back in and Roar!"];
        
        [self localNotification:[NSDate dateWithTimeInterval:DAY*5 sinceDate:[NSDate date]] text:@"Quick race the buddies in Formula 1 racing!"];
        
        [self localNotification:[NSDate dateWithTimeInterval:DAY*7 sinceDate:[NSDate date]] text:@"It's time to dodge other players, blow up some cars and go crazy on Formula 1 racing!"];
    }
}

#pragma mark TESTFLIGHT

/*
- (void)initTestflight
{
    if(IS_TESTFLIGHT) {
        [TestFlight takeOff:TESTFLIGHT_TEAM_KEY];
    }
}
*/

#pragma mark FLURRY

- (void)initFlurry
{
    if(IS_FLURRY) {
       
    }
}






@end
