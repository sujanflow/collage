//
//  CropViewController.h
//  pb2free
//
//  Created by Kevin Chee on 26/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "POP.h"

#import <GoogleMobileAds/GADBannerView.h>
#import "AdManager.h"



@interface CollageCropViewController : UIViewController
<UIGestureRecognizerDelegate, ADBannerViewDelegate, POPAnimationDelegate,GADBannerViewDelegate>
{
    NSInteger collageTag;
    NSInteger photoTag;
    NSMutableArray *scrolls;
    NSMutableArray *photos;
    NSMutableArray *separators;
    NSMutableArray *collages;
    NSMutableArray *originals;
    NSMutableDictionary *framesModify;
    NSDictionary *collageCropDictionary;
    UIImageView *photoStitch;
    BOOL separatorActive;
    
}

@property (weak, nonatomic) IBOutlet UIView *imageFrameView;

@property (nonatomic, retain) IBOutlet UIView *bar;
@property (nonatomic, retain) IBOutlet UIView *bottomBar;
@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet UILabel *cropTitle;
@property (nonatomic, retain) IBOutlet UIButton *separatorButton;
@property (nonatomic, retain) IBOutlet UIImageView *barBottom;
@property (nonatomic, retain) IBOutlet UIImageView *collageCanvas;
@property (nonatomic, retain) IBOutlet UISlider *radiusSlider;
@property (nonatomic, retain) IBOutlet UIView *borderCanvas;
@property (nonatomic, retain) IBOutlet UISlider *borderSlider;
@property (nonatomic, retain) IBOutlet ADBannerView *banner;
@property (nonatomic, retain) IBOutlet UIScrollView *colours;
@property (nonatomic, retain) IBOutlet UIScrollView *backgrounds;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, retain) IBOutlet UIButton *recover;
@property (nonatomic, retain) IBOutlet UIImageView *collageBG;
@property (weak, nonatomic) IBOutlet UIButton *activateBackgroundsOutlet;
@property (weak, nonatomic) IBOutlet UIButton *activateRadiusOutlet;
@property (weak, nonatomic) IBOutlet UIButton *activateBorderOutlet;
@property (weak, nonatomic) IBOutlet UIButton *activateColoursOutlet;

@property(nonatomic, strong) IBOutlet GADBannerView *bannerCropView;

#pragma IBAction
- (IBAction)goEdit:(id)sender;
- (IBAction)backFromAlbum:(UIStoryboardSegue *)segue;
- (IBAction)activateSeparators:(id)sender;
- (IBAction)activateRadius:(id)sender;
- (IBAction)radiusChange:(id)sender;
- (IBAction)activateBorder:(id)sender;
- (IBAction)borderChange:(id)sender;
- (IBAction)activateColours:(id)sender;
- (IBAction)activateBackgrounds:(id)sender;
- (IBAction)recoverScreen:(id)sender;


#pragma Public
- (void)gimmeCollageNumber:(NSInteger)collage;
- (void)purchaseSuccess;
- (void)purchaseFail;
+ (CollageCropViewController *)sharedLayer;

@end
