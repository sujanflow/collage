//
//  CropViewController.m
//  pb2free
//
//  Created by Kevin Chee on 26/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "CollageCropViewController.h"
#import "CollageAlbumViewController.h"
#import "EditViewController.h"
#import "QuartzCore/Quartzcore.h"
#import "Languages.h"
#import "Collages.h"
#import "FontPack.h"
#import "IAPHelper.h"
@import GoogleMobileAds;


#define kToMenu             @"cropToMenu"
#define kToEdit             @"cropToEdit"
#define kToCollageAlbum     @"cropToCollageAlbum"

static CollageCropViewController *layer;

@interface CollageCropViewController ()<GADInterstitialDelegate>

@property(nonatomic, strong) GADInterstitial*interstitial;

@end

@implementation CollageCropViewController
@synthesize bannerCropView;

BOOL adBool = YES;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.interstitial = [[GADInterstitial alloc]
//                         initWithAdUnitID:@"ca-app-pub-3940256099942544/4411468910"];
//    GADRequest *request = [GADRequest request];
//    [self.interstitial loadRequest:request];
    
    self.interstitial = [self createAndLoadInterstitial];

    
    
    //Load Dictionary with wood name cross refference values for image name
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"ConfigPList" ofType:@"plist"];
    collageCropDictionary = [[NSDictionary alloc] initWithContentsOfFile:plistCatPath];
    [[IAPHelper sharedHelper] loadDictionary:collageCropDictionary];

    layer = nil;
    layer = self;
    
    [_indicator setHidesWhenStopped:YES];
    [_indicator stopAnimating];
    
    _banner.delegate = self;
    [_banner setFrame:CGRectMake(0.0, self.view.frame.size.height - _banner.frame.size.height - _bottomBar.frame.size.height,
                                 _banner.frame.size.width, _banner.frame.size.height)];
    
    [_cropTitle setFont:[UIFont fontWithName:@"Segoe Print" size:[Languages labelSizeDefault] + [Languages cropTitleSizeIncrease]]];
    [_label setFont:[UIFont fontWithName:@"Segoe Print" size:[Languages labelSizeDefault] + [Languages cropMessageSizeIncrease]]];

    photoStitch = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"main_logo.png"]];
    
    [photoStitch setFrame:CGRectMake(_collageCanvas.frame.size.width - (photoStitch.frame.size.width/2.0) - 5.0,
                                     _collageCanvas.frame.size.height - (photoStitch.frame.size.height/2.0) - 5.0,
                                     photoStitch.frame.size.width/2.0, photoStitch.frame.size.height/2.0)];
    [photoStitch setAlpha:0.6];
    [_collageCanvas addSubview:photoStitch];
    
    
    
    CGPoint origin;
    
    
//    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//
//        //is iPhone
//
//        if ([[UIScreen mainScreen] bounds].size.height == 568){
//            origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-50 - _bottomBar.frame.size.height);
//        }
//        else {
//
//            if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0){
//                origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2,[UIScreen mainScreen].bounds.size.height-50-_bottomBar.frame.size.height);
//                //device with Retina Display
//            }
//            else{origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2,[UIScreen mainScreen].bounds.size.height-50-_bottomBar.frame.size.height);
//                //no Retina Display
//
//            }
//        }
//    }else if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad) {

        //is iPad

//        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0){
//            //device with Retina Display
//            origin = CGPointMake(20,0);        }
//        else{
//            origin = CGPointMake(20,0);                //no Retina Display
//        }
//    }

    
    //setting for iphone x
    if (@available(iOS 11, *)) {
        
        UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
        if (insets.top > 0) {
            // We're running on an iPhone with a notch.
            origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-140);
        }else{
            
            origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-100);
            
        }
    }else{
        
        origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-100);
        
        
    }

    
    //    self.bannerCropView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:admob_bottom_banner andOrigin:origin];
    self.bannerCropView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:@"ca-app-pub-8340924500267413/5572444788" andOrigin:origin];
    self.bannerCropView.rootViewController = self;
    [self.view addSubview:self.bannerCropView];
    
    
//     [[AdManager sharedInstance] showAdmobFullscreen];
    
    [self hideAllFunctions];
    [self initCollages];
    [self initColours];
    [self initBackgrounds];
    [self addShadowToCanvas];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addShadowToCanvas
{
    _collageCanvas.layer.shadowColor = [UIColor blackColor].CGColor;
    _collageCanvas.layer.shadowOffset = CGSizeMake(0, 1);
    _collageCanvas.layer.shadowOpacity = 1;
    _collageCanvas.layer.shadowRadius = 3.0;
    _collageCanvas.clipsToBounds = NO;
    
}

- (void)hideAllFunctions
{
    [_radiusSlider setHidden:YES];
    [_borderCanvas setHidden:YES];
    [_borderSlider setHidden:YES];
    [_colours setHidden:YES];
    [_backgrounds setHidden:YES];
    [_recover setHidden:YES];
}

#pragma Algorithms
- (UIImage *)gimmeMyPictureImage
{
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0)
//    {
//        // Use Retina Display
//        UIGraphicsBeginImageContextWithOptions(self.view.frame.size, NO, 0.0);
//    }
//    else
//    {
//        UIGraphicsBeginImageContext(self.view.frame.size);
//    }
     UIGraphicsBeginImageContext(self.imageFrameView.frame.size);
    [self.imageFrameView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *photo = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return photo;
}

- (UIImage *)gimmeMyInstaPictureImage
{
//    int retina = 1;
//
//    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
//        [[UIScreen mainScreen] scale] == 2.0)
//    {
//        retina = 2;
//    }
//    else if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
//             [[UIScreen mainScreen] scale] == 3.0)
//    {
//        retina = 3;
//    }
    NSLog(@"[[UIScreen mainScreen] scale] %f",[[UIScreen mainScreen] scale]);
    
    UIImage *photo;
    
    //?????????????
//    CGRect cropRect = CGRectMake(_collageCanvas.frame.origin.x, _collageCanvas.frame.origin.y*retina, 320.0*retina, 320.0*retina);
    CGRect cropRect = CGRectMake(_collageCanvas.frame.origin.x, _collageCanvas.frame.origin.y, _collageCanvas.frame.size.width, _collageCanvas.frame.size.width);
    
     NSLog(@"canvas  %@  and cropRect %@", NSStringFromCGRect(_collageCanvas.frame), NSStringFromCGRect(cropRect));

    CGImageRef imageRef = CGImageCreateWithImageInRect([[self gimmeMyPictureImage] CGImage], cropRect);
    photo = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return photo;
}

#pragma IBAction functions
- (IBAction)goEdit:(id)sender
{
    for (UIView *separator in separators)
    {
        [separator setHidden:YES];
    }
    
    [self hideAllFunctions];
    
    POPSpringAnimation *topBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    topBarAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    topBarAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    topBarAnimation.springSpeed = 7;
    topBarAnimation.springBounciness = 15;
    [_bar.layer pop_addAnimation:topBarAnimation forKey:@"TopBarAnimationScale"];
    
    POPSpringAnimation *bottomBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    bottomBarAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    bottomBarAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    bottomBarAnimation.springSpeed = 7;
    bottomBarAnimation.springBounciness = 15;
    bottomBarAnimation.delegate = self;
    [_bottomBar.layer pop_addAnimation:bottomBarAnimation forKey:@"BottomBarAnimationScale"];
    
    NSLog(@"Collage Crop - Edit Pressed");
}

-(void)shiftForSpace:(BOOL)up
{
//    if (self.view.frame.size.height == 480)
//    {
        if (up)
        {
            // Move out
            POPSpringAnimation *topBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
            topBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bar.frame.origin.y - _bar.frame.size.height,
                                                                          _bar.frame.size.width, _bar.frame.size.height)];
            topBarAnimation.springSpeed = 1;
            topBarAnimation.springBounciness = 5;
            [_bar.layer pop_addAnimation:topBarAnimation forKey:@"TopBarAnimation"];
            
            POPSpringAnimation *bottomBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
            bottomBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bottomBar.frame.origin.y + (2*_bottomBar.frame.size.height),
                                                                             _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
            bottomBarAnimation.springSpeed = 1;
            bottomBarAnimation.springBounciness = 5;
            [_bottomBar.layer pop_addAnimation:bottomBarAnimation forKey:@"BottomBarAnimation"];
            
            POPSpringAnimation *collageAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
            collageAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, 0.0f,
                                                                           _collageCanvas.frame.size.width, _collageCanvas.frame.size.height)];
            collageAnimation.springSpeed = 1;
            collageAnimation.springBounciness = 5;
            [_collageCanvas.layer pop_addAnimation:collageAnimation forKey:@"CollageAnimation"];
            
            [_recover setHidden:NO];
            POPSpringAnimation *recoverScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
            recoverScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
            recoverScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
            recoverScaleAnimation.springSpeed = 1;
            recoverScaleAnimation.springBounciness = 8;
            [_recover.layer pop_addAnimation:recoverScaleAnimation forKey:@"RecoverScaleAnimation"];
        }
        else
        {
            // Move in
            POPSpringAnimation *topBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
            topBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bar.frame.origin.y + _bar.frame.size.height,
                                                                          _bar.frame.size.width, _bar.frame.size.height)];
            topBarAnimation.springSpeed = 1;
            topBarAnimation.springBounciness = 5;
            [_bar.layer pop_addAnimation:topBarAnimation forKey:@"TopBarAnimation"];
            
            POPSpringAnimation *bottomBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
            bottomBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bottomBar.frame.origin.y - (2*_bottomBar.frame.size.height),
                                                                             _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
            bottomBarAnimation.springSpeed = 1;
            bottomBarAnimation.springBounciness = 5;
            [_bottomBar.layer pop_addAnimation:bottomBarAnimation forKey:@"BottomBarAnimation"];
            
            POPSpringAnimation *collageAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
            collageAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, 43.0f,
                                                                           _collageCanvas.frame.size.width, _collageCanvas.frame.size.height)];
            collageAnimation.springSpeed = 1;
            collageAnimation.springBounciness = 5;
            [_collageCanvas.layer pop_addAnimation:collageAnimation forKey:@"CollageAnimation"];
            
            [self hideAllFunctions];
        }
    //}
}

- (IBAction)activateSeparators:(id)sender
{
    [self hideAllFunctions];
    if (!separatorActive)
    {
        // Activate
        for (UIImageView *photo in photos)
        {
            [photo setUserInteractionEnabled:NO];
        }
        
        for (UIScrollView *scroll in scrolls)
        {
            [scroll setUserInteractionEnabled:NO];
        }
        
        for (UIView *separator in separators)
        {
            [separator setHidden:NO];
        }
        
        _activateBackgroundsOutlet.enabled = NO;
        _activateRadiusOutlet.enabled = NO;
        _activateBorderOutlet.enabled = NO;
        _activateColoursOutlet.enabled = NO;
        
        separatorActive = YES;
    }
    else
    {
        // Deactivate
        for (UIImageView *photo in photos)
        {
            [photo setUserInteractionEnabled:YES];
        }
        
        for (UIScrollView *scroll in scrolls)
        {
            [scroll setUserInteractionEnabled:YES];
        }
        
        for (UIView *separator in separators)
        {
            [separator setHidden:YES];
        }
        
        
        _activateBackgroundsOutlet.enabled = YES;
        _activateRadiusOutlet.enabled = YES;
        _activateBorderOutlet.enabled = YES;
        _activateColoursOutlet.enabled = YES;
        
        
        separatorActive = NO;
    }
    
    adBool = YES;
    
}

- (IBAction)activateRadius:(id)sender
{
    adBool = YES;
    
    [self hideAllFunctions];
   // [self shiftForSpace:YES];
    [_radiusSlider setHidden:NO];
    
    POPSpringAnimation *pop = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    pop.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.5f, 0.5f)];
    pop.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    pop.springSpeed = 8;
    pop.springBounciness = 20;
    [_radiusSlider.layer pop_addAnimation:pop forKey:@"RadiusAnimationScale"];
}

- (IBAction)radiusChange:(id)sender
{
    UISlider *slider = (UISlider *)sender;
    float radius, value;
    
    for (UIScrollView *scroll in scrolls)
    {
        [scroll.layer setMasksToBounds:YES];
        if (scroll.frame.size.height >= scroll.frame.size.width)
        {
            value = scroll.frame.size.width;
        }
        else
        {
            value = scroll.frame.size.height;
        }
        radius = (value / 2.0) * slider.value;
        [scroll.layer setCornerRadius:radius];
    }
}

- (IBAction)activateBorder:(id)sender
{
    adBool = YES;
    
    [self hideAllFunctions];
    //[self shiftForSpace:YES];
    [_borderCanvas setHidden:NO];
    //[_borderSlider setHidden:NO];
    
    POPSpringAnimation *pop = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    pop.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.5f, 0.5f)];
    pop.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    pop.springSpeed = 8;
    pop.springBounciness = 20;
    [_borderCanvas.layer pop_addAnimation:pop forKey:@"ResizeAnimationScale"];
}

- (IBAction)borderChange:(id)sender
{
    UIButton *button = (UIButton *)sender;
    UIScrollView *scroll;
    CGRect rect;
    CGFloat shortest = self.view.frame.size.width, border = 0.0;
    
    for (int i = 0; i < [collages count]; i++)
    {
        rect = [[collages objectAtIndex:i] CGRectValue];
        if (shortest >= rect.size.height)
        {
            shortest = rect.size.height;
        }
        
        if (shortest >= rect.size.width)
        {
            shortest = rect.size.width;
        }
    }
    
    if (shortest > 100.0)
    {
        shortest = 100.0;
    }
    
    switch (button.tag)
    {
        case 1:
            border = 0.0;
            break;
        
        case 2:
            border = shortest * 0.1;
            break;
        
        case 3:
            border = shortest * 0.2;
            break;
            
        case 4:
            border = shortest * 0.3;
            break;
            
        default:
            break;
    }
    
    for (int i = 0; i < [collages count]; i++)
    {
        scroll = [scrolls objectAtIndex:i];
        rect = [[collages objectAtIndex:i] CGRectValue];
        [scroll setFrame:[self setCollageFrame:rect border:border]];
    }
    
    /*
    UISlider *slider = (UISlider *)sender;
    
    UIScrollView *scroll;
    CGRect rect;
    
    for (int i = 0; i < [collages count]; i++)
    {
        scroll = [scrolls objectAtIndex:i];
        rect = [[collages objectAtIndex:i] CGRectValue];
        [scroll setFrame:CGRectMake(rect.origin.x + slider.value, rect.origin.y + slider.value,
                                    rect.size.width - (slider.value * 2.0), rect.size.height - (slider.value * 2.0))];
    }
    [self radiusChange:_radiusSlider];
     */
}

- (void)readjustBorderSlider:(NSString *)type
{
    CGRect rect;
    CGFloat shortest = 0;
    int tag;
    
    if ([type isEqualToString:@"horizontal"])
    {
        for (int i = 0; i < [collages count]; i++)
        {
            rect = [[collages objectAtIndex:i] CGRectValue];
            if (shortest <= rect.size.height)
            {
                shortest = rect.size.height;
                tag = i;
            }
        }
    }
    else
    {
        for (int i = 0; i < [collages count]; i++)
        {
            rect = [[collages objectAtIndex:i] CGRectValue];
            if (shortest <= rect.size.width)
            {
                shortest = rect.size.width;
                tag = i;
            }
        }
    }
}

- (IBAction)activateColours:(id)sender
{
    adBool = YES;
    
    [self hideAllFunctions];
   // [self shiftForSpace:YES];
    [_colours setHidden:NO];
    
    POPSpringAnimation *pop = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    pop.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.5f, 0.5f)];
    pop.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    pop.springSpeed = 8;
    pop.springBounciness = 20;
    [_colours.layer pop_addAnimation:pop forKey:@"ColoursAnimationScale"];
}

- (IBAction)activateBackgrounds:(id)sender
{
    [self hideAllFunctions];
   // [self shiftForSpace:YES];
    [_backgrounds setHidden:NO];
    
    POPSpringAnimation *pop = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    pop.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.5f, 0.5f)];
    pop.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    pop.springSpeed = 8;
    pop.springBounciness = 20;
    [_backgrounds.layer pop_addAnimation:pop forKey:@"BackgroundAnimationScale"];
 
//    edited by Monir
//    if (adBool == YES){
//        adBool = NO;
//
//        [NSTimer scheduledTimerWithTimeInterval: 1.0
//                                         target: self
//                                       selector:@selector(showInterstitial)
//                                       userInfo: nil repeats:NO];
//
//    }
    
}


- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8340924500267413/6310811383"];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}


- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

- (void)showInterstitial{
    
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"Ad wasn't ready");
    }
    NSLog(@"showInterstitial");
}
//

- (void)purchaseMe:(id)sender
{
    NSString *key = [NSString stringWithFormat:@"%@.backgrounds", collageCropDictionary[@"BundleIdentifier"]];
    NSSet *iapSet = [NSSet setWithObjects:key, nil];
    
    [[IAPHelper sharedHelper] retrieveProduct:iapSet];
    [_indicator startAnimating];
}

- (IBAction)recoverScreen:(id)sender
{
    [self shiftForSpace:NO];
}

#pragma Segue functions
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kToEdit])
    {
        [segue.destinationViewController gimmeEditPhoto:[self gimmeMyInstaPictureImage]];
        
        NSLog(@"Collage Crop - Edit Photo set");
    }
}

#pragma Public functions
- (void)gimmeCollageNumber:(NSInteger)collage
{
    collageTag = collage;
}

- (IBAction)backFromAlbum:(UIStoryboardSegue *)segue
{
    UIImageView *photo = [photos objectAtIndex:photoTag];
    UIImage *image = [segue.sourceViewController selectedPhoto];
    
    [self setPhotoFrame:image photo:photo];
    [photo setImage:image];
    
    NSLog(@"Collage Crop - Photo initialised");

}

- (void)purchaseSuccess
{
    [_indicator stopAnimating];
    NSLog(@"Success");
}

- (void)purchaseFail
{
    [_indicator stopAnimating];
    NSLog(@"Fail");
}

+ (CollageCropViewController *)sharedLayer
{
    return layer;
}

#pragma Gesture
- (IBAction)separatorPan:(UIPanGestureRecognizer *)recognizer
{
    CGFloat max = 320.0, min = 0.0, pos;
    CGRect rect, frame;
    CGPoint translation = [recognizer translationInView:_collageCanvas];
    NSArray *framesEnlarge = [NSArray arrayWithArray:[framesModify objectForKey:[NSString stringWithFormat:@"i%zd", recognizer.view.tag]]];
    NSArray *framesReduce = [NSArray arrayWithArray:[framesModify objectForKey:[NSString stringWithFormat:@"d%zd", recognizer.view.tag]]];
    NSArray *separatorReduce = [NSArray arrayWithArray:[framesModify objectForKey:[NSString stringWithFormat:@"s%zd", recognizer.view.tag]]];
    NSMutableArray *framesToIncrease = [[NSMutableArray alloc] init];
    NSMutableArray *framesToDecrease = [[NSMutableArray alloc] init];
    NSMutableArray *allSeparators = [[NSMutableArray alloc] init];
    NSMutableArray *separatorsToIncrease = [[NSMutableArray alloc] init];
    NSMutableArray *separatorsToDecrease = [[NSMutableArray alloc] init];
    
    NSLog(@"%@", NSStringFromCGRect(recognizer.view.frame));
    for (id i in framesEnlarge)
    {
        [framesToIncrease addObject:[scrolls objectAtIndex:[i integerValue]]];
    }

    for (id i in framesReduce)
    {
        [framesToDecrease addObject:[scrolls objectAtIndex:[i integerValue]]];
    }

    for (id i in separatorReduce)
    {
        [allSeparators addObject:[separators objectAtIndex:[i integerValue]]];
    }

    if (recognizer.view.frame.size.width > recognizer.view.frame.size.height)
    {
        for (UIView *line in allSeparators)
        {
            if (line.frame.origin.y < recognizer.view.frame.origin.y)
            {
                [separatorsToDecrease addObject:line];
            }
            else
            {
                [separatorsToIncrease addObject:line];
            }
        }
    }
    else
    {
        for (UIView *line in allSeparators)
        {
            if (line.frame.origin.x < recognizer.view.frame.origin.x)
            {
                [separatorsToDecrease addObject:line];
            }
            else
            {
                [separatorsToIncrease addObject:line];
            }
        }
    }
    
    if (recognizer.view.frame.size.width > recognizer.view.frame.size.height)
    {
        for (id i in framesReduce)
        {
            frame = [[collages objectAtIndex:[i integerValue]] CGRectValue];
            pos = frame.origin.y;
            if (pos > min) { min = pos; }
        }
        for (id i in framesEnlarge)
        {
            frame = [[collages objectAtIndex:[i integerValue]] CGRectValue];
            pos = frame.origin.y + frame.size.height;
            if (pos < max) { max = pos; }
        }
    }
    else
    {
        for (id i in framesReduce)
        {
            frame = [[collages objectAtIndex:[i integerValue]] CGRectValue];
            pos = frame.origin.x;
            if (pos > min) { min = pos; }
        }
        for (id i in framesEnlarge)
        {
            frame = [[collages objectAtIndex:[i integerValue]] CGRectValue];
            pos = frame.origin.x + frame.size.width;
            if (pos < max) { max = pos; }
        }
    }
    
    min = min + 50.0;
    max = max - 50.0;
    
    if ((recognizer.view.frame.size.width > recognizer.view.frame.size.height) &&
        ((recognizer.view.center.y + translation.y) <= max && (recognizer.view.center.y + translation.y) >= min))
    {
        // Horizontal line
        recognizer.view.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);
        [recognizer setTranslation:CGPointMake(0, 0) inView:_collageCanvas];
        for (UIScrollView *scrollI in framesToIncrease)
        {
            [scrollI setFrame:CGRectMake(scrollI.frame.origin.x, scrollI.frame.origin.y + translation.y,
                                         scrollI.frame.size.width, scrollI.frame.size.height - translation.y)];
            rect = [[collages objectAtIndex:scrollI.tag] CGRectValue];
            rect.origin.y = rect.origin.y + translation.y;
            rect.size.height = rect.size.height - translation.y;
            [collages replaceObjectAtIndex:scrollI.tag withObject:[NSValue valueWithCGRect:rect]];
        }
        for (UIScrollView *scrollD in framesToDecrease)
        {
            [scrollD setFrame:CGRectMake(scrollD.frame.origin.x, scrollD.frame.origin.y,
                                         scrollD.frame.size.width, scrollD.frame.size.height + translation.y)];
            rect = [[collages objectAtIndex:scrollD.tag] CGRectValue];
            rect.size.height = rect.size.height + translation.y;
            [collages replaceObjectAtIndex:scrollD.tag withObject:[NSValue valueWithCGRect:rect]];
        }
        for (UIView *lineD in separatorsToDecrease)
        {
            [lineD setFrame:CGRectMake(lineD.frame.origin.x, lineD.frame.origin.y,
                                       lineD.frame.size.width, lineD.frame.size.height + translation.y)];
            [separators replaceObjectAtIndex:lineD.tag withObject:lineD];
            
            for (UIView *subview in lineD.subviews)
            {
                [subview setFrame:CGRectMake(subview.frame.origin.x, lineD.frame.size.height/2.0,
                                             subview.frame.size.width, subview.frame.size.height)];
            }
        }
        for (UIView *lineI in separatorsToIncrease)
        {
            [lineI setFrame:CGRectMake(lineI.frame.origin.x, lineI.frame.origin.y + translation.y,
                                       lineI.frame.size.width, lineI.frame.size.height - translation.y)];
            [separators replaceObjectAtIndex:lineI.tag withObject:lineI];
            
            for (UIView *subview in lineI.subviews)
            {
                [subview setFrame:CGRectMake(subview.frame.origin.x, lineI.frame.size.height/2.0,
                                             subview.frame.size.width, subview.frame.size.height)];
            }
            
            NSLog(@"recognizer: %@", NSStringFromCGRect(recognizer.view.frame));
            NSLog(@"line: %@", NSStringFromCGRect(lineI.frame));
        }
        
        [self readjustBorderSlider:@"horizontal"];
    }
    else if ((recognizer.view.frame.size.height > recognizer.view.frame.size.width) &&
             ((recognizer.view.center.x + translation.x) <= max && (recognizer.view.center.x + translation.x) >= min))
    {
        // Vertical line
        recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y);
        [recognizer setTranslation:CGPointMake(0, 0) inView:_collageCanvas];
        for (UIScrollView *scrollI in framesToIncrease)
        {
            [scrollI setFrame:CGRectMake(scrollI.frame.origin.x + translation.x, scrollI.frame.origin.y,
                                         scrollI.frame.size.width - translation.x, scrollI.frame.size.height)];
            rect = [[collages objectAtIndex:scrollI.tag] CGRectValue];
            rect.origin.x = rect.origin.x + translation.x;
            rect.size.width = rect.size.width - translation.x;
            [collages replaceObjectAtIndex:scrollI.tag withObject:[NSValue valueWithCGRect:rect]];
        }
        for (UIScrollView *scrollD in framesToDecrease)
        {
            [scrollD setFrame:CGRectMake(scrollD.frame.origin.x, scrollD.frame.origin.y,
                                         scrollD.frame.size.width + translation.x, scrollD.frame.size.height)];
            rect = [[collages objectAtIndex:scrollD.tag] CGRectValue];
            rect.size.width = rect.size.width + translation.x;
            [collages replaceObjectAtIndex:scrollD.tag withObject:[NSValue valueWithCGRect:rect]];
        }
        for (UIView *lineD in separatorsToDecrease)
        {
            [lineD setFrame:CGRectMake(lineD.frame.origin.x, lineD.frame.origin.y,
                                       lineD.frame.size.width + translation.x, lineD.frame.size.height)];
            [separators replaceObjectAtIndex:lineD.tag withObject:lineD];
            
            for (UIView *subview in lineD.subviews)
            {
                [subview setFrame:CGRectMake(lineD.frame.size.width/2.0, subview.frame.origin.y,
                                             subview.frame.size.width, subview.frame.size.height)];
            }
        }
        for (UIView *lineI in separatorsToIncrease)
        {
            [lineI setFrame:CGRectMake(lineI.frame.origin.x + translation.x, lineI.frame.origin.y,
                                       lineI.frame.size.width - translation.x, lineI.frame.size.height)];
            [separators replaceObjectAtIndex:lineI.tag withObject:lineI];
            
            for (UIView *subview in lineI.subviews)
            {
                [subview setFrame:CGRectMake(lineI.frame.size.width/2.0, subview.frame.origin.y,
                                             subview.frame.size.width, subview.frame.size.height)];
            }
            
            NSLog(@"recognizer: %@", NSStringFromCGRect(recognizer.view.frame));
            NSLog(@"line: %@", NSStringFromCGRect(lineI.frame));
        }
        
        [self readjustBorderSlider:@"vertical"];
    }
    
    [self radiusChange:_radiusSlider];
}

- (IBAction)panEm:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.view.superview];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view.superview];
}

- (IBAction)pinchEm:(UIPinchGestureRecognizer *)recognizer
{
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}

- (IBAction)rotateEm:(UIRotationGestureRecognizer *)recognizer
{
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
    recognizer.rotation = 0;
}

- (IBAction)tapEm:(UITapGestureRecognizer *)recognizer
{
    photoTag = [recognizer.view tag];
    UIScrollView *scroll = [scrolls objectAtIndex:photoTag];
    UIView *flash = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, scroll.frame.size.width, scroll.frame.size.height)];
    
    [flash setBackgroundColor:[UIColor whiteColor]];
    [scroll addSubview:flash];
    
    // Gives flash animation
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [flash setAlpha:0.5];
                     }completion:^(BOOL finished){
                         // Launch photo album
                         [self performSegueWithIdentifier:kToCollageAlbum sender:nil];

                         // Remove flash in case of obstruction
                         [flash removeFromSuperview];
                     }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([otherGestureRecognizer isMemberOfClass:[UIPanGestureRecognizer class]] && [gestureRecognizer isMemberOfClass:[UITapGestureRecognizer class]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isMemberOfClass:[UIPanGestureRecognizer class]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)giveGestures:(UIImageView *)photo
{
    UIPanGestureRecognizer *pan;
    UIPinchGestureRecognizer *pinch;
    UITapGestureRecognizer *tap;
    
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panEm:)];
    pan.delegate = self;
    [photo addGestureRecognizer:pan];
    
    pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchEm:)];
    pinch.delegate = self;
    [photo addGestureRecognizer:pinch];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEm:)];
    tap.delegate = self;
    [tap requireGestureRecognizerToFail:pan];
    [photo addGestureRecognizer:tap];
    
    NSLog(@"Collage Crop - Gesture added");
}

- (void)giveTapGesture:(UIScrollView *)scroll
{
    UITapGestureRecognizer *tap;
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEm:)];
    tap.delegate = self;
    [scroll addGestureRecognizer:tap];
}

#pragma Collages
- (void)initColours
{
    int i = 0;
    
    while (i <= [FontPack totalColour])
    {
        [_colours addSubview:[self createColourButton:[FontPack findColour:i] :i]];
        i++;
    }
    
    [_colours setContentSize:CGSizeMake(50.0*i, 50.0)];
}

- (UIButton *)createColourButton:(UIColor *)colour :(int)num
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(50.0*num, 0.0, 50.0, 50.0)];
    [button setBackgroundColor:colour];
    [button addTarget:self action:@selector(changeColour:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (IBAction)changeColour:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    [_collageCanvas setImage:nil];
    [_collageCanvas setBackgroundColor:button.backgroundColor];
}

- (void)initBackgrounds
{
    int i = 0;
    
    while (i < [(NSNumber *)collageCropDictionary[@"TotalCollageBackgrounds"] intValue])
    {
        [_backgrounds addSubview:[self createBackgroundButton:i]];
        i++;
    }
    
    [_backgrounds setContentSize:CGSizeMake(50.0*i, 50.0)];
}

- (UIButton *)createBackgroundButton:(int)num
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    NSString *index = [NSString stringWithFormat:@"background_%i",num];
    [button setFrame:CGRectMake(50.0*num, 0.0, 50.0, 50.0)];
    [button setBackgroundImage:[UIImage imageNamed:index] forState:UIControlStateNormal];
    button.tag = num;
    [button addTarget:self action:@selector(changeBackground:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (IBAction)changeBackground:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if (button.tag >= [(NSNumber *)collageCropDictionary[@"FreeCollageBackgrounds"] intValue])
    {
        // Paid Content
        NSString *key = [NSString stringWithFormat:@"%@.backgrounds", collageCropDictionary[@"BundleIdentifier"]];
        if (![[IAPHelper sharedHelper] isProductPurchased:key])
        {
            [self purchaseMe:nil];
        }
        else
        {
            [_collageCanvas setBackgroundColor:[UIColor clearColor]];
            [_collageCanvas setImage:[UIImage imageNamed:[NSString stringWithFormat:@"background_%zd", button.tag]]];
        }
    }
    else
    {
        // Free content
        [_collageCanvas setBackgroundColor:[UIColor clearColor]];
        [_collageCanvas setImage:[UIImage imageNamed:[NSString stringWithFormat:@"background_%zd", button.tag]]];
    }
}

- (void)initCollages
{
    separatorActive = NO;
    [self hideAllFunctions];
    scrolls = [[NSMutableArray alloc] init];
    photos = [[NSMutableArray alloc] init];
    separators = [[NSMutableArray alloc] init];
    collages = [[NSMutableArray alloc] init];
    originals = [[NSMutableArray alloc] init];
    framesModify = [[NSMutableDictionary alloc] init];

    collages = [Collages collageFrames:collageTag];
    originals = [Collages collageFrames:collageTag];
    NSMutableArray *lines = [Collages collageSeparators:collageTag];
    CGRect rect;
    
    for (int i = 0; i < [collages count]; i++)
    {
        rect = [[collages objectAtIndex:i] CGRectValue];
        [self setCollage:i x:rect.origin.x y:rect.origin.y width:rect.size.width height:rect.size.height];
        
        NSLog(@"setCollage  %f",rect.origin.x);
        
    }

    for (int j = 0; j < [lines count]; j++)
    {
        rect = [[lines objectAtIndex:j] CGRectValue];
        [self setSeparator:j x:rect.origin.x y:rect.origin.y width:rect.size.width height:rect.size.height];
    }
    
    framesModify = [Collages collageFrameModify:collageTag];
    
    NSLog(@"Collage Crop - Collage initialised");
}

- (CGRect )setCollageFrame:(CGRect)frame border:(CGFloat)border
{
    CGRect rect;
    CGFloat xAdd = border, yAdd = border, wDeduct = border * 2.0, hDeduct = border * 2.0;
    CGFloat x = frame.origin.x, y = frame.origin.y;
    CGFloat width = frame.size.width, height = frame.size.height;
    
    if (x == 0.0 && width == _collageCanvas.frame.size.width)
    {
        xAdd = border;
        wDeduct = border * 2.0;
        NSLog(@"width is equal");
    }
    else if (x == 0.0 && width != _collageCanvas.frame.size.width)
    {
//        xAdd = border;
//        wDeduct = border + (border/2.0);
        
        NSLog(@"width is not equal");
    }
    else if (x != 0.0 && (x+width) == _collageCanvas.frame.size.width)
    {
        xAdd = border/2.0;
        wDeduct = border + (border/2.0);
        NSLog(@"width is ......");
    }
    else if (x != 0.0 && (x+width) != _collageCanvas.frame.size.width)
    {
//        xAdd = border/2.0;
//        wDeduct = border;
        NSLog(@"width is not");
        
    }else if ((x == 0) && width <= _collageCanvas.frame.size.width){
        
        NSLog(@"width is less");
    }
    
    if (y == 0.0 && height == _collageCanvas.frame.size.height)
    {
        yAdd = border;
        hDeduct = border * 2.0;
        NSLog(@"height is ==");
    }
    else if (y == 0.0 && height != _collageCanvas.frame.size.height)
    {
        yAdd = border;
        hDeduct = border + border ;
        NSLog(@"height is !=");
    }
    else if (y != 0.0 && (y+height) == _collageCanvas.frame.size.height)
    {
        yAdd = border/2.0;
        hDeduct = border + (border/2.0);
        
        NSLog(@" y + height is ==");
    }
    else if (y != 0.0 && (y+height) != _collageCanvas.frame.size.height)
    {
        yAdd = border/2.0;
        hDeduct = border;
        NSLog(@" y + height is !=");
    }
    
    rect = CGRectMake(x + xAdd, y + yAdd, width - wDeduct, height - hDeduct);
    
    return rect;
}

- (void)setCollage:(int)tag x:(float)x y:(float)y width:(float)width height:(float)height
{
    UIScrollView *scroll = [[UIScrollView alloc] init];
    UIImageView *photo = [[UIImageView alloc] init];
    CGRect rect = CGRectMake(x, y, width, height);
    [scroll setFrame:[self setCollageFrame:rect border:10.0]];
    scroll.tag = tag;
    photo.tag = tag;
    
    [photo setUserInteractionEnabled:YES];
    [self giveGestures:photo];
    [scroll addSubview:photo];
    
    [self giveTapGesture:scroll];
    [scroll.layer setMasksToBounds:YES];
    [scroll setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:212.0/255.0 blue:64.0/255.0 alpha:1.0]];
    
    [_collageCanvas addSubview:scroll];
    [_collageCanvas bringSubviewToFront:photoStitch];
    
    // Add into arrays for future reference
    [scrolls addObject:scroll];
    [photos addObject:photo];
    
    NSLog(@"Collage Crop - Collage set");
}

- (void)setSeparator:(int)tag x:(float)x y:(float)y width:(float)width height:(float)height
{
    
    UIPanGestureRecognizer *pan;
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(separatorPan:)];
    pan.delegate = self;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [line setBackgroundColor:[UIColor clearColor]];
    [line setAlpha:0.8f];
    [line setOpaque:YES];
    [line setHidden:YES];
    [line addGestureRecognizer:pan];
    line.tag = tag;
    
    CGFloat radius;
    CGFloat circleX, circleY;
    if (width > height)
    {
        circleX = width/2.0;
        circleY = 0.0;
        radius = height;
    }
    else
    {
        circleX = 0.0;
        circleY = height/2.0;
        radius = width;
    }
    
    
    UIView *circleOne = [[UIView alloc] initWithFrame:CGRectMake(circleX, circleY, radius, radius)];
    circleOne.layer.cornerRadius = radius/2.0;
    circleOne.layer.masksToBounds = YES;
    [circleOne setBackgroundColor:[UIColor whiteColor]];
    [line addSubview:circleOne];

    radius = radius * 0.8;
    UIView *circleTwo = [[UIView alloc] initWithFrame:CGRectMake(circleX + 2, circleY + 2, radius, radius)];
    circleTwo.layer.cornerRadius = radius/2.0;
    circleTwo.layer.masksToBounds = YES;
    [circleTwo setBackgroundColor:[UIColor blackColor]];
    [line addSubview:circleTwo];
    
    [_collageCanvas addSubview:line];
    [separators addObject:line];
    
    NSLog(@"Collage Crop - Line set");
}

- (void)setPhotoFrame:(UIImage *)photoImage photo:(UIImageView *)photo
{
    CGRect frame;
    CGSize size;
    float width, height;
    UIScrollView *scroll = [scrolls objectAtIndex:photoTag];
    
    size = photoImage.size;
    NSLog(@"%@", NSStringFromCGSize(photoImage.size));
    if (size.width < size.height)
    {
        width = scroll.frame.size.width;
        height = (size.height * scroll.frame.size.width)/size.width;
        if (height < scroll.frame.size.height)
        {
            width = (size.width * scroll.frame.size.height)/size.height;
            height = scroll.frame.size.height;
        }
        frame = CGRectMake(0.0, 0.0, width, height);
    }
    else if (size.width > size.height)
    {
        width = (size.width * scroll.frame.size.height)/size.height;
        height = scroll.frame.size.height;
        if (width < scroll.frame.size.width)
        {
            width = scroll.frame.size.width;
            height = (size.height * scroll.frame.size.width)/size.width;
        }
        
        frame = CGRectMake(0.0, 0.0, width, height);
    }
    else
    {
        if (scroll.frame.size.width > scroll.frame.size.height)
        {
            frame = CGRectMake(0.0, 0.0, scroll.frame.size.width, scroll.frame.size.width);
        }
        else
        {
            frame = CGRectMake(0.0, 0.0, scroll.frame.size.height, scroll.frame.size.height);
        }
    }
    
    [photo setFrame:frame];
    
    NSLog(@"Collage Crop - Photo frame: %@", NSStringFromCGRect(frame));
}

#pragma mark POP Delegate
- (void)pop_animationDidStop:(POPAnimation *)anim finished:(BOOL)finished
{
    [self performSegueWithIdentifier:kToEdit sender:nil];
}
@end
