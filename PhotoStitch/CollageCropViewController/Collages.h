//
//  Collages.h
//  piceditor7
//
//  Created by Kevin Chee on 30/04/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Collages : NSObject

+ (NSMutableArray *)collageFrames:(NSInteger)tag;
+ (NSMutableArray *)collageSeparators:(NSInteger)tag;
+ (NSMutableDictionary *)collageFrameModify:(NSInteger)tag;

@end
