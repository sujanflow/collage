//
//  Collages.m
//  piceditor7
//
//  Created by Kevin Chee on 30/04/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import "Collages.h"
#import <UIKit/UIKit.h>

@implementation Collages



+ (NSMutableArray *)collageFrames:(NSInteger)tag
{
    NSMutableArray *frames = [[NSMutableArray alloc] init];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    switch (tag)
    {
            // First page, First row
        case 0:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, screenSize.width, screenSize.width)]];
            break;
            
        case 5:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, screenSize.width, screenSize.width/2)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, screenSize.width/2, screenSize.width, screenSize.width/2)]];
            break;
            
        case 10:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, screenSize.width, screenSize.width*.6688)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, screenSize.width*.6688, screenSize.width, screenSize.width*.33125)]];
            break;
            
        case 15:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, screenSize.width, screenSize.width*.33125)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, screenSize.width*.33125, screenSize.width, screenSize.width*.6688)]];
            break;
            
            
            
            
            // First page, Second row
        case 1:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, screenSize.width/2, screenSize.width)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(screenSize.width/2, 0.0, screenSize.width/2, screenSize.width)]];
            break;
            
        case 6:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 214.0, 320.0)]];
            break;
            
        case 11:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 320.0)]];
            break;
            
        case 16:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // First page, Third row
        case 2:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 320.0)]];
            break;
            
        case 7:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 12:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 320.0, 160.0)]];
            break;
            
        case 17:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 214.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 160.0, 214.0, 160.0)]];
            break;
            
            // First page, Fourth row
        case 3:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 214.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 320.0)]];
            break;
            
        case 8:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 160.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 106.0, 160.0, 214.0)]];
            break;
            
        case 13:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 320.0, 106.0)]];
            break;
            
        case 18:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 320.0)]];
            break;
            
            // First page, Fifth row
        case 4:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 320.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 320.0, 106.0)]];
            break;
            
        case 9:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 14:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
        case 19:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 320.0)]];
            break;
            
            // Second Page, First row
        case 20:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 320.0, 160.0)]];
            break;
            
        case 25:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 30:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 35:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 320.0)]];
            break;
            
            // Second Page, Second row
        case 21:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 320.0, 213.0)]];
            break;
            
        case 26:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 213.0, 106.0, 107.0)]];
            break;
            
        case 31:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 320.0)]];
            break;
            
        case 36:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 320.0, 80.0)]];
            break;
            
            // Second Page, Third row
        case 22:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 213.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 160.0, 107.0, 160.0)]];
            break;
            
        case 27:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 213.0, 160.0)]];
            
            break;
            
        case 32:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 213.0, 160.0, 107.0)]];
            break;
            
        case 37:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 213.0)]];
            break;
            
        case 23:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 213.0, 107.0, 107.0)]];
            break;
            
            // Second Page, Fourth row
        case 28:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 213.0, 107.0)]];
            break;
            
        case 33:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 213.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 213.0)]];
            break;
            
        case 38:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 213.0)]];
            break;
            
            // Second Page, Fifth row
        case 24:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 320.0)]];
            break;
            
        case 29:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 34:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 320.0, 80.0)]];
            break;
            
        case 39:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Third Page, First row
        case 40:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 320.0)]];
            break;
            
        case 45:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 214.0, 160.0)]];
            break;
            
        case 50:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 320.0, 106.0)]];
            break;
            
        case 55:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 160.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 106.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
            // Third Page, Second row
        case 41:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 213.0, 160.0)]];
            break;
            
        case 46:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 213.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 51:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 213.0)]];
            break;
            
        case 56:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 213.0, 106.0, 107.0)]];
            break;
            
            // Third Page, Third row
        case 42:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
        case 47:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 52:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 57:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Third Page, Fourth row
        case 43:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 213.0)]];
            break;
            
        case 48:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 213.0, 107.0)]];
            break;
            
        case 53:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 213.0)]];
            break;
            
        case 58:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 213.0, 106.0, 107.0)]];
            break;
            
            // Third Page, Fifth row
        case 44:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 320.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
        case 49:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 320.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
        case 54:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 320.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 213.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 59:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
            // Fourth Page, First row
        case 60:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 213.0)]];
            break;
            
        case 65:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 70:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 320.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 75:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 160.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
            // Fourth Page, Second row
        case 61:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 106.0, 213.0, 214.0)]];
            break;
            
        case 66:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 213.0, 160.0, 106.0)]];
            break;
            
        case 71:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 213.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 213.0, 107.0, 107.0)]];
            break;
            
        case 76:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 213.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 160.0, 106.0, 160.0)]];
            break;
            
            // Fourth Page, Third row
        case 62:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 213.0, 80.0, 107.0)]];
            break;
            
        case 67:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 320.0, 213.0)]];
            break;
            
        case 72:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 320.0)]];
            break;
            
        case 77:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 240.0, 107.0, 80.0)]];
            break;
            
            // Fourth Page, Fourth row
        case 63:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 68:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
        case 73:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 214.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 78:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
            // Fourth Page, Fifth row
        case 64:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 69:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 213.0)]];
            break;
            
        case 74:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 213.0)]];
            break;
            
        case 79:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 213.0, 106.0, 107.0)]];
            break;
            
            // Fifth Page, First row
        case 80:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 213.0, 106.0, 107.0)]];
            break;
            
        case 85:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 213.0)]];
            break;
            
        case 90:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 95:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
            // Fifth Page, Second row
        case 81:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 213.0)]];
            break;
            
        case 86:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
        case 91:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 213.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 96:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 213.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
            // Fifth Page, Third row
        case 82:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
        case 87:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 213.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 92:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
        case 97:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 213.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
            // Fifth Page, Fourth row
        case 83:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 214.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 320.0)]];
            break;
            
        case 88:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 320.0, 106.0)]];
            break;
            
        case 93:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 320.0)]];
            break;
            
        case 98:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 320.0, 106.0)]];
            break;
            
            // Fifth Page, Fifth row
        case 84:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 214.0, 214.0, 106.0)]];
            break;
            
        case 89:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 106.0, 106.0, 214.0)]];
            break;
            
        case 94:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 214.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 213.0, 107.0, 107.0)]];
            break;
            
        case 99:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 106.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 213.0, 107.0, 107.0)]];
            break;
            
            // Sixth Page, First row
        case 100:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(64.0, 213.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(128.0, 213.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(192.0, 213.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(256.0, 213.0, 64.0, 106.0)]];
            break;
            
        case 105:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(64.0, 0.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(128.0, 0.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(192.0, 0.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(256.0, 0.0, 64.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 320.0, 214.0)]];
            break;
            
        case 110:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 64.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 128.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 192.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 256.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 214.0, 320.0)]];
            break;
            
        case 115:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 64.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 128.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 192.0, 106.0, 64.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 256.0, 106.0, 64.0)]];
            break;
            
            // Sixth Page, Second row
        case 101:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 106:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 106.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 213.0)]];
            break;
            
        case 111:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 116:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Sixth Page, Third row
        case 102:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 107:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 112:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 117:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Sixth Page, Fourth row
        case 103:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 213.0)]];
            break;
            
        case 108:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 213.0, 80.0, 107.0)]];
            break;
            
        case 113:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
        case 118:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 213.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 240.0, 107.0, 80.0)]];
            break;
            
            // Sixth Page, Fifth row
        case 104:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 109:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 114:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
        case 119:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
            // Seventh Page, First row
        case 120:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 125:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 320.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 107.0)]];
            break;
            
        case 130:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 135:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
            // Seventh Page, Second row
        case 121:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 126:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 131:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 136:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 213.0)]];
            break;
            
            // Seventh Page, Third row
        case 122:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 213.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 127:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 213.0)]];
            break;
            
        case 132:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 137:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
            // Seventh Page, Fourth row
        case 123:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
        case 128:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 133:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 213.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 214.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 138:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 214.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 213.0, 106.0)]];
            break;
            
            // Seventh Page, Fifth row
        case 124:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 214.0, 80.0, 106.0)]];
            break;
            
        case 129:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 106.0, 214.0, 214.0)]];
            break;
            
        case 134:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 214.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 139:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 214.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 80.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 240.0, 106.0, 80.0)]];
            break;
            
            // Eighth Page, First row
        case 140:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 145:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 150:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 155:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Eighth Page, Second row
        case 141:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 146:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 151:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 156:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Eighth Page, Third row
        case 142:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 147:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 152:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 157:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Eighth Page, Fourth row
        case 143:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 148:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 153:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 158:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
            // Eighth Page, Fifth row
        case 144:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 320.0, 80.0)]];
            break;
            
        case 149:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 320.0)]];
            break;
            
        case 154:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 159:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
            // Ninth Page, First row
        case 160:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 165:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 170:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 175:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Ninth Page, Second row
        case 161:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 166:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 171:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 176:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Ninth Page, Third row
        case 162:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 167:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 172:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 177:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
            // Ninth Page, Fourth row
        case 163:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 168:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 173:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 178:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
            // Ninth Page, Fifth row
        case 164:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 213.0, 106.0, 107.0)]];
            break;
            
        case 169:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 106.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 213.0, 106.0, 107.0)]];
            break;
            
        case 174:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 240.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 179:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 240.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
            // Tenth Page, First row
        case 180:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 240.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 185:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 240.0, 240.0)]];
            break;
            
        case 190:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 320.0, 80.0)]];
            break;
            
        case 195:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
            // Tenth Page, Second row
        case 181:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 186:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 191:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 240.0)]];
            break;
            
        case 196:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
            // Tenth Page, Third row
        case 182:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 240.0)]];
            break;
            
        case 187:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 192:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 197:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 240.0)]];
            break;
            
            // Tenth Page, Fourth row
        case 183:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 240.0)]];
            break;
            
        case 188:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 214.0, 80.0, 106.0)]];
            break;
            
        case 193:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 106.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 106.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 106.0, 80.0, 214.0)]];
            break;
            
        case 198:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 106.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 106.0, 80.0, 214.0)]];
            break;
            
            // Tenth Page, Fifth row
        case 184:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 120.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 120.0, 80.0, 200.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 200.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 200.0, 80.0, 120.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 189:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 194:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 240.0, 80.0)]];
            break;
            
        case 199:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 240.0, 80.0)]];
            break;
            
            // Eleventh Page, First row
        case 200:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 80.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 240.0, 106.0, 80.0)]];
            break;
            
        case 205:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 80.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 160.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 240.0, 214.0, 80.0)]];
            break;
            
        case 210:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 80.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 240.0, 214.0, 80.0)]];
            break;
            
        case 215:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 120.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(120.0, 80.0, 200.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 200.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(200.0, 160.0, 120.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 240.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
            // Eleventh Page, Second row
        case 201:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 206:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 211:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 107.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 216:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 214.0, 160.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 214.0, 80.0, 107.0)]];
            break;
            
            // Eleventh Page, Third row
        case 202:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 80.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 214.0, 106.0, 106.0)]];
            break;
            
        case 207:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 107.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 80.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 240.0, 106.0, 80.0)]];
            break;
            
        case 212:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 214.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 217:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 214.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
            // Eleventh Page, Fourth row
        case 203:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 208:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 240.0, 160.0)]];
            break;
            
        case 213:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 218:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
            // Eleventh Page, Fifth row
        case 204:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 209:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 160.0)]];
            break;
            
        case 214:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 219:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
            // Twelth Page, First row
        case 220:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 225:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 160.0)]];
            break;
            
        case 230:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 160.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 160.0, 80.0)]];
            break;
            
        case 235:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 120.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(120.0, 80.0, 120.0, 240.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
            // Twelth Page, Second row
        case 221:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 240.0, 120.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 200.0, 240.0, 120.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 226:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 320.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 231:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 320.0, 160.0)]];
            break;
            
        case 236:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 320.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
            // Twelth Page, Third row
        case 222:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 227:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 320.0)]];
            break;
            
        case 232:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 160.0, 320.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        case 237:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 107.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
            // Twelth Page, Fourth row
        case 223:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 213.0, 106.0, 107.0)]];
            break;
            
        case 228:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 107.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 214.0, 160.0, 106.0)]];
            break;
            
        case 233:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 107.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 213.0, 80.0, 107.0)]];
            break;
            
        case 238:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 106.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 106.0, 106.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 213.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 213.0, 80.0, 107.0)]];
            break;
            
            // Twelth Page, Fifth row
        case 224:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 160.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 106.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 106.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 106.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 106.0, 80.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 213.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 213.0, 106.0, 107.0)]];
            break;
            
        case 229:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
        case 234:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 239:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 160.0, 106.0, 160.0)]];
            break;
            
            // Thirteenth Page, First row
        case 240:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 160.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 240.0, 107.0, 80.0)]];
            break;
            
        case 245:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 214.0, 107.0, 106.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 240.0, 107.0, 80.0)]];
            break;
            
        case 250:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 106.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 0.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 80.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 160.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 240.0, 107.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 0.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 107.0, 107.0, 107.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 214.0, 107.0, 106.0)]];
            break;
            
        case 255:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 160.0, 160.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
            // Thirteenth Page, Second row
        case 241:
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 0.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 80.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 160.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 240.0, 80.0, 80.0)]];
            [frames addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 240.0, 80.0, 80.0)]];
            break;
            
        default:
            break;
    }
    
    NSLog(@"Collage Crop - %ld collage frames built", (unsigned long)[frames count]);
    return frames;
}

+ (NSMutableArray *)collageSeparators:(NSInteger)tag
{
    NSMutableArray *separators = [[NSMutableArray alloc] init];
    
    switch (tag)
    {
            // First page, first row
        case 0:
            break;
            
        case 5:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 20.0)]];
            break;
            
        case 10:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            break;
            
        case 15:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 320.0, 15.0)]];
            break;
            
            // First page, Second row
        case 1:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 6:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 11:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 16:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 153.0, 160.0, 15.0)]];
            break;
            
            // First page, Third row
        case 2:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 7:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 153.0, 15.0, 160.0)]];
            break;
            
        case 12:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0, 153.0, 320.0, 15.0)]];
            break;
            
        case 17:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 153.0, 214.0, 15.0)]];
            break;
            
            // First page, Fourth row
        case 3:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 8:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 106.0, 15.0, 214.0)]];
            break;
            
        case 13:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            break;
            
        case 18:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            break;
            
            // First page, Fifth row
        case 4:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            break;
            
        case 9:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 160.0, 320.0, 15.0)]];
            break;
            
        case 14:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 207.0, 160.0, 15.0)]];
            break;
            
        case 19:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            break;
            
            // Second Page, First row
        case 20:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            break;
            
        case 25:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 30:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 207.0, 107.0, 15.0)]];
            break;
            
        case 35:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            break;
            
            // Second Page, Second row
        case 21:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            break;
            
        case 26:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 31:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 36:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            break;
            
            // Second Page, Third row
        case 22:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 27:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 32:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 206.0, 160.0, 15.0)]];
            break;
            
        case 37:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 100.0, 160.0, 15.0)]];
            break;
            
            // Second Page, Fourth row
        case 23:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 28:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 33:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 107.0, 15.0, 213.0)]];
            break;
            
        case 38:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 213.0)]];
            break;
            
            // Second Page, Fifth row
        case 24:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 29:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 34:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            break;
            
        case 39:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            break;
            
            // Third Page, First row
        case 40:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 207.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 45:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 214.0, 15.0)]];
            break;
            
        case 50:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            break;
            
        case 55:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 106.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 207.0, 160.0, 15.0)]];
            break;
            
            // Third Page, Second row
        case 41:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 213.0, 15.0)]];
            break;
            
        case 46:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 213.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 207.0, 107.0, 15.0)]];
            break;
            
        case 51:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 107.0, 15.0, 213.0)]];
            break;
            
        case 56:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 213.0, 15.0, 107.0)]];
            break;
            
            // Third Page, Third row
        case 42:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 207.0, 160.0, 15.0)]];
            break;
            
        case 47:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            break;
            
        case 52:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 57:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
            // Third Page, Fourth row
        case 43:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 213.0, 15.0)]];
            break;
            
        case 48:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 206.0, 213.0, 15.0)]];
            break;
            
        case 53:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 213.0)]];
            break;
            
        case 58:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 213.0, 15.0, 107.0)]];
            break;
            
            // Third Page, Fifth row
        case 44:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 49:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 54:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 59:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 153.0, 106.0, 15.0)]];
            break;
            
            // Fourth Page, First row
        case 60:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            break;
            
        case 65:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 70:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 75:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            break;
            
            // Fourth Page, Second row
        case 61:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 106.0, 15.0, 214.0)]];
            break;
            
        case 66:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 213.0, 15.0, 106.0)]];
            break;
            
        case 71:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 206.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 76:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 213.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 153.0, 106.0, 15.0)]];
            break;
            
            // Fourth Page, Third row
        case 62:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 67:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            break;
            
        case 72:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 77:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 233.0, 107.0, 15.0)]];
            break;
            
            // Fourth Page, Fourth row
        case 63:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 68:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(152.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            break;
            
        case 73:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 78:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Fourth Page, Fifth row
        case 64:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 69:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 213.0, 15.0)]];
            break;
            
        case 74:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            break;
            
        case 79:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 206.0, 106.0, 15.0)]];
            break;
            
            // Fifth Page, First row
        case 80:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 206.0, 106.0, 15.0)]];
            break;
            
        case 85:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            break;
            
        case 90:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 153.0, 106.0, 15.0)]];
            break;
            
        case 95:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 153.0, 106.0, 15.0)]];
            break;
            
            // Fifth Page, Second row
        case 81:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            break;
            
        case 86:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 91:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 96:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Fifth Page, Third row
        case 82:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 87:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 92:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 97:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 213.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 213.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Fifth Page, Fourth row
        case 83:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 88:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 214.0)]];
            break;
            
        case 93:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 98:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 214.0)]];
            break;
            
            // Fifth Page, Fifth row
        case 84:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 89:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 100.0, 15.0, 214.0)]];
            break;
            
        case 94:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 100.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 99:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 100.0, 15.0, 214.0)]];
            break;
            
            // Sixth Page, First row
        case 100:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(57.0, 213.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(121.0, 213.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(185.0, 213.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(249.0, 213.0, 15.0, 106.0)]];
            break;
            
        case 105:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(57.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(121.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(185.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(249.0, 0.0, 15.0, 106.0)]];
            break;
            
        case 110:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 57.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 121.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 185.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 249.0, 106.0, 15.0)]];
            break;
            
        case 115:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 57.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 121.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 185.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 249.0, 106.0, 15.0)]];
            break;
            
            // Sixth Page, Second row
        case 101:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 106:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 111:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 116:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            break;
            
            // Sixth Page, Third row
        case 102:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 107:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 112:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 117:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            break;
            
            // Sixth Page, Fourth row
        case 103:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(152.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 213.0)]];
            break;
            
        case 108:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(152.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 213.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 213.0)]];
            break;
            
        case 113:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 213.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 213.0, 15.0)]];
            break;
            
        case 118:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 213.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 213.0, 15.0)]];
            break;
            
            // Sixth Page, Fifth row
        case 104:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 109:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 160.0)]];
            break;
            
        case 114:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 207.0, 160.0, 15.0)]];
            break;
            
        case 119:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 160.0, 15.0)]];
            break;
            
            // Seventh Page, First row
        case 120:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 125:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 130:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            break;
            
        case 135:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            break;
            
            // Seventh Page, Second row
        case 121:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 126:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 131:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 136:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            break;
            
            // Seventh Page, Third row
        case 122:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 127:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            break;
            
        case 132:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 137:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Seventh Page, Fourth row
        case 123:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 128:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 133:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 138:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Seventh Page, Fifth row
        case 124:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 214.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 214.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 129:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 106.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 106.0)]];
            break;
            
        case 134:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 207.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 214.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 106.0, 15.0)]];
            break;
            
        case 139:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 73.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 233.0, 106.0, 15.0)]];
            break;
            
            // Eighth Page, First row
        case 140:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 145:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 150:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 155:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
            // Eighth Page, Second row
        case 141:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 146:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 151:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 156:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
            // Eighth Page, Third row
        case 142:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(154.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 147:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 152:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 157:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
            // Eighth Page, Fourth row
        case 143:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 148:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 153:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 158:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 207.0, 160.0, 15.0)]];
            break;
            
            // Eighth Page, Fifth row
        case 144:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            break;
            
        case 149:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 154:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 159:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
            // Ninth Page, First row
        case 160:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 165:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 170:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 175:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
            // Ninth Page, Second row
        case 161:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 166:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 171:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 176:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            break;
            
            // Ninth Page, Third row
        case 162:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 167:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 172:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 177:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
            // Ninth Page, Fourth row
        case 163:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 153.0, 106.0, 15.0)]];
            break;
            
        case 168:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 207.0, 107.0, 15.0)]];
            break;
            
        case 173:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 207.0, 107.0, 15.0)]];
            break;
            
        case 178:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Ninth Page, Fifth row
        case 164:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 107.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 169:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 174:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 179:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
            // Tenth Page, First row
        case 180:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 185:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 240.0, 15.0)]];
            break;
            
        case 190:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            break;
            
        case 195:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            break;
            
            // Tenth Page, Second row
        case 181:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 186:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 191:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 240.0)]];
            break;
            
        case 196:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 240.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
            // Tenth Page, Third row
        case 182:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 240.0)]];
            break;
            
        case 187:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 240.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 192:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 197:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            break;
            
            // Tenth Page, Fourth row
        case 183:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            break;
            
        case 188:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 207.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 207.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 207.0, 80.0, 15.0)]];
            break;
            
        case 193:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 99.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 99.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 99.0, 80.0, 15.0)]];
            break;
            
        case 198:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 99.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 207.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 99.0, 80.0, 15.0)]];
            break;
            
            // Tenth Page, Fifth row
        case 184:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 113.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 193.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 189:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 194:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 199:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            break;
            
            // Eleventh Page, First row
        case 200:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 205:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 210:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 215:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(113.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(193.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
            // Eleventh Page, Second row
        case 201:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 206:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 211:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 214.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 216:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 214.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 214.0, 15.0, 107.0)]];
            break;
            
            // Eleventh Page, Third row
        case 202:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 100.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 207.0, 106.0, 15.0)]];
            break;
            
        case 207:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 73.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 233.0, 106.0, 15.0)]];
            break;
            
        case 212:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 214.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 153.0, 106.0, 15.0)]];
            break;
            
        case 217:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 100.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 214.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Eleventh Page, Fourth row
        case 203:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 208:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 153.0, 240.0, 15.0)]];
            break;
            
        case 213:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            break;
            
        case 218:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            break;
            
            // Eleventh Page, Fifth row
        case 204:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 209:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            break;
            
        case 214:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 219:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
            // Twelth Page, First row
        case 220:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 225:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 160.0)]];
            break;
            
        case 230:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 160.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 160.0, 15.0)]];
            break;
            
        case 235:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(113.0, 80.0, 15.0, 240.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
            // Twelth Page, Second row
        case 221:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 193.0, 240.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 226:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 160.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
        case 231:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 80.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 320.0, 15.0)]];
            break;
            
        case 236:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 240.0, 15.0, 80.0)]];
            break;
            
            // Twelth Page, Third row
        case 222:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 227:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            break;
            
        case 232:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        case 237:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 214.0, 15.0, 106.0)]];
            break;
            
            // Twelth Page, Fourth row
        case 223:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 107.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 228:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 100.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 107.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 214.0, 15.0, 106.0)]];
            break;
            
        case 233:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 107.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 238:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 106.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 106.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 213.0, 15.0, 107.0)]];
            break;
            
            // Twelth Page, Fifth row
        case 224:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 106.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 99.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 106.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 106.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 106.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 206.0, 320.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 213.0, 15.0, 107.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 213.0, 15.0, 107.0)]];
            break;
            
        case 229:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 153.0, 160.0, 15.0)]];
            break;
            
        case 234:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 207.0, 107.0, 15.0)]];
            break;
            
        case 239:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(207.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(214.0, 153.0, 106.0, 15.0)]];
            break;
            
            // Thirteenth Page, First row
        case 240:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(100.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(107.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 233.0, 107.0, 15.0)]];
            break;
            
        case 245:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 100.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 207.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 233.0, 107.0, 15.0)]];
            break;
            
        case 250:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 106.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(99.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 73.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 153.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(106.0, 233.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(206.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 99.0, 107.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(213.0, 207.0, 107.0, 15.0)]];
            break;
            
        case 255:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 80.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 160.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 240.0, 15.0, 80.0)]];
            break;
            
            // Thirteenth Page, Second row
        case 241:
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(73.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(153.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(233.0, 0.0, 15.0, 320.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 73.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 153.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(80.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(160.0, 233.0, 80.0, 15.0)]];
            [separators addObject:[NSValue valueWithCGRect:CGRectMake(240.0, 233.0, 80.0, 15.0)]];
            break;
            
        default:
            break;
    }
    
    NSLog(@"Collage Crop - %zd collage separators built", [separators count]);
    return separators;
}

+ (NSMutableDictionary *)collageFrameModify:(NSInteger)tag
{
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
    
    switch (tag)
    {
            // First page, first row
        case 0:
            break;
            
        case 5:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            break;
            
        case 10:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            break;
            
        case 15:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            break;
            
            // First page, second row
        case 1:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            break;
            
        case 6:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            break;
            
        case 11:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            break;
            
        case 16:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            break;
            
            // First page, third row
        case 2:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
        case 7:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            break;
            
        case 12:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
        case 17:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            break;
            
            // First Page, Fourth row
        case 3:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
        case 8:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            break;
            
        case 13:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
        case 18:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            break;
            
            // First Page, Fifth row
        case 4:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            break;
            
        case 9:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            break;
            
        case 14:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 19:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1] ,[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            break;
            
            // Second Page
            // First row
        case 20:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1] ,[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            break;
            
        case 25:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 30:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 35:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1] ,[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            break;
            
            // Second row
        case 21:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1] ,[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            break;
            
        case 26:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 31:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            break;
            
        case 36:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            break;
            
            // Third row
        case 22:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 27:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 32:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 37:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
            // Fourth row
        case 23:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 28:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 33:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 38:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
            // Fifth row
        case 24:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
        case 29:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
        case 34:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
        case 39:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            break;
            
            // Third Page, First row
        case 40:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s2"];
            break;
            
        case 45:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            break;
            
        case 50:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            break;
            
        case 55:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s1"];
            break;
            
            // Third Page, Second row
        case 41:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 46:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            break;
            
        case 51:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 56:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            break;
            
            // Third Page, Third row
        case 42:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            break;
            
        case 47:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 52:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            break;
            
        case 57:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
            // Third Page, Fourth row
        case 43:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 48:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 53:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 58:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            break;
            
            // Third Page, Fifth row
        case 44:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 49:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 54:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 59:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
            // Fourth Page, First row
        case 60:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 65:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 70:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 75:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
            // Fourth Page, Second row
        case 61:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s3"];
            break;
            
        case 66:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 71:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
        case 76:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            break;
            
            // Fourth Page, Third row
        case 62:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], nil] forKey:@"s0"];
            break;
            
        case 67:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            break;
            
        case 72:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            break;
            
        case 77:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], nil] forKey:@"s0"];
            break;
            
            // Fourth Page, Fourth row
        case 63:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            break;
            
        case 68:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:4], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:2], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s4"];
            break;
            
        case 73:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3],  nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],  nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s2"];
            break;
            
        case 78:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Fourth Page, Fifth row
        case 64:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 69:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s4"];
            break;
            
        case 74:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 79:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Fifth Page, First row
        case 80:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 85:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 90:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 95:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Fifth Page, Second row
        case 81:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 86:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 91:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 96:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Fifth Page, Third row
        case 82:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 87:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 92:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 97:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Fifth Page, Fourth row
        case 83:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            break;
            
        case 88:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s3"];
            break;
            
        case 93:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            break;
            
        case 98:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s3"];
            break;
            
            // Fifth Page, Fifth row
        case 84:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            break;
            
        case 89:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s3"];
            break;
            
        case 94:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            break;
            
        case 99:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s3"];
            break;
            
            // Sixth Page, First row
        case 100:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s0"];
            break;
            
        case 105:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s0"];
            break;
            
        case 110:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s0"];
            break;
            
        case 115:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s0"];
            break;
            
            // Sixth Page, Second row
        case 101:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 106:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s0"];
            break;
            
        case 111:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            break;
            
        case 116:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            break;
            
            // Sixth Page, Third row
        case 102:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 107:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 112:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            break;
            
        case 117:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Sixth Page, Fourth row
        case 103:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 108:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 113:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 118:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
            // Sixth Page, Fifth row
        case 104:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 109:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 114:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 119:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
            // Seventh Page, First row
        case 120:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 125:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 130:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 135:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
            // Seventh Page, Second row
        case 121:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 126:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 131:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 136:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
            // Seventh Page, Third row
        case 122:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 127:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 132:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 137:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
            // Seventh Page, Fourth row
        case 123:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 128:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 133:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 138:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
            // Seventh Page, Fifth row
        case 124:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            break;
            
        case 129:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            break;
            
        case 134:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            break;
            
        case 139:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            break;
            
            // Eighth Page, First row
        case 140:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 145:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s5"];
            break;
            
        case 150:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
        case 155:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Eighth Page, Second row
        case 141:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4],nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 146:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s5"];
            break;
            
        case 151:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            break;
            
        case 156:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
            // Eighth Page, Third row
        case 142:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 147:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 152:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s4"];
            break;
            
        case 157:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s5"];
            break;
            
            // Eighth Page, Fourth row
        case 143:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 148:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 153:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 158:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
            // Eighth Page, Fifth row
        case 144:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s5"];
            break;
            
        case 149:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s5"];
            break;
            
        case 154:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                             [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            break;
            
        case 159:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            break;
            
            // Ninth Page, First row
        case 160:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 165:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 170:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:7], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s5"];
            break;
            
        case 175:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
            // Ninth Page, Second row
        case 161:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 166:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 171:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:7], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s5"];
            break;
            
        case 176:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s5"];
            break;
            
            // Ninth Page, Third row
        case 162:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s4"];
            break;
            
        case 167:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 172:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s3"];
            break;
            
        case 177:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
            // Ninth Page, Fourth row
        case 163:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            
            break;
            
        case 168:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s4"];
            break;
            
        case 173:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s4"];
            break;
            
        case 178:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
            // Ninth Page, Fifth row
        case 164:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s4"];
            break;
            
        case 169:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s4"];
            break;
            
        case 174:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                             [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            break;
            
        case 179:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5],
                             [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            break;
            
            // Tenth Page, First row
        case 180:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s4"];
            break;
            
        case 185:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s6"];
            break;
            
        case 190:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s6"];
            
            break;
            
        case 195:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
            // Tenth Page, Second row
        case 181:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s4"];
            break;
            
        case 186:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                             [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            break;
            
        case 191:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s6"];
            break;
            
        case 196:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                             [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            break;
            
            // Tenth Page, Third row
        case 182:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                             [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                             [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"s5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s6"];
            break;
            
        case 187:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                             [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:4],
                             [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            break;
            
        case 192:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 197:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
            // Tenth Page, Fourth row
        case 183:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 188:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 193:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 198:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
            // Tenth Page, Fifth row
        case 184:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 189:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 194:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 199:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
            // Eleventh Page, First row
        case 200:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 205:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 210:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 215:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
            // Eleventh Page, Second row
        case 201:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 206:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 211:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 216:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
            // Eleventh Page, Third row
        case 202:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 207:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 212:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 217:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
            // Eleventh Page, Fourth row
        case 203:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 208:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:8], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:7], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:4], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s6"];
            break;
            
        case 213:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 218:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], [NSNumber numberWithInt:8], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:7], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"s6"];
            break;
            
            // Eleventh Page, Fifth row
        case 204:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 209:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:8], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s6"];
            break;
            
        case 214:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 219:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
            // Twelth Page, First row
        case 220:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"s4"];
            break;
            
        case 225:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:7], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"s5"];
            break;
            
        case 230:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:6], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"s4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"s5"];
            break;
            
        case 235:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:5], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            break;
            
            // Twelth Page, Second row
        case 221:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"s3"];
            break;
            
        case 226:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
        case 231:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s7"];
            break;
            
        case 236:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
            // Twelth Page, Third row
        case 222:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
        case 227:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"s7"];
            break;
            
        case 232:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
        case 237:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"s6"];
            break;
            
            // Twelth Page, Fourth row
        case 223:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 228:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"s6"];
            break;
            
        case 233:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
        case 238:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
            // Twelth Page, Fifth row
        case 224:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 229:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"s6"];
            break;
            
        case 234:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                                                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:4], nil] forKey:@"s3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 239:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:7], nil] forKey:@"s6"];
            break;
            
            // Thirteenth Page, First row
        case 240:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
        case 245:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s4"];
            break;
            
        case 250:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3],
                                                      [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:2],
                                                      [NSNumber numberWithInt:3], [NSNumber numberWithInt:4], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:3], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:6], [NSNumber numberWithInt:7], nil] forKey:@"s5"];
            break;
            
        case 255:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:6], [NSNumber numberWithInt:10], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:5], [NSNumber numberWithInt:9], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:7],
                                                      [NSNumber numberWithInt:8], [NSNumber numberWithInt:12], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:6], [NSNumber numberWithInt:11], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:2], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:9], nil] forKey:@"i8"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d8"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:10], [NSNumber numberWithInt:11], nil] forKey:@"i9"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d9"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:12], nil] forKey:@"i10"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"d10"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:11], nil] forKey:@"i11"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:10], nil] forKey:@"d11"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:8], [NSNumber numberWithInt:9], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:7],
                                                      [NSNumber numberWithInt:9], [NSNumber numberWithInt:10], nil] forKey:@"s2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"s5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:11], nil] forKey:@"s9"];
            break;
            
            // Thirteenth Page, Second row
        case 241:
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:9], [NSNumber numberWithInt:13], nil] forKey:@"i0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:4],
                                                      [NSNumber numberWithInt:8], [NSNumber numberWithInt:12], nil] forKey:@"d0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:10], [NSNumber numberWithInt:14], nil] forKey:@"i1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:5],
                                                      [NSNumber numberWithInt:9], [NSNumber numberWithInt:13], nil] forKey:@"d1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:7],
                                                      [NSNumber numberWithInt:11], [NSNumber numberWithInt:15], nil] forKey:@"i2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], [NSNumber numberWithInt:6],
                                                      [NSNumber numberWithInt:10], [NSNumber numberWithInt:14], nil] forKey:@"d2"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"i3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:0], nil] forKey:@"d3"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"i4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], nil] forKey:@"d4"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"i5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil] forKey:@"d5"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"i6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], nil] forKey:@"d6"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"i7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], nil] forKey:@"d7"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:9], nil] forKey:@"i8"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], nil] forKey:@"d8"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:10], nil] forKey:@"i9"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:6], nil] forKey:@"d9"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:11], nil] forKey:@"i10"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:7], nil] forKey:@"d10"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:12], nil] forKey:@"i11"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:8], nil] forKey:@"d11"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:13], nil] forKey:@"i12"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:9], nil] forKey:@"d12"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:14], nil] forKey:@"i13"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:10], nil] forKey:@"d13"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:15], nil] forKey:@"i14"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:11], nil] forKey:@"d14"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:4], [NSNumber numberWithInt:7],
                                                      [NSNumber numberWithInt:8], [NSNumber numberWithInt:11], [NSNumber numberWithInt:12], nil] forKey:@"s0"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:8],
                                                      [NSNumber numberWithInt:9], [NSNumber numberWithInt:12], [NSNumber numberWithInt:13], nil] forKey:@"s1"];
            [info setObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInt:6], [NSNumber numberWithInt:9],
                                                      [NSNumber numberWithInt:10], [NSNumber numberWithInt:13], [NSNumber numberWithInt:14], nil] forKey:@"s2"];
            break;
            
        default:
            break;
    }
    
    NSLog(@"Collage Crop - %zd collages to modify", [info count]);
    return info;
}

@end
