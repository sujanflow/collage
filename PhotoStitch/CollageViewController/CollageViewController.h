//
//  MainMenuViewController.h
//  stickem3
//
//  Created by Kevin Chee on 30/07/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GoogleMobileAds/GADBannerView.h>
#import "AdManager.h"


    
@interface CollageViewController : UIViewController
<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate,GADBannerViewDelegate>
{
    NSMutableArray *photos;
    NSMutableArray *album;
    NSMutableArray *pics;
    NSMutableArray *timestamp;
    NSMutableArray *date;
    
    int selectedSection;
    NSInteger selectedCollage;
    float fontDecrease;
    NSString *lang;
    
    UIActivityIndicatorView *activity;
    GADBannerView *ibannerView;
}

@property (nonatomic, retain) IBOutlet UICollectionView *collection;
@property (nonatomic, retain) IBOutlet UINavigationBar *albumNavBar;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) IBOutlet UIImageView *collageBG;
@property (nonatomic, retain) IBOutlet UINavigationItem *navTitle;
//@property(nonatomic, strong) IBOutlet GADBannerView *bannerView;

@property (weak, nonatomic) IBOutlet UIView *bannerView;


- (IBAction)dismissMyself:(id) sender;

@end
