//
//  MainMenuViewController.m
//  stickem3
//
//  Created by Kevin Chee on 30/07/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "CollageViewController.h"
#import "CollageCropViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Languages.h"

#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)
@interface CollageViewController ()

@end

@implementation CollageViewController
@synthesize bannerView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lang = [Languages currentLocale];
    fontDecrease = [Languages photoLabelSizeDecrease];
    
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activity setFrame:CGRectMake(self.view.frame.size.width/2.0 - 18.5, self.view.frame.size.height/2.0 - 18.5, 37.0, 37.0)];
    [activity setHidesWhenStopped:YES];
    [activity stopAnimating];
    [activity setColor:[UIColor grayColor]];
    [self.view addSubview:activity];
    
    
    CGPoint origin;
    
//    origin = CGPointMake(self.bannerView.frame.origin.x,self.bannerView.frame.origin.y);
//
//    NSLog(@"origin %@", NSStringFromCGPoint(origin));
    

//    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

        //is iPhone
        
            //setting for iphone x
            if (@available(iOS 11, *)) {
                
                UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
                if (insets.top > 0) {
                    // We're running on an iPhone with a notch.
                    origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-90);
                }else{
                
                   origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-50);

            }
            }else{
                
                origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-50);

                
            }
            
        
//    }else  {
//
//        //is iPad
//
//        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0){
//            //device with Retina Display
//            origin = CGPointMake(20,0);        }
//        else{
//            origin = CGPointMake(20,0);                //no Retina Display
//        }
//    }
//    ibannerView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:admob_bottom_banner andOrigin:origin];
    
        NSLog(@"origin %@", NSStringFromCGPoint(origin));
    
    ibannerView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:@"ca-app-pub-8340924500267413/5572444788" andOrigin:origin];
    
    NSLog(@"ibannerView origin %f  view height %f", ibannerView.frame.origin.y,self.view.bounds.size.height);
    
    ibannerView.rootViewController = self;
    [self.view addSubview:ibannerView];
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (screenSize.height >= 568)
    {
         return 260;
    }
    else
    {
         return 256;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger collageNum = indexPath.row;
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *picture = (UIImageView *)[cell viewWithTag:99];
    
//    if ([[UIScreen mainScreen] bounds].size.height < 568)
//    {
//        switch (indexPath.row)
//        {
//            case 244:
//                collageNum = 245;
//                break;
//
//            case 248:
//                collageNum = 250;
//                break;
//
//            case 252:
//                collageNum = 255;
//                break;
//
//            default:
//                break;
//        }
//    }
//
//    if ([[UIScreen mainScreen] bounds].size.height < 568)
//    {
//        if (indexPath.row != 245 && indexPath.row != 250 && indexPath.row != 255)
//        {
//            NSLog(@"main - %zd", indexPath.row);
//            [picture setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Collage_%zd", collageNum]]];
//        }
//        else
//        {
//            [picture setImage:nil];
//        }
//    }
//    else
//    {
        NSLog(@"main - %zd", indexPath.row);
        [picture setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Collage_%zd", collageNum]]];
   // }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did deselect : %zd", indexPath.row);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    selectedCollage = indexPath.row;
    
    
    if (screenSize.height >= 568)
    {
        if (indexPath.row <= 241 || indexPath.row == 245 || indexPath.row == 250 || indexPath.row == 255)
        {
            // Selects a collage and send for cropping
            NSLog(@"Collage - Selected Section: %zd, Row: %zd", selectedSection, indexPath.row);
            selectedCollage = indexPath.row;
        }
    }
    else
    {
        if (indexPath.row <= 241 || indexPath.row == 244 || indexPath.row == 248 || indexPath.row == 252)
        {
            // Selects a collage and send for cropping
            NSLog(@"Collage - Selected Section???????: %zd, Row: %zd", selectedSection, indexPath.row);
            switch (indexPath.row)
            {
                case 244:
                    selectedCollage = 245;
                    break;
                
                case 248:
                    selectedCollage = 250;
                    break;
                    
                case 252:
                    selectedCollage = 255;
                    break;
                    
                default:
                    selectedCollage = indexPath.row;
                    break;
            }
        }
    }

    [self performSegueWithIdentifier:@"cropSegue" sender:nil];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControl.currentPage = _collection.contentOffset.x / _collection.frame.size.width;
}

- (IBAction)dismissMyself:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [activity startAnimating];
    [segue.destinationViewController gimmeCollageNumber:selectedCollage];
}

@end
