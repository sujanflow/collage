//
//  CropViewController.h
//  pb2free
//
//  Created by Kevin Chee on 26/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CropViewController : UIViewController
<UIGestureRecognizerDelegate>
{
    UIImage *photoImage;
    UIPanGestureRecognizer *pan;
    UIRotationGestureRecognizer *rotate;
    UIPinchGestureRecognizer *pinch;
}

@property (nonatomic, retain) IBOutlet UIView *shadow;
@property (nonatomic, retain) IBOutlet UIImageView *photo;
@property (nonatomic, retain) IBOutlet UIScrollView *scroll;
@property (nonatomic, retain) IBOutlet UIView *bar;
@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet UILabel *cropTitle;
@property (nonatomic, retain) IBOutlet UIImageView *barBottom;

#pragma IBAction
- (IBAction)goEdit:(id)sender;

#pragma Public
- (void)gimmeCropPhoto:(UIImage *)image;

@end
