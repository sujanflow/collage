//
//  CropViewController.m
//  pb2free
//
//  Created by Kevin Chee on 26/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "CropViewController.h"
#import "EditViewController.h"
#import "QuartzCore/Quartzcore.h"
#import "Languages.h"

#define kToMenu @"cropToMenu"
#define kToEdit @"cropToEdit"

@interface CropViewController ()

@end

@implementation CropViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view
    
    [self initPhoto];
    [self initGesture];
    [self addShadowToCanvas];
    
    [_cropTitle setFont:[UIFont fontWithName:@"Segoe Print" size:[Languages labelSizeDefault] + [Languages cropTitleSizeIncrease]]];
    [_label setFont:[UIFont fontWithName:@"Segoe Print" size:[Languages labelSizeDefault] + [Languages cropMessageSizeIncrease]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Initialisation
- (void)initPhoto
{
    [self setPhotoFrame];
    [_photo setImage:photoImage];
    
    NSLog(@"Crop - Photo initialised");
}

- (void)initGesture
{
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panEm:)];
    pan.delegate = self;
    [_photo addGestureRecognizer:pan];
    
    rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotateEm:)];
    rotate.delegate = self;
    //[_photo addGestureRecognizer:rotate];
    
    pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchEm:)];
    pinch.delegate = self;
    [_photo addGestureRecognizer:pinch];
    
    NSLog(@"Crop - Gestures added to photo");
}

- (void)setPhotoFrame
{
    CGRect frame;
    CGSize size;
    
    size = photoImage.size;
    NSLog(@"%@", NSStringFromCGSize(photoImage.size));
    if (size.width < size.height)
    {
        frame = CGRectMake(0.0, -70.0, 320.0, (size.height * 320.0)/size.width);
    }
    else if (size.width > size.height)
    {
        frame = CGRectMake(-70.0, 0.0, (size.width * 320.0)/size.height, 320.0);
    }
    else
    {
        frame = CGRectMake(0.0, 0.0, 320.0, 320.0);
    }
    
    [_photo setFrame:frame];
    
    NSLog(@"Crop - Photo frame: %@", NSStringFromCGRect(frame));
}

- (void)addShadowToCanvas
{
    _shadow.layer.shadowColor = [UIColor blackColor].CGColor;
    _shadow.layer.shadowOffset = CGSizeMake(0, 1);
    _shadow.layer.shadowOpacity = 1;
    _shadow.layer.shadowRadius = 3.0;
    _shadow.clipsToBounds = NO;
}

#pragma Algorithms
- (UIImage *)gimmeMyPictureImage
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0)
    {
        // Use Retina Display
        UIGraphicsBeginImageContextWithOptions(self.view.frame.size, NO, 0.0);
    }
    else
    {
        UIGraphicsBeginImageContext(self.view.frame.size);
    }
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *photo = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return photo;
}

- (UIImage *)gimmeMyInstaPictureImage
{
    int retina = 1;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
        [[UIScreen mainScreen] scale] == 2.0)
    {
        retina = 2;
    }
    else if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
             [[UIScreen mainScreen] scale] == 3.0)
    {
        retina = 3;
    }
    
    UIImage *photo;
    CGRect cropRect = CGRectMake(_scroll.frame.origin.x, _scroll.frame.origin.y*retina, 320.0*retina, 320.0*retina);
    CGImageRef imageRef = CGImageCreateWithImageInRect([[self gimmeMyPictureImage] CGImage], cropRect);
    photo = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return photo;
}

#pragma Gesture
- (IBAction)panEm:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.view.superview];   
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view.superview];
}

- (IBAction)pinchEm:(UIPinchGestureRecognizer *)recognizer
{
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}

- (IBAction)rotateEm:(UIRotationGestureRecognizer *)recognizer
{
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
    recognizer.rotation = 0;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma IBAction functions
- (IBAction)goEdit:(id)sender
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _bar.frame = CGRectMake(_bar.frame.origin.x, _bar.frame.origin.y - _bar.frame.size.height,
                                                 _bar.frame.size.width, _bar.frame.size.height);
                         [_label setAlpha:0.0];
                         _barBottom.frame = CGRectMake(_barBottom.frame.origin.x,
                                                       _barBottom.frame.origin.y + _barBottom.frame.size.height,
                                                       _barBottom.frame.size.width,
                                                       _barBottom.frame.size.height);
                     }completion:^(BOOL finished){
                         [self performSegueWithIdentifier:kToEdit sender:sender];
                     }];
    
    NSLog(@"Crop - Edit Pressed");
}

#pragma Segue functions
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kToEdit])
    {
        [segue.destinationViewController gimmeEditPhoto:[self gimmeMyInstaPictureImage]];
        
        NSLog(@"Crop - Edit Photo set");
    }
}

#pragma Public functions
- (void)gimmeCropPhoto:(UIImage *)image
{
    photoImage = image;
}
@end
