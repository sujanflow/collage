//
//  CollectionPack.h
//  pb2free
//
//  Created by Kevin Chee on 10/10/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionPack : NSObject
{
}

+ (NSInteger)getPacks:(NSString *)collection;
+ (NSInteger)getPackItems:(NSString *)collection :(NSInteger)pack;

@end
