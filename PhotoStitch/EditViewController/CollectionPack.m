//
//  CollectionPack.m
//  pb2free
//
//  Created by Kevin Chee on 10/10/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "CollectionPack.h"

// Packs
#define kPackInstaframe     3
#define kPackDoodles        3
#define kPackQuotes         3
#define kPackFonts          22

// Instaframe Items
#define kPackInstaframe0    13
#define kPackInstaframe1    41
#define kPackInstaframe2    44
#define kPackInstaframe3    20
#define kPackInstaframe4    20
#define kPackInstaframe5    20

// Doodles Items
#define kPackDoodles0       156
#define kPackDoodles1       57
#define kPackDoodles2       77
#define kPackDoodles3       25
#define kPackDoodles4       25
#define kPackDoodles5       25

// Quotes Items
#define kPackQuotes0        18
#define kPackQuotes1        103
#define kPackQuotes2        36
#define kPackQuotes3        31
#define kPackQuotes4        31
#define kPackQuotes5        31

@implementation CollectionPack

+ (NSInteger)getPacks:(NSString *)collection
{
    int count = 0;
    
    if ([collection isEqualToString:@"Instaframe"])
    {
        count = kPackInstaframe;
    }
    else if ([collection isEqualToString:@"Doodles"])
    {
        count = kPackDoodles;
    }
    else if ([collection isEqualToString:@"Quotes"])
    {
        count = kPackQuotes;
    }
    else if ([collection isEqualToString:@"Fonts"])
    {
        count = kPackFonts;
    }
    
    return count;
}

+ (NSInteger)getPackItems:(NSString *)collection :(NSInteger)pack
{
    int count = 0;
    
    if ([collection isEqualToString:@"Instaframe"])
    {
        switch (pack)
        {
            case 0:
                count = kPackInstaframe0;
                break;
                
            case 1:
                count = kPackInstaframe1;
                break;
                
            case 2:
                count = kPackInstaframe2;
                break;
                
            case 3:
                count = kPackInstaframe3;
                break;
                
            case 4:
                count = kPackInstaframe4;
                break;
                
            case 5:
                count = kPackInstaframe5;
                break;
                
            default:
                break;
        }
    }
    else if ([collection isEqualToString:@"Doodles"])
    {
        switch (pack)
        {
            case 0:
                count = kPackDoodles0;
                break;
                
            case 1:
                count = kPackDoodles1;
                break;
                
            case 2:
                count = kPackDoodles2;
                break;
                
            case 3:
                count = kPackDoodles3;
                break;
                
            case 4:
                count = kPackDoodles4;
                break;
                
            case 5:
                count = kPackDoodles5;
                break;
                
            default:
                break;
        }
    }
    else if ([collection isEqualToString:@"Quotes"])
    {
        switch (pack)
        {
            case 0:
                count = kPackQuotes0;
                break;
                
            case 1:
                count = kPackQuotes1;
                break;
                
            case 2:
                count = kPackQuotes2;
                break;
                
            case 3:
                count = kPackQuotes3;
                break;
                
            case 4:
                count = kPackQuotes4;
                break;
                
            case 5:
                count = kPackQuotes5;
                break;
                
            default:
                break;
        }
    }
    
    return count;
}

@end
