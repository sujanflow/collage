//
//  EditViewController.h
//  pb2free
//
//  Created by Kevin Chee on 26/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>



#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>
#import "Sticker.h"
#import <AviarySDK/AviarySDK.h>
#import <AviarySDK/AviarySDK.h>
#import "EditFlowLayout.h"



#import <iAd/iAd.h>
#import "POP.h"

#import <GoogleMobileAds/GADBannerView.h>
#import "AdManager.h"

@interface EditViewController : UIViewController
<UIGestureRecognizerDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,
StickerDelegate, UITextFieldDelegate, AFPhotoEditorControllerDelegate, POPAnimationDelegate,ADBannerViewDelegate, GADBannerViewDelegate>
{
    UIImage *photoImage;
    BOOL aviary;
    NSInteger functionTag;
    NSDictionary *editViewDictionary;
    
    NSString *currentCollection;
    BOOL isPack;
    BOOL exitCollection;
    NSInteger packID;
    EditFlowLayout *myFlowLayout;
    UIView *collectionTopBar;
    UIButton *collectionCloseButton;
    UIButton *collectionPacksButton;
    UILabel *collectionText;
    UIButton *collectionPurchase;
    UIActivityIndicatorView *collectionIndicator;
    
    Sticker *selectedSticker;
    NSMutableArray *stickerArray;
    NSInteger nextTapIndex;
    UITapGestureRecognizer *tap;
    BOOL editMode;
    
    UIImage *blurImage;
    ADBannerView *bView;

}

#pragma Edit
@property (nonatomic, retain) IBOutlet UIView *shadow;
@property (nonatomic, retain) IBOutlet UIView *editView;
@property (nonatomic, retain) IBOutlet UIImageView *savedPhoto;
@property (nonatomic, retain) IBOutlet UIImageView *photo;
@property (nonatomic, retain) IBOutlet UIImageView *border;
@property (nonatomic, retain) IBOutlet UIScrollView *canvas;
@property (nonatomic, retain) IBOutlet UIScrollView *colours;
@property (nonatomic, retain) IBOutlet UIView *edit;
@property (nonatomic, retain) IBOutlet UIButton *add;
@property (nonatomic, retain) IBOutlet UIButton *buttonEditDone;
@property (nonatomic, retain) IBOutlet UIImageView *addRightBar;
@property (nonatomic, retain) IBOutlet UIButton *keyboard;
@property (nonatomic, retain) IBOutlet UIImageView *keyboardRightBar;
@property (nonatomic, retain) IBOutlet UIView *topBar;
@property (nonatomic, retain) IBOutlet UIView *bottomBar;
@property (nonatomic, retain) IBOutlet UIButton *buttonRestart;
@property (nonatomic, retain) IBOutlet UIButton *buttonFinish;
@property (nonatomic, retain) IBOutlet UIButton *buttonBorderDelete;
@property (nonatomic, retain) IBOutlet UILabel *topBarText;
@property (nonatomic, retain) IBOutlet ADBannerView *banner;
@property (nonatomic, retain) IBOutlet UIImageView *editBG;
 
#pragma Collection
@property (nonatomic, retain) IBOutlet UICollectionView *collection;

#pragma Overlay
@property (nonatomic, retain) IBOutlet UIView *overlay;
@property (nonatomic, retain) IBOutlet UIView *overlayTransparent;
@property (nonatomic, retain) IBOutlet UILabel *overlayTitle;
@property (nonatomic, retain) IBOutlet UIButton *buttonNo;
@property (nonatomic, retain) IBOutlet UIButton *buttonYes;
@property(nonatomic, retain) IBOutlet GADBannerView *bSView;

#pragma IBAction
- (IBAction)goStartOver:(id)sender;
- (IBAction)goMenu:(id)sender;
- (IBAction)goFinish:(id)sender;
- (IBAction)startOverClose:(id)sender;
- (IBAction)functionButton:(id)sender;
- (IBAction)editDone:(id)sender;
- (IBAction)launchKeyboard:(id)sender;
- (IBAction)addSticker:(id)sender;
- (IBAction)deleteBorder:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *testImage;


#pragma Public
- (void)gimmeEditPhoto:(UIImage *)image;
- (void)purchaseSuccess;
- (void)purchaseFail;
+ (EditViewController *)sharedLayer;

@end
