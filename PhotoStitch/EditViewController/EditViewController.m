//
//  EditViewController.m
//  pb2free
//
//  Created by Kevin Chee on 26/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//
@import GoogleMobileAds;

#import "EditViewController.h"
#import "SavedViewController.h"
#import "FontPack.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "QuartzCore/Quartzcore.h"
#import "CollectionPack.h"
#import "IAPHelper.h"
#import "Languages.h"
@import GoogleMobileAds;

#define kToMenu         @"editToMenu"
#define kToSave         @"editToSave"
#define kAviaryAPIKey   @"42d379bf17210df5"
#define kAviarySecret   @"12039d06bff6d1a8"



static EditViewController *layer;


@interface EditViewController ()<GADInterstitialDelegate>
//@property(nonatomic, strong) GADBannerView *bView;
@property(nonatomic, strong) GADInterstitial*interstitial;
@end



@implementation EditViewController
@synthesize bSView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.interstitial = [self createAndLoadInterstitial];
    
    //Load Dictionary with wood name cross refference values for image name
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"ConfigPList" ofType:@"plist"];
    editViewDictionary = [[NSDictionary alloc] initWithContentsOfFile:plistCatPath];
    [[IAPHelper sharedHelper] loadDictionary:editViewDictionary];

    layer = nil;
    layer = self;
	// Do any additional setup after loading the view

    
    
//    edited by Monir
   /* _banner.delegate = self;
    [_banner setFrame:CGRectMake(0.0, self.view.frame.size.height - _banner.frame.size.height - _bottomBar.frame.size.height,_banner.frame.size.width, _banner.frame.size.height)]; */
    
    // Set Photo
    [self initPhoto];
    [self addShadowToCanvas];
    
    // Init Collection View
    [self initCollection];
    
    // Init Colours
    [self initColours];
    
    // Move out of normal position to animate them
    [self initAnimation];
    
    // Hide items not used on start
    [self hideMe];
    
    // Load OpenGL for Aviary
    [AFOpenGLManager beginOpenGLLoad];
    
    editMode = NO;
    aviary = NO;
    stickerArray = [[NSMutableArray alloc] init];
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCanvas:)];
    [tap setNumberOfTapsRequired:1];
    [_canvas addGestureRecognizer:tap];
    
    [_topBarText setFont:[UIFont fontWithName:@"Roboto Condensed" size:[Languages labelSizeDefault] + [Languages editTitleSizeIncrease]]];
    [_overlayTitle setFont:[UIFont fontWithName:@"Roboto Condensed" size:43.0f]];
    [_buttonNo.titleLabel setFont:[UIFont fontWithName:@"Roboto Condensed" size:35.0f]];
    [_buttonYes.titleLabel setFont:[UIFont fontWithName:@"Roboto Condensed" size:35.0f]];
    
    
    CGPoint origin;
    
    //setting for iphone x
    if (@available(iOS 11, *)) {
        
        UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
        if (insets.top > 0) {
            // We're running on an iPhone with a notch.
            origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-90);
        }else{
            
            origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-50);
            
        }
    }else{
        
        origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-50);
        
        
    }

    
    
    
    
    
    /*
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?kGADAdSizeLeaderboard:kGADAdSizeBanner origin:origin];
    bannerView.adUnitID = admob_interstitial;
    NSString *sourceString = [[NSThread callStackSymbols] objectAtIndex:1];
    
    NSCharacterSet *separatorSet = [NSCharacterSet characterSetWithCharactersInString:@" -[]+?.,"];
    NSMutableArray *array = [NSMutableArray arrayWithArray:[sourceString  componentsSeparatedByCharactersInSet:separatorSet]];
    [array removeObject:@""];
    
    bannerView.rootViewController =self;
    
    [bannerView loadRequest:[self adMobrequest]];
    */
    
    
   
    /*
    
    self.bView = [[GADBannerView alloc]
                       initWithAdSize:kGADAdSizeBanner];
    [self.view addSubview:self.bView ];
    self.bView .adUnitID = admob_interstitial;
    self.bView .rootViewController = self;
    [self.bView loadRequest:[self adMobrequest]];
   // [self.bView  loadRequest:[GADRequest request]];
    [self.view addSubview:self.bView ];*/
    
    /*
    bView = [[ADBannerView alloc]initWithFrame:
                  CGRectMake(0, 100, 320, 50)];
    bView.delegate=self;
    
    
    // Optional to set background color to clear color
    [bView setBackgroundColor:[UIColor redColor]];
    [self.view addSubview: bView];*/
    
   /*
   
    self.bSView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:origin];//
    //[[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:origin];
     [self.view addSubview: self.bSView];
    // self.bSView.delegate = self;
    self.bSView.adUnitID = admob_interstitial;
    self.bSView.rootViewController = self;
   [self.bSView  loadRequest:[GADRequest request]];
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADBannerView automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[
                            @"2077ef9a63d2b398840261c8221a0c9a"  // Eric's iPod Touch
                            ];
    [self.bSView loadRequest:request];
    
   [self.view addSubview: self.bSView ];
    */

    
    
    
//    bSView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:admob_bottom_banner andOrigin:origin];
    bSView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:@"ca-app-pub-8340924500267413/5572444788" andOrigin:origin];
    bSView.rootViewController = self;
    [self.view addSubview:bSView];
    
    
}


- (void)viewDidAppear:(BOOL)animated
{
    // Move to correct position
    
  //  [self animateIn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- Initialisation
- (void)initPhoto
{
    
    [_photo setImage:photoImage];
    
    NSLog(@"canvas  %@  and photo %@", NSStringFromCGRect(_canvas.frame), NSStringFromCGRect(_photo.frame));
}

- (void)initCollection
{
    [self createTopBar];
    
    isPack = YES;
    exitCollection = NO;
    
    myFlowLayout = [[EditFlowLayout alloc] init];
    [myFlowLayout setStackFactor:1.0];
    [myFlowLayout setStackCenter:CGPointMake(57.5, 57.5)];
    [_collection setCollectionViewLayout:myFlowLayout animated:YES];
}

- (void)initColours
{
    int i = 0;
    
    while (i <= [FontPack totalColour])
    {
        [_colours addSubview:[self createColourButton:[FontPack findColour:i] :i]];
        i++;
    }
    
    [_colours setContentSize:CGSizeMake(50.0*i, 50.0)];
}

- (UIButton *)createColourButton:(UIColor *)colour :(int)num
{    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(50.0*num, 0.0, 50.0, 50.0)];
    [button setBackgroundColor:colour];
    [button addTarget:self action:@selector(changeColour:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (void)addShadowToCanvas
{
    _shadow.layer.shadowColor = [UIColor blackColor].CGColor;
    _shadow.layer.shadowOffset = CGSizeMake(0, 1);
    _shadow.layer.shadowOpacity = 1;
    _shadow.layer.shadowRadius = 3.0;
    _shadow.clipsToBounds = NO;
}

- (void)initAnimation
{
//    CGRect topFrame = _topBar.frame;
//    CGRect bottomFrame = _bottomBar.frame;
//    CGRect overlayTitleFrame = _overlayTitle.frame;
//    CGRect overlayNoFrame = _buttonNo.frame;
//    CGRect overlayYesFrame = _buttonYes.frame;
//    CGRect collectionFrame = _collection.frame;
//    CGRect editFrame = _edit.frame;
//    CGRect colourFrame = _colours.frame;
//
//    // Top bar
//    topFrame = CGRectMake(0, 0 - topFrame.size.height,
//                          topFrame.size.width, topFrame.size.height);
//    [_topBar setFrame:topFrame];
//
//    // Bottom bar
//    bottomFrame = CGRectMake(0, 468 + bottomFrame.size.height,
//                             bottomFrame.size.width, bottomFrame.size.height);
//    [_bottomBar setFrame:bottomFrame];
//
//    // Overlay
//    overlayTitleFrame = CGRectMake(overlayTitleFrame.origin.x, overlayTitleFrame.origin.y - self.view.frame.size.height,
//                                   overlayTitleFrame.size.width, overlayTitleFrame.size.height);
//    [_overlayTitle setFrame:overlayTitleFrame];
//
//    overlayNoFrame = CGRectMake(overlayNoFrame.origin.x, overlayNoFrame.origin.y + self.view.frame.size.height,
//                                overlayNoFrame.size.width, overlayNoFrame.size.height);
//    [_buttonNo setFrame:overlayNoFrame];
//
//    overlayYesFrame = CGRectMake(overlayYesFrame.origin.x, overlayYesFrame.origin.y + self.view.frame.size.height,
//                                 overlayYesFrame.size.width, overlayYesFrame.size.height);
//    [_buttonYes setFrame:overlayYesFrame];
//
//    // Collection
//    collectionFrame = CGRectMake(collectionFrame.origin.x, collectionFrame.origin.y + self.view.frame.size.height,
//                                 collectionFrame.size.width, collectionFrame.size.height);
//    [_collection setFrame:collectionFrame];
//
//    // Edit
//    editFrame = CGRectMake(editFrame.origin.x, editFrame.origin.y - editFrame.size.height,
//                           editFrame.size.width, editFrame.size.height);
//    [_edit setFrame:editFrame];
//
//    // Colours
//    colourFrame = CGRectMake(colourFrame.origin.x, colourFrame.origin.y + colourFrame.size.height,
//                             colourFrame.size.width, colourFrame.size.height);
//    [_colours setFrame:colourFrame];
    
    NSLog(@"Edit - Prepared for Animation");
}

- (void)animateIn
{
    CGRect canvasFrame = _canvas.frame;
    float canvasY, availableHeight, spaceBetween;
    
    POPSpringAnimation *topBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    topBarAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, 0 - _topBar.frame.size.height,
                                                                    _topBar.frame.size.width, _topBar.frame.size.height)];
    topBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, -40 + _topBar.frame.size.height,
                                                                  _topBar.frame.size.width, _topBar.frame.size.height)];
    topBarAnimation.springSpeed = 3;
    topBarAnimation.springBounciness = 18;
    [_topBar.layer pop_addAnimation:topBarAnimation forKey:@"TopBarAnimation"];

    POPSpringAnimation *bottomBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    bottomBarAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, 468 + _bottomBar.frame.size.height,
                                                                       _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
    bottomBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, 518 - _bottomBar.frame.size.height,
                                                                     _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
    bottomBarAnimation.springSpeed = 3;
    bottomBarAnimation.springBounciness = 18;
    [_bottomBar.layer pop_addAnimation:bottomBarAnimation forKey:@"BottomBarAnimation"];
    
    // Animate photo canvas
    availableHeight = [[UIScreen mainScreen] bounds].size.height - _topBar.frame.size.height - _colours.frame.size.height - 50.0;
    spaceBetween = (availableHeight - 320.0)/2.0;
    canvasY = _topBar.frame.size.height + spaceBetween;
    canvasFrame = CGRectMake(canvasFrame.origin.x, canvasY, canvasFrame.size.width, canvasFrame.size.height);
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_canvas setFrame:canvasFrame];
                         [_shadow setFrame:canvasFrame];
                     }completion:^(BOOL finished){
                     }];

    NSLog(@"%@", NSStringFromCGRect(_canvas.frame));
    NSLog(@"Edit - Contents Animated");
}

- (void)hideMe
{
    _overlay.hidden = YES;
    _overlayTitle.hidden = YES;
    _buttonNo.hidden = YES;
    _buttonYes.hidden = YES;
    _collection.hidden = YES;
    
   // _colours.hidden = YES;
    
    collectionTopBar.hidden = YES;
    _edit.hidden = YES;
    _keyboard.hidden = YES;
    _keyboardRightBar.hidden = _keyboard.hidden;
    _savedPhoto.hidden = YES;
    _buttonBorderDelete.hidden = YES;
}

- (void)captureBlur
{
    // Get a UIImage from the UIView
    _shadow.layer.shadowColor = [UIColor whiteColor].CGColor;
    UIGraphicsBeginImageContext(self.view.frame.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Blue the UIImage
    CIImage *imageToBlur = [CIImage imageWithCGImage:viewImage.CGImage];
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [gaussianBlurFilter setValue:imageToBlur forKey:@"inputImage"];
    [gaussianBlurFilter setValue:[NSNumber numberWithFloat:5] forKey:@"inputRadius"];
    CIImage *resultImage = [gaussianBlurFilter valueForKey:@"outputImage"];
    
    // Create UIImage from filtered image
    blurImage = [[UIImage alloc] initWithCIImage:resultImage];
    
    // Place the UIImage in a UIImageView
    UIImageView *newView = [[UIImageView alloc] initWithFrame:self.view.frame];
    newView.image = blurImage;
    
    // Insert blur UIImageView below transparent view inside the blur image container
    [_overlay insertSubview:newView belowSubview:_overlayTransparent];
}

- (void)goBlur
{
    _edit.alpha = 0.0f;
    _bottomBar.alpha = 0.0f;
    _canvas.alpha = 0.0f;
    _topBar.alpha = 0.0f;
}

- (void)backFromBlur
{
    _edit.alpha = 1.0f;
    _bottomBar.alpha = 1.0f;
    _canvas.alpha = 1.0f;
    _topBar.alpha = 1.0f;
    _shadow.layer.shadowColor = [UIColor blackColor].CGColor;
}

#pragma mark -- Collection View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (isPack)
    {
        return [CollectionPack getPacks:currentCollection];
    }
    else if (exitCollection)
    {
        return 0;
    }
    else
    {
        return [CollectionPack getPackItems:currentCollection :packID];
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (isPack)
    {
        if ([currentCollection isEqualToString:@"Instaframe"] || [currentCollection isEqualToString:@"Doodles"] ||
            [currentCollection isEqualToString:@"Quotes"])
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:100];
            [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Pack%zd%@_0.png", indexPath.row, currentCollection]]];
            
            UILabel *label = (UILabel *)[cell viewWithTag:99];
            [label setText:nil];
        }
        else if ([currentCollection isEqualToString:@"Fonts"])
        {
            UILabel *label = (UILabel *)[cell viewWithTag:99];
            [label setFont:[UIFont fontWithName:[FontPack findFont:indexPath.row+1] size:40.0]];
            [label setTextColor:[UIColor colorWithRed:105.0/255.0 green:105.0/255.0 blue:105.0/255.0 alpha:1.0]];
            [label setText:@"ABC"];
            
            UIImageView *image = (UIImageView *)[cell viewWithTag:100];
            [image setImage:nil];
        }
    }
    else
    {
        if ([currentCollection isEqualToString:@"Instaframe"] || [currentCollection isEqualToString:@"Doodles"] ||
            [currentCollection isEqualToString:@"Quotes"])
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:100];
            [image setImage:[UIImage imageNamed:
                             [NSString stringWithFormat:@"Pack%zd%@_%zd.png", packID, currentCollection, indexPath.row+1]]];
            
            UILabel *label = (UILabel *)[cell viewWithTag:99];
            [label setText:nil];
        }
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isPack && ![currentCollection isEqualToString:@"Fonts"])
    {
        packID = indexPath.row;
        NSString *key = [NSString stringWithFormat:@"%@.%@%zd", editViewDictionary[@"BundleIdentifier"], currentCollection, packID];
        if (![[IAPHelper sharedHelper] isProductPurchased:key])
        {
            [collectionPurchase setHidden:NO];
            [collectionPurchase setAlpha:0.0];
        }
        
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [collectionText setFrame:CGRectMake(collectionText.frame.origin.x - 50.0f,
                                                                 collectionText.frame.origin.y,
                                                                 collectionText.frame.size.width,
                                                                 collectionText.frame.size.height)];
                             [collectionText setAlpha:0.0];
                             [collectionCloseButton setAlpha:0.0];
                         }completion:^(BOOL finished)
         {
             // Move items to the right
             myFlowLayout.stackCenter = CGPointMake(self.view.frame.size.width + 150.0, 100.0);
             [collectionText setFrame:CGRectMake(collectionText.frame.origin.x + 100.0f,
                                                 collectionText.frame.origin.y,
                                                 collectionText.frame.size.width,
                                                 collectionText.frame.size.height)];
             [self updateCollectionText];
             
             [UIView animateWithDuration:0.2
                                   delay:0
                                 options:UIViewAnimationOptionCurveEaseOut
                              animations:^{
                                  [collectionText setFrame:CGRectMake(collectionText.frame.origin.x - 50.0f,
                                                                      collectionText.frame.origin.y,
                                                                      collectionText.frame.size.width,
                                                                      collectionText.frame.size.height)];
                                  [collectionText setAlpha:1.0];
                                  [collectionPacksButton setAlpha:1.0];
                                  if (!collectionPurchase.hidden)
                                  {
                                      [collectionPurchase setAlpha:1.0];
                                  }
                              }completion:^(BOOL finished){}];
         }];
        
        [_collection performBatchUpdates:^{
            // First move albums away to the left
            myFlowLayout.stackCenter = CGPointMake(-150.0, 100.0);
            myFlowLayout.moveOut = YES;
        } completion:
         ^(BOOL finished)
         {
             isPack = NO;
             
             [_collection reloadSections:[NSIndexSet indexSetWithIndex:0]];
             
             [_collection performBatchUpdates:^{
                 myFlowLayout.stackCenter = CGPointMake(57.5, 57.5);
                 myFlowLayout.moveOut = NO;
             } completion:^(BOOL finished) {}];
             
             NSLog(@"Edit - Pack selected: %zd", packID);
         }];
                
        [_collection setContentOffset:CGPointZero animated:YES];
    }
    else if (isPack && [currentCollection isEqualToString:@"Fonts"])
    {
        // To distinguish between item selected and collection close without selection
        UIButton *temp = [UIButton buttonWithType:UIButtonTypeCustom];
        temp.tag = 0;
        [self collectionClose:temp];
        
        CGFloat width, height;
        width = 200.0;
        height = 100.0;
        CGFloat centerX = _canvas.frame.size.height/2.0, centerY = _canvas.frame.size.width/2.0;

        Sticker *text = [[Sticker alloc] initWithFrame:CGRectMake(centerX - (width/2.0), centerY - (height/2.0), width, height)];
        text.tag = indexPath.row + 1;
        text.delegate = self;
        [text giveText];
        [text giveGestures];
        [text setInitSize:CGSizeMake(width, height)];
        [_canvas addSubview:text];
        
        [stickerArray addObject:text];
        selectedSticker = text;
        _keyboard.hidden = selectedSticker.isImage;
        _keyboardRightBar.hidden = _keyboard.hidden;
        _add.hidden = NO;
        _buttonBorderDelete.hidden = YES;
        _addRightBar.hidden = _add.hidden;
    
        [_collection setContentOffset:CGPointZero animated:YES];
        
        // Edit Mode state
        [self enterEditMode];
    }
    else
    {
        NSString *key = [NSString stringWithFormat:@"%@.%@%zd", editViewDictionary[@"BundleIdentifier"], currentCollection, packID];
        if (![[IAPHelper sharedHelper] isProductPurchased:key])
        {
            [self purchaseMe:nil];
        }
        else
        {
            // To distinguish between item selected and collection close without selection
            UIButton *temp = [UIButton buttonWithType:UIButtonTypeCustom];
            temp.tag = 0;
            [self collectionClose:temp];
            
            CGFloat width, height;
            CGFloat centerX = _canvas.frame.size.height/2.0, centerY = _canvas.frame.size.width/2.0;
            UIImage *image = [self imageNamed:[NSString stringWithFormat:@"Pack%zd%@_%zd.png", packID, currentCollection, indexPath.row+1]
                                  withColor:[UIColor whiteColor]];

            if ([currentCollection isEqualToString:@"Instaframe"])
            {
                [_border setImage:image];
                _border.tag = indexPath.row + 1;
                _keyboard.hidden = YES;
                _keyboardRightBar.hidden = _keyboard.hidden;
                _add.hidden = YES;
                _buttonBorderDelete.hidden = NO;
                _addRightBar.hidden = _add.hidden;
            }
            else if ([currentCollection isEqualToString:@"Doodles"] || [currentCollection isEqualToString:@"Quotes"])
            {
                width = image.size.width;
                height = image.size.height;
                
                if ((width >= 200.0 && width <= 300.0) || (height >= 200.0 && height <= 300.0))
                {
                    width = width/2.0;
                    height = height/2.0;
                }
                else if (width >= 300.0 || height >= 300.0)
                {
                    width = width/3.0;
                    height = height/3.0;
                }
                
                Sticker *sticker = [[Sticker alloc] initWithFrame:CGRectMake(centerX - (width/2.0), centerY - (height/2.0), width, height)];
                [sticker giveImage:image :currentCollection :packID];
                [sticker giveGestures];
                [sticker setInitSize:CGSizeMake(width, height)];
                sticker.tag = indexPath.row + 1;
                sticker.delegate = self;
                [_canvas addSubview:sticker];
                
                [stickerArray addObject:sticker];
                selectedSticker = sticker;
                _keyboard.hidden = selectedSticker.isImage;
                _keyboardRightBar.hidden = _keyboard.hidden;
                _add.hidden = NO;
                _buttonBorderDelete.hidden = YES;
                _addRightBar.hidden = _add.hidden;
            }
            
            [_collection setContentOffset:CGPointZero animated:YES];
            
            // Edit Mode state
            [self enterEditMode];
            
            //[Flurry logEvent:[NSString stringWithFormat:@"Edit_Pack%d%@_%d", packID, currentCollection, indexPath.row+1]];
            NSLog(@"Edit - Selected image size : %@", NSStringFromCGSize(image.size));
        }
    }
}

- (void)createTopBar
{
    collectionTopBar = [[UIView alloc] initWithFrame:CGRectMake(0.0, -50.0, self.view.frame.size.width, 50.0)];
    [collectionTopBar setFrame:CGRectInset(collectionTopBar.frame, -1.0f, 0.0f)];
    [collectionTopBar setBackgroundColor:[UIColor clearColor]];
    [collectionTopBar.layer setBorderWidth:1.0f];
    
    collectionCloseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [collectionCloseButton setFrame:CGRectMake(0.0, 0.0, 50.0, 50.0)];
    [collectionCloseButton addTarget:self action:@selector(collectionClose:) forControlEvents:UIControlEventTouchUpInside];
    [collectionCloseButton setImage:[UIImage imageNamed:@"edit_close.png"] forState:UIControlStateNormal];
    [collectionTopBar addSubview:collectionCloseButton];

    collectionPacksButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [collectionPacksButton setFrame:CGRectMake(0.0, 0.0, 50.0, 50.0)];
    [collectionPacksButton addTarget:self action:@selector(collectionPacks:) forControlEvents:UIControlEventTouchUpInside];
    [collectionPacksButton setImage:[UIImage imageNamed:@"edit_stickers_back.png"] forState:UIControlStateNormal];
    [collectionPacksButton setAlpha:0.0];
    [collectionTopBar addSubview:collectionPacksButton];
    
    collectionText = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 0.0, 220.0, 50.0)];
    [collectionText setFont:[UIFont fontWithName:@"Roboto Condensed" size:[Languages labelSizeDefault] + [Languages editCollecitonTitleSizeIncrease]]];
    [collectionText setTextAlignment:NSTextAlignmentCenter];
    [collectionText setBackgroundColor:[UIColor clearColor]];
    [collectionTopBar addSubview:collectionText];
    
    collectionPurchase = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 80.0, 0.0, 80.0, 50.0)];
    [collectionPurchase setTitle:NSLocalizedString(@"EDIT_PURCHASE", @"Edit - Purchase") forState:UIControlStateNormal];
    [collectionPurchase.titleLabel setFont:[UIFont fontWithName:@"Roboto Condensed" size:15.0]];
    [collectionPurchase.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [collectionPurchase setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [collectionPurchase setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [collectionPurchase addTarget:self action:@selector(purchaseMe:) forControlEvents:UIControlEventTouchUpInside];
    [collectionTopBar addSubview:collectionPurchase];
    
    collectionIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 80.0, -15.0, 80.0, 80.0)];
    collectionIndicator.hidesWhenStopped = YES;
    [collectionIndicator setColor:[UIColor blackColor]];
    [collectionTopBar addSubview:collectionIndicator];
    
    
    
    //UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0.0, 49.0, collectionTopBar.frame.size.width, 1.0)];
    //line.backgroundColor = [UIColor blackColor];
    //[collectionTopBar addSubview:line];
    
    [self.view addSubview:collectionTopBar];
}

- (IBAction)collectionClose:(id)sender
{
    CGRect collectionFrame = _collection.frame;
    CGRect barFrame = collectionTopBar.frame;

//    collectionFrame = CGRectMake(collectionFrame.origin.x, collectionFrame.origin.y + self.view.frame.size.height,
//                                 collectionFrame.size.width, collectionFrame.size.height);
    
    barFrame = CGRectMake(barFrame.origin.x, barFrame.origin.y - barFrame.size.height,
                          barFrame.size.width, barFrame.size.height);
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_overlay setAlpha:0.0];
                         [self backFromBlur];
                         [_collection setFrame:collectionFrame];
                         [collectionTopBar setFrame:barFrame];
                     }completion:^(BOOL finished){
                         [_collection setContentOffset:CGPointZero animated:YES];
                         _overlay.hidden = YES;
                         _collection.hidden = YES;
                         collectionTopBar.hidden = YES;
                         [collectionPacksButton setAlpha:0.0];
                         [collectionCloseButton setAlpha:1.0];
                         exitCollection = YES;
                         [_collection reloadSections:[NSIndexSet indexSetWithIndex:0]];
                     }];
    
    NSLog(@"Edit - Collection closed. Current Collection: %@", currentCollection);
}

- (IBAction)collectionPacks:(id)sender
{
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [collectionText setFrame:CGRectMake(collectionText.frame.origin.x + 50.0f,
                                                             collectionText.frame.origin.y,
                                                             collectionText.frame.size.width,
                                                             collectionText.frame.size.height)];
                         [collectionText setAlpha:0.0];
                         [collectionPacksButton setAlpha:0.0];
                         if (!collectionPurchase.hidden)
                         {
                             [collectionPurchase setAlpha:0.0];
                         }
                     }completion:^(BOOL finished)
     {
         // Move packs to the left
         myFlowLayout.stackCenter = CGPointMake(-150.0, 100.0);
         [collectionText setFrame:CGRectMake(collectionText.frame.origin.x - 100.0f,
                                             collectionText.frame.origin.y,
                                             collectionText.frame.size.width,
                                             collectionText.frame.size.height)];
         [self setPackName];
         
         
         [UIView animateWithDuration:0.2
                               delay:0
                             options:UIViewAnimationOptionCurveEaseOut
                          animations:^{
                              [collectionText setFrame:CGRectMake(collectionText.frame.origin.x + 50.0f,
                                                                  collectionText.frame.origin.y,
                                                                  collectionText.frame.size.width,
                                                                  collectionText.frame.size.height)];
                              [collectionText setAlpha:1.0];
                              [collectionCloseButton setAlpha:1.0];
                              [collectionPurchase setHidden:YES];
                          }completion:^(BOOL finished){}];
     }];
    
    [_collection performBatchUpdates:^{
        // First move items away to the right
        myFlowLayout.stackCenter = CGPointMake(self.view.frame.size.width + 150.0, 100.0);
        myFlowLayout.moveOut = YES;
    } completion:
     ^(BOOL finished)
     {
         isPack = YES;
         [_collection reloadSections:[NSIndexSet indexSetWithIndex:0]];
         
         [_collection performBatchUpdates:^{
             myFlowLayout.stackCenter = CGPointMake(57.5, 57.5);
             myFlowLayout.moveOut = NO;
         } completion:^(BOOL finished) {
             [_collection setContentOffset:CGPointZero animated:YES];
         }];
     }];
    
    
    NSLog(@"Edit - Back to Packs : Pack%zd%@", packID, currentCollection);
}

- (void)setPackName
{
    if ([currentCollection isEqualToString:@"Instaframe"])
    {
        collectionText.text = NSLocalizedString(@"EDIT_BORDERS", @"Edit - Borders");
    }
    else if ([currentCollection isEqualToString:@"Doodles"])
    {
        collectionText.text = NSLocalizedString(@"EDIT_DOODLES", @"Edit - Doodles");
    }
    else if ([currentCollection isEqualToString:@"Quotes"])
    {
        collectionText.text = NSLocalizedString(@"EDIT_QUOTES", @"Edit - Quotes");
    }
}

- (void)updateCollectionText
{
    NSString *pack;
    
    if ([currentCollection isEqualToString:@"Instaframe"])
    {
        pack = [NSString stringWithFormat:@"%@ Pack %zd", NSLocalizedString(@"EDIT_INSTAFRAME", @"Edit - Instaframe"), packID];
    }
    else if ([currentCollection isEqualToString:@"Doodles"])
    {
        pack = [NSString stringWithFormat:@"%@ Pack %zd", NSLocalizedString(@"EDIT_DOODLES", @"Edit - Doodles"), packID];
    }
    else if ([currentCollection isEqualToString:@"Quotes"])
    {
        pack = [NSString stringWithFormat:@"%@ Pack %zd", NSLocalizedString(@"EDIT_QUOTES", @"Edit - Quotes"), packID];
    }

    if (packID == 0)
    {
        pack = NSLocalizedString(@"EDIT_STARTERPACK", @"Edit - Starter Pack");
    }
    
    collectionText.text = [NSString stringWithFormat:@"%@", pack];
}

#pragma mark -- Editing
- (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color
{
    UIImage *image = [UIImage imageNamed:name];
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0)
    {
        // Use Retina Display
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, image.scale);
    }
    else
    {
        UIGraphicsBeginImageContext(rect.size);
    }
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, image.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *flippedImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:UIImageOrientationDownMirrored];
    
    return flippedImage;
}

- (UIImage *)gimmeMyPictureImage
{
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0)
//    {
//        // Use Retina Display
//        UIGraphicsBeginImageContextWithOptions(self.view.frame.size, NO, 0.0);
//    }
//    else
//    {
//        UIGraphicsBeginImageContext(self.view.frame.size);
//    }
    UIGraphicsBeginImageContext(self.canvas.frame.size);
    [self.canvas.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *photo = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return photo;
}

- (UIImage *)gimmeMyInstaPictureImage
{
//    int retina = 1;
//
//    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
//        [[UIScreen mainScreen] scale] == 2.0)
//    {
//        retina = 2;
//    }
    
    UIImage *photo;
//    CGRect cropRect = CGRectMake(_canvas.frame.origin.x, _canvas.frame.origin.y*retina, 320.0*retina, 320.0*retina);
    CGRect cropRect = CGRectMake(_canvas.frame.origin.x, _canvas.frame.origin.y, _canvas.frame.size.width, _canvas.frame.size.height);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([[self gimmeMyPictureImage] CGImage], cropRect);
    photo = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return photo;
}

- (IBAction)changeColour:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if ([currentCollection isEqualToString:@"Instaframe"])
    {
        [_border setImage:[self imageNamed:[NSString stringWithFormat:@"Pack%zd%@_%zd.png", packID, currentCollection, _border.tag]
                                            withColor:button.backgroundColor]];
    }
    else
    {
        if ([selectedSticker isImage])
        {
            [selectedSticker changeImage:[self imageNamed:[NSString stringWithFormat:@"Pack%zd%@_%zd.png",
                                                           selectedSticker.pack, selectedSticker.collection, selectedSticker.tag]
                                                withColor:button.backgroundColor]];
        }
        else
        {
            [selectedSticker changeTextColor:button.backgroundColor];
        }
    }
}

- (void)enterEditMode
{
    CGRect editFrame = _edit.frame;
    CGRect bottomBarFrame = _bottomBar.frame;
    CGRect canvasFrame = _canvas.frame;
    float canvasY, availableHeight, spaceBetween;
 
    editMode = YES;
    
//    availableHeight = [[UIScreen mainScreen] bounds].size.height - _topBar.frame.size.height - _colours.frame.size.height - 50.0;
//    spaceBetween = (availableHeight - 320.0)/2.0;
//    canvasY = _topBar.frame.size.height + spaceBetween;
//    canvasFrame = CGRectMake(canvasFrame.origin.x, canvasY, canvasFrame.size.width, canvasFrame.size.height);
//
//    editFrame = CGRectMake(editFrame.origin.x, editFrame.origin.y - editFrame.size.height,
//                           editFrame.size.width, editFrame.size.height);
//
//    bottomBarFrame = CGRectMake(bottomBarFrame.origin.x, bottomBarFrame.origin.y + bottomBarFrame.size.height,
//                                bottomBarFrame.size.width, bottomBarFrame.size.height);
//
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
//                         if ([currentCollection isEqualToString:@"Instaframe"])
//                         {
//                             _buttonBorderDelete.hidden = NO;
//                             [_buttonBorderDelete setAlpha:1.0];
//                         }
                         [_canvas setFrame:canvasFrame];
                         [_shadow setFrame:canvasFrame];
                     }completion:^(BOOL finished){
                     }];
    
    _edit.hidden = NO;
    
    POPSpringAnimation *topBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    topBarAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _topBar.frame.origin.y + _topBar.frame.size.height,
                                                                    _topBar.frame.size.width, _topBar.frame.size.height)];
    topBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _topBar.frame.origin.y - _topBar.frame.size.height,
                                                                  _topBar.frame.size.width, _topBar.frame.size.height)];
    topBarAnimation.springSpeed = 8;
    topBarAnimation.springBounciness = 1;
    [_topBar.layer pop_addAnimation:topBarAnimation forKey:@"TopBarAnimation"];
    
    POPSpringAnimation *bottomBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    bottomBarAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bottomBar.frame.origin.y - _bottomBar.frame.size.height,
                                                                       _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
    bottomBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bottomBar.frame.origin.y + _bottomBar.frame.size.height,
                                                                     _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
    bottomBarAnimation.springSpeed = 8;
    bottomBarAnimation.springBounciness = 1;
    [_bottomBar.layer pop_addAnimation:bottomBarAnimation forKey:@"BottomBarAnimation"];
    
    POPSpringAnimation *editAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    editAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _edit.frame.origin.y - _edit.frame.size.height,
                                                                  _edit.frame.size.width, _edit.frame.size.height)];
    editAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _edit.frame.origin.y + _edit.frame.size.height,
                                                                _edit.frame.size.width, _edit.frame.size.height)];
    editAnimation.springSpeed = 5;
    editAnimation.springBounciness = 15;
    editAnimation.beginTime = (CACurrentMediaTime() + 0.5);
    [_edit.layer pop_addAnimation:editAnimation forKey:@"EditAnimation"];
    
    POPSpringAnimation *coloursAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    coloursAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _colours.frame.origin.y + _colours.frame.size.height,
                                                                       _colours.frame.size.width, _colours.frame.size.height)];
    coloursAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _colours.frame.origin.y - _colours.frame.size.height,
                                                                     _colours.frame.size.width, _colours.frame.size.height)];
    coloursAnimation.springSpeed = 5;
    coloursAnimation.springBounciness = 1;
    coloursAnimation.beginTime = (CACurrentMediaTime() + 0.5);
    [_colours.layer pop_addAnimation:coloursAnimation forKey:@"ColoursAnimation"];
}

-(void)shiftForSpace:(BOOL)up
{
    
    NSLog(@"shiftForSpace");
    
    if (self.view.frame.size.height == 480)
    {
        if (up)
        {
            //Move Up
            if (_topBar.frame.origin.y == 0.0)
            {
                [_topBar setFrame:CGRectMake(_topBar.frame.origin.x, _topBar.frame.origin.y - _topBar.frame.size.height,
                                             _topBar.frame.size.width, _topBar.frame.size.height)];
                [_canvas setFrame:CGRectMake(0.0, 0.0, _canvas.frame.size.width, _canvas.frame.size.height)];
                [_shadow setFrame:_canvas.frame];
            }
        }
        else
        {
            [_topBar setFrame:CGRectMake(_topBar.frame.origin.x, _topBar.frame.origin.y + _topBar.frame.size.height,
                                         _topBar.frame.size.width, _topBar.frame.size.height)];
            [_canvas setFrame:CGRectMake(0.0, 50.0, _canvas.frame.size.width, _canvas.frame.size.height)];
            [_shadow setFrame:_canvas.frame];
        }
    }
}

#pragma mark -- IBAction functions
- (void)purchaseMe:(id)sender
{
    NSString *key = [NSString stringWithFormat:@"%@.%@%zd", editViewDictionary[@"BundleIdentifier"], currentCollection, packID];
    NSSet *iapSet = [NSSet setWithObjects:key, nil];
    
    [[IAPHelper sharedHelper] retrieveProduct:iapSet];
    [collectionIndicator startAnimating];
}

- (IBAction)goStartOver:(id)sender
{
    CGRect overlayTitleFrame = _overlayTitle.frame;
    CGRect overlayNoFrame = _buttonNo.frame;
    CGRect overlayYesFrame = _buttonYes.frame;
    
//    overlayTitleFrame = CGRectMake(overlayTitleFrame.origin.x, overlayTitleFrame.origin.y + self.view.frame.size.height,
//                                   overlayTitleFrame.size.width, overlayTitleFrame.size.height);
//
//    overlayNoFrame = CGRectMake(overlayNoFrame.origin.x, overlayNoFrame.origin.y - self.view.frame.size.height,
//                                overlayNoFrame.size.width, overlayNoFrame.size.height);
//
//    overlayYesFrame = CGRectMake(overlayYesFrame.origin.x, overlayYesFrame.origin.y - self.view.frame.size.height,
//                                 overlayYesFrame.size.width, overlayYesFrame.size.height);
//
    _overlay.hidden = NO;
    _overlayTitle.hidden = NO;
    _buttonNo.hidden = NO;
    _buttonYes.hidden = NO;
    _buttonRestart.enabled = NO;
    
    [self captureBlur];
    [UIView animateWithDuration:0.8
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_overlay setAlpha:0.5];
                         [_overlayTitle setFrame:overlayTitleFrame];
                         [_buttonNo setFrame:overlayNoFrame];
                         [_buttonYes setFrame:overlayYesFrame];
                     }completion:^(BOOL finished){
                     }];
    [self goBlur];
    NSLog(@"Edit - Go Start Over Pressed");
}

- (IBAction)goMenu:(id)sender
{
    [self performSegueWithIdentifier:kToMenu sender:sender];
    
    NSLog(@"Edit - Go Menu Pressed");
}

- (IBAction)goFinish:(id)sender
{
    // Save Picture
    UIImage *photo = [self gimmeMyInstaPictureImage];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library saveImage:photo
               toAlbum:@"CollageFoto"
   withCompletionBlock:^(NSError *error) {
       if (error!=nil)
       {
           NSLog(@"Big error: %@", [error description]);
       }
   }];
    
    // Perform transition
    [_savedPhoto setImage:photo];
    [_savedPhoto setFrame:_canvas.frame];
    [_savedPhoto setHidden:YES];
    
    /*
    POPSpringAnimation *topBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    topBarAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    topBarAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    topBarAnimation.springSpeed = 7;
    topBarAnimation.springBounciness = 15;
    [_topBar.layer pop_addAnimation:topBarAnimation forKey:@"TopBarAnimationScale"];
    
    POPSpringAnimation *bottomBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    bottomBarAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    bottomBarAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    bottomBarAnimation.springSpeed = 7;
    bottomBarAnimation.springBounciness = 15;
    [_bottomBar.layer pop_addAnimation:bottomBarAnimation forKey:@"BottomBarAnimationScale"];
    */
    
    POPBasicAnimation *editViewAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    editViewAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    editViewAnimation.delegate = self;
    editViewAnimation.name = @"EditViewAnimationScale";
    [_editView.layer pop_addAnimation:editViewAnimation forKey:@"EditViewAnimationScale"];
    
    /*
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_savedPhoto setTransform:CGAffineTransformScale(_savedPhoto.transform, 1.2, 1.2)];
                     }completion:^(BOOL finished){
                         [UIView animateWithDuration:0.3
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              [_editView setFrame:CGRectMake(rectPoint.x - rectSize.width, rectPoint.y, rectSize.width, rectSize.height)];
                                          }completion:^(BOOL finished){
                                              [self performSegueWithIdentifier:kToSave sender:nil];
                                          }];
                     }];*/
    
    NSLog(@"Edit - Save Pic and Transition");
}

- (IBAction)startOverClose:(id)sender
{
    CGRect overlayTitleFrame = _overlayTitle.frame;
    CGRect overlayNoFrame = _buttonNo.frame;
    CGRect overlayYesFrame = _buttonYes.frame;
    
    overlayTitleFrame = CGRectMake(overlayTitleFrame.origin.x, overlayTitleFrame.origin.y - self.view.frame.size.height,
                                   overlayTitleFrame.size.width, overlayTitleFrame.size.height);
    
    overlayNoFrame = CGRectMake(overlayNoFrame.origin.x, overlayNoFrame.origin.y + self.view.frame.size.height,
                                overlayNoFrame.size.width, overlayNoFrame.size.height);
    
    overlayYesFrame = CGRectMake(overlayYesFrame.origin.x, overlayYesFrame.origin.y + self.view.frame.size.height,
                                 overlayYesFrame.size.width, overlayYesFrame.size.height);
    
    [self backFromBlur];
    [UIView animateWithDuration:0.8
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_overlayTitle setFrame:overlayTitleFrame];
                         [_buttonNo setFrame:overlayNoFrame];
                         [_buttonYes setFrame:overlayYesFrame];
                         [_overlay setAlpha:0.0];
                         [self backFromBlur];
                     }completion:^(BOOL finished){
                          _overlay.hidden = YES;
                          _overlayTitle.hidden = YES;
                          _buttonNo.hidden = YES;
                          _buttonYes.hidden = YES;
                          _buttonRestart.enabled = YES;
                     }];
    
    NSLog(@"Edit - Start Over Closed");
}

- (IBAction)editDone:(id)sender
{
    CGRect editFrame = _edit.frame;
    CGRect bottomBarFrame = _bottomBar.frame;
    CGRect canvasFrame = _canvas.frame;
    float canvasY, availableHeight, spaceBetween;
    
//    availableHeight = [[UIScreen mainScreen] bounds].size.height - _topBar.frame.size.height - _colours.frame.size.height - 50.0;
//    spaceBetween = (availableHeight - 320.0)/2.0;
//    canvasY = _topBar.frame.size.height + spaceBetween;
//    canvasFrame = CGRectMake(canvasFrame.origin.x, canvasY, canvasFrame.size.width, canvasFrame.size.height);
//
//    editFrame = CGRectMake(editFrame.origin.x, editFrame.origin.y + editFrame.size.height,
//                           editFrame.size.width, editFrame.size.height);
//
//    bottomBarFrame = CGRectMake(bottomBarFrame.origin.x, bottomBarFrame.origin.y - bottomBarFrame.size.height,
//                                bottomBarFrame.size.width, bottomBarFrame.size.height);
//
    POPSpringAnimation *editAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    editAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _edit.frame.origin.y,
                                                                  _edit.frame.size.width, _edit.frame.size.height)];
    editAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _edit.frame.origin.y - _edit.frame.size.height,
                                                                _edit.frame.size.width, _edit.frame.size.height)];
    editAnimation.springSpeed = 8;
    editAnimation.springBounciness = 1;
    [_edit.layer pop_addAnimation:editAnimation forKey:@"EditAnimation"];
    
    POPSpringAnimation *coloursAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    coloursAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _colours.frame.origin.y,
                                                                     _colours.frame.size.width, _colours.frame.size.height)];
    coloursAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _colours.frame.origin.y + _colours.frame.size.height,
                                                                   _colours.frame.size.width, _colours.frame.size.height)];
    coloursAnimation.springSpeed = 8;
    coloursAnimation.springBounciness = 1;
    [_colours.layer pop_addAnimation:coloursAnimation forKey:@"ColoursAnimation"];
    
    POPSpringAnimation *topBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    topBarAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _topBar.frame.origin.y - _topBar.frame.size.height,
                                                                    _topBar.frame.size.width, _topBar.frame.size.height)];
    topBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _topBar.frame.origin.y + _topBar.frame.size.height,
                                                                  _topBar.frame.size.width, _topBar.frame.size.height)];
    topBarAnimation.springSpeed = 5;
    topBarAnimation.springBounciness = 15;
    topBarAnimation.beginTime = (CACurrentMediaTime() + 0.5);
    [_topBar.layer pop_addAnimation:topBarAnimation forKey:@"TopBarAnimation"];
    
    POPSpringAnimation *bottomBarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    bottomBarAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bottomBar.frame.origin.y + _bottomBar.frame.size.height,
                                                                       _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
    bottomBarAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, _bottomBar.frame.origin.y - _bottomBar.frame.size.height,
                                                                     _bottomBar.frame.size.width, _bottomBar.frame.size.height)];
    bottomBarAnimation.springSpeed = 5;
    bottomBarAnimation.springBounciness = 15;
    bottomBarAnimation.beginTime = (CACurrentMediaTime() + 0.5);
    [_bottomBar.layer pop_addAnimation:bottomBarAnimation forKey:@"BottomBarAnimation"];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_canvas setFrame:canvasFrame];
                         [_shadow setFrame:canvasFrame];
                     }completion:^(BOOL finished){
                                              editMode = NO;
                                              nextTapIndex = 0;
                                              for (Sticker *sticker in stickerArray)
                                              {
                                                  [sticker removeGestures];
                                                  [self.view sendSubviewToBack:_canvas];
                                              }
                                              currentCollection = @"Nothing";
                                          }];
    
    NSLog(@"Edit - Editing Done");
}

- (IBAction)functionButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    CGRect collectionFrame = _collection.frame;
    CGRect barFrame = collectionTopBar.frame;
    
//    collectionFrame = CGRectMake(collectionFrame.origin.x, collectionFrame.origin.y - self.view.frame.size.height,
//                                 collectionFrame.size.width, collectionFrame.size.height);
    
    barFrame = CGRectMake(barFrame.origin.x, barFrame.origin.y + barFrame.size.height,
                          barFrame.size.width, barFrame.size.height);
    
    _overlay.hidden = NO;
    _collection.hidden = NO;
    aviary = NO;
    collectionTopBar.hidden = NO;
    collectionPurchase.hidden = YES;
    switch (button.tag)
    {
        case 1:
            collectionText.text = NSLocalizedString(@"EDIT_BORDERS", @"Edit - Borders");
            currentCollection = @"Instaframe";
            
//            edited by Monir
//            [NSTimer scheduledTimerWithTimeInterval: 1.5
//                                             target: self
//                                           selector:@selector(showInterstitial)
//                                           userInfo: nil repeats:NO];
//
            break;
            
        case 2:
            collectionText.text = NSLocalizedString(@"EDIT_DOODLES", @"Edit - Doodles");
            currentCollection = @"Doodles";
            break;
            
        case 3:
            collectionText.text = NSLocalizedString(@"EDIT_QUOTES", @"Edit - Quotes");
            currentCollection = @"Quotes";
            break;
            
        case 4:
            collectionText.text = NSLocalizedString(@"EDIT_FONTS", @"Edit - Fonts");
            currentCollection = @"Fonts";
            break;
            
        case 5:
            currentCollection = @"Aviary";
            aviary = YES;
            break;
            
        default:
            break;
    }
    
    if (!aviary)
    {
        isPack = YES;
        exitCollection = NO;
        [self captureBlur];

        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [_overlay setAlpha:0.5];
                             [_collection setFrame:collectionFrame];
                             [_collection reloadSections:[NSIndexSet indexSetWithIndex:0]];
                             [collectionTopBar setFrame:barFrame];
                         }completion:^(BOOL finished){
                         }];
        
        // For blur effect
        [self goBlur];
    }
    else
    {
        // kAviaryAPIKey and kAviarySecret are developer defined
        // and contain your API key and secret respectively
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [AFPhotoEditorController setAPIKey:kAviaryAPIKey secret:kAviarySecret];
        });
        
        AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:_photo.image];
        [editorController setDelegate:self];
        [self presentViewController:editorController animated:YES completion:nil];
    }
    
    functionTag = button.tag;
    
    NSLog(@"Edit - Current Collection: %@", currentCollection);
}



// edited by Monir
- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8340924500267413/6310811383"];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}


- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}


- (void)showInterstitial{
    
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"Ad wasn't ready");
    }
    NSLog(@"showInterstitial");
}
//



- (IBAction)addSticker:(id)sender
{
    CGFloat width = selectedSticker.initSize.width, height = selectedSticker.initSize.height;
    CGFloat centerX = _canvas.frame.size.width/2.0, centerY = _canvas.frame.size.height/2.0;
    
    CGRect centerFrame = CGRectMake(centerX - (width/2.0), centerY - (height/2.0), width, height);
    
    Sticker *sticker = [[Sticker alloc] initWithFrame:selectedSticker.frame];
    sticker.tag = selectedSticker.tag;
    sticker.delegate = self;
    if ([selectedSticker isImage])
    {
        [sticker giveImage:[self imageNamed:[NSString stringWithFormat:@"Pack%zd%@_%zd.png",
                                             selectedSticker.pack, selectedSticker.collection, selectedSticker.tag]
                                  withColor:[UIColor whiteColor]]
                          :selectedSticker.collection
                          :selectedSticker.pack];
    }
    else
    {
        [sticker giveText];
    }
    [sticker giveGestures];
    [sticker setInitSize:selectedSticker.initSize];
    [_canvas addSubview:sticker];
    [self.view bringSubviewToFront:sticker];
    
    [stickerArray addObject:sticker];
    [selectedSticker removeGestures];
    selectedSticker = sticker;
    
    [UIView animateWithDuration:1.0
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [selectedSticker setFrame:centerFrame];
                         // Need to animate label as well
                         if (![selectedSticker isImage])
                         {
                             [selectedSticker.label setFrame:CGRectMake(0.0, 0.0, width, height)];
                         }
                     }completion:^(BOOL finished){
                         
                     }];
    
    NSLog(@"Edit - Sticker added");
}

- (IBAction)tapCanvas:(UITapGestureRecognizer *)recognizer
{
    NSInteger j;
    BOOL restart = NO;
    CGPoint translation = [recognizer locationInView:[recognizer.view self]];
    
    if (![currentCollection isEqualToString:@"Instaframe"])
    {
        // If next tap index is more than sticker count, start again
        if (nextTapIndex >= [stickerArray count])
        {
            nextTapIndex = 0;
        }
        
        if (nextTapIndex != 0)
        {
            for (NSInteger i = nextTapIndex; i < [stickerArray count]; i++)
            {
                Sticker *sticker = [stickerArray objectAtIndex:i];
                // Found something
                if (CGRectContainsPoint(sticker.frame, translation))
                {
                    [selectedSticker removeGestures];
                    [sticker giveGestures];
                    [_canvas bringSubviewToFront:sticker];
                    
                    selectedSticker = sticker;
                    [selectedSticker iAmChosen];
                    _keyboard.hidden = selectedSticker.isImage;
                    _keyboardRightBar.hidden = _keyboard.hidden;
                    _add.hidden = NO;
                    _addRightBar.hidden = _add.hidden;
                    
                    if (!editMode)
                    {
                        [self enterEditMode];
                    }
                    nextTapIndex = i + 1;
                    break;
                }
                
                j = i + 1;
                if (j >= [stickerArray count])
                {
                    // Restart from 0
                    restart = YES;
                }
            }
            
            if (restart)
            {
                // If nextTapIndex is not 0, need to go through entire Array
                nextTapIndex = 0;
                [self tapCanvas:recognizer];
            }
        }
        else
        {
            for (NSInteger i = nextTapIndex; i < [stickerArray count]; i++)
            {
                Sticker *sticker = [stickerArray objectAtIndex:i];
                // Found something
                if (CGRectContainsPoint(sticker.frame, translation))
                {
                    [selectedSticker removeGestures];
                    [sticker giveGestures];
                    [_canvas bringSubviewToFront:sticker];
                    
                    selectedSticker = sticker;
                    [selectedSticker iAmChosen];
                    _keyboard.hidden = selectedSticker.isImage;
                    _keyboardRightBar.hidden = _keyboard.hidden;
                    _add.hidden = NO;
                    _addRightBar.hidden = _add.hidden;
                    
                    if (!editMode)
                    {
                        [self enterEditMode];
                    }
                    nextTapIndex = i + 1;
                    break;
                }
            }
        }
    }
    
    NSLog(@"Edit - Canvas tapped");}

- (IBAction)launchKeyboard:(id)sender
{
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 246.0, 320.0, 30.0)];
    [textField setBorderStyle:UITextBorderStyleRoundedRect];
    [textField setFont:[UIFont systemFontOfSize:15.0]];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setReturnKeyType:UIReturnKeyDone];
    [textField setText:selectedSticker.label.text];
    textField.delegate = self;
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [selectedSticker prepareForTyping];
    
    NSLog(@"Edit - Keyboard pressed");
}

- (IBAction)deleteBorder:(id)sender
{
    [_border setImage:nil];
    [self editDone:nil];
}

#pragma mark -- UITextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view bringSubviewToFront:_canvas];

    POPSpringAnimation *canvasAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    canvasAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, 0.0f, _canvas.frame.size.width, _canvas.frame.size.height)];
    canvasAnimation.springSpeed = 3;
    canvasAnimation.springBounciness = 8;
    [_canvas pop_addAnimation:canvasAnimation forKey:@"CanvasAnimation"];
    
    POPSpringAnimation *shadowAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    shadowAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0.0f, 0.0f, _shadow.frame.size.width, _shadow.frame.size.height)];
    shadowAnimation.springSpeed = 3;
    shadowAnimation.springBounciness = 8;
    [_shadow pop_addAnimation:shadowAnimation forKey:@"ShadowAnimation"];
    
    POPSpringAnimation *buttonKeyboardAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    buttonKeyboardAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(_keyboard.frame.origin.x, _keyboard.frame.origin.y - _keyboard.frame.size.height,
                                                                          _keyboard.frame.size.width, _keyboard.frame.size.height)];
    buttonKeyboardAnimation.springSpeed = 1;
    buttonKeyboardAnimation.springBounciness = 5;
    [_keyboard pop_addAnimation:buttonKeyboardAnimation forKey:@"ButtonKeyboardAnimation"];
    
    POPSpringAnimation *buttonAddAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    buttonAddAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(_add.frame.origin.x, _add.frame.origin.y - _add.frame.size.height,
                                                                     _add.frame.size.width, _add.frame.size.height)];
    buttonAddAnimation.springSpeed = 1;
    buttonAddAnimation.springBounciness = 5;
    [_add pop_addAnimation:buttonAddAnimation forKey:@"ButtonAddAnimation"];
    
    POPSpringAnimation *buttonEditDoneAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    buttonEditDoneAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(_buttonEditDone.frame.origin.x,
                                                                          _buttonEditDone.frame.origin.y - _buttonEditDone.frame.size.height,
                                                                          _buttonEditDone.frame.size.width, _buttonEditDone.frame.size.height)];
    buttonEditDoneAnimation.springSpeed = 1;
    buttonEditDoneAnimation.springBounciness = 5;
    [_buttonEditDone pop_addAnimation:buttonEditDoneAnimation forKey:@"ButtonEditDoneAnimation"];

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (![selectedSticker.label.text isEqualToString:@""])
    {
        [selectedSticker finishTyping];
        [textField resignFirstResponder];
        [textField removeFromSuperview];
        
        CGFloat availableHeight, spaceBetween, canvasY;
        CGRect canvasFrame;
        
        availableHeight = [[UIScreen mainScreen] bounds].size.height - _topBar.frame.size.height - _colours.frame.size.height - 50.0;
        spaceBetween = (availableHeight - 320.0)/2.0;
        canvasY = _topBar.frame.size.height + spaceBetween;
        canvasFrame = CGRectMake(_canvas.frame.origin.x, canvasY, _canvas.frame.size.width, _canvas.frame.size.height);
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [_canvas setFrame:canvasFrame];
                             [_shadow setFrame:canvasFrame];
                         }completion:^(BOOL finished){
                             [self.view sendSubviewToBack:_canvas];
                         }];
        
        
        
        POPSpringAnimation *buttonKeyboardAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
        buttonKeyboardAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(_keyboard.frame.origin.x, _keyboard.frame.origin.y + _keyboard.frame.size.height,
                                                                              _keyboard.frame.size.width, _keyboard.frame.size.height)];
        buttonKeyboardAnimation.springSpeed = 1;
        buttonKeyboardAnimation.springBounciness = 5;
        [_keyboard pop_addAnimation:buttonKeyboardAnimation forKey:@"ButtonKeyboardAnimation"];
        
        POPSpringAnimation *buttonAddAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
        buttonAddAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(_add.frame.origin.x, _add.frame.origin.y + _add.frame.size.height,
                                                                         _add.frame.size.width, _add.frame.size.height)];
        buttonAddAnimation.springSpeed = 1;
        buttonAddAnimation.springBounciness = 5;
        [_add pop_addAnimation:buttonAddAnimation forKey:@"ButtonAddAnimation"];
        
        POPSpringAnimation *buttonEditDoneAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
        buttonEditDoneAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(_buttonEditDone.frame.origin.x,
                                                                              _buttonEditDone.frame.origin.y + _buttonEditDone.frame.size.height,
                                                                              _buttonEditDone.frame.size.width, _buttonEditDone.frame.size.height)];
        buttonEditDoneAnimation.springSpeed = 1;
        buttonEditDoneAnimation.springBounciness = 5;
        [_buttonEditDone pop_addAnimation:buttonEditDoneAnimation forKey:@"ButtonEditDoneAnimation"];
        
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [selectedSticker typingText:newString];
    return YES;
}

#pragma mark -- Segue functions
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kToMenu])
    {
        NSLog(@"Edit - Back to Menu");
    }
    else if ([[segue identifier] isEqualToString:kToSave])
    {
        [segue.destinationViewController gimmePhoto:_savedPhoto];
        NSLog(@"Edit - Transition to Saved");
    }

}

#pragma mark -- Public functions
- (void)gimmeEditPhoto:(UIImage *)image
{
    photoImage = image;
}

- (void)purchaseSuccess
{
    collectionPurchase.hidden = YES;
    [collectionIndicator stopAnimating];
}

- (void)purchaseFail
{
    [collectionIndicator stopAnimating];
}

+ (EditViewController *)sharedLayer
{
    return layer;
}

#pragma mark -- Sticker Delegate
- (void)removeFromStickerArray
{
    [stickerArray removeObject:selectedSticker];
    
    if ([stickerArray count] != 0)
    {
        selectedSticker = [stickerArray objectAtIndex:0];
    }
    else
    {
        [selectedSticker removeAllSubviews];
        [selectedSticker removeFromSuperview];
        selectedSticker = nil;
    }
}

#pragma mark -- Aviary Delegate
- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image
{
    // Handle the result image here
    [_photo setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Edit - Aviary finished");
}

- (void)photoEditorCanceled:(AFPhotoEditorController *)editor
{
    // Handle cancellation here
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Edit - Aviary cancelled");
}

#pragma mark -- POP Animation
- (void)pop_animationDidStop:(POPAnimation *)anim finished:(BOOL)finished
{
    if ([anim.name isEqualToString:@"EditViewAnimationScale"])
    {
        [self performSegueWithIdentifier:kToSave sender:nil];
    }
    
    [anim pop_removeAnimationForKey:anim.name];
}
@end
