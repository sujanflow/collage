//
//  FontPack.h
//  pb2free
//
//  Created by Kevin Chee on 26/09/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface FontPack : NSObject

+ (NSString *)findFont:(NSInteger)tag;
+ (int)totalColour;
+ (UIColor *)findColour:(NSInteger)tag;

@end
