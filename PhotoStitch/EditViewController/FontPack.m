//
//  FontPack.m
//  pb2free
//
//  Created by Kevin Chee on 26/09/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "FontPack.h"

@implementation FontPack

+ (NSString *)findFont:(NSInteger)tag
{
    NSString *name;
    
    switch (tag)
    {
        case 1:
            name = @"AmericanTypewriter-Bold";
            break;
            
        case 2:
            name = @"AppleGothic";
            break;
            
        case 3:
            name = @"Segoe Print";
            break;
            
        case 4:
            name = @"Courier-Oblique";
            break;
            
        case 5:
            name = @"Kingthings Trypewriter 2";
            break;
            
        case 6:
            name = @"Luckiest Guy";
            break;
            
        case 7:
            name = @"Ostrich Sans";
            break;
            
        case 8:
            name = @"Pacifico";
            break;
            
        case 9:
            name = @"Matiz";
            break;
            
        case 10:
            name = @"Impact Label";
            break;
            
        case 11:
            name = @"Helsinki";
            break;
            
        case 12:
            name = @"Lilly";
            break;
            
        case 13:
            name = @"NeoRetroDraw";
            break;
            
        case 14:
            name = @"Avenir";
            break;
            
        case 15:
            name = @"Superclarendon";
            break;
            
        case 16:
            name = @"Courier";
            break;
            
        case 17:
            name = @"Baskerville";
            break;
            
        case 18:
            name = @"Banana Brick";
            break;
            
        case 19:
            name = @"Orbitron";
            break;
            
        case 20:
            name = @"MakibaFont";
            break;
            
        case 21:
            name = @"analog";
            break;
            
        case 22:
            name = @"Cochin-Italic";
            break;
            
        default:
            name = @"Verdana-BoldItalic";
            break;
    }
    
    return name;
}

+ (int)totalColour
{
    int total = 35;
    return total-1;
}

+ (UIColor *)findColour:(NSInteger)tag
{
    NSArray *array = [NSArray arrayWithObjects:
                      [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:64/255.0 green:64/255.0 blue:64/255.0 alpha:1.0],
                      [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0],
                      [UIColor colorWithRed:160/255.0 green:160/255.0 blue:160/255.0 alpha:1.0],
                      [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:51/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:102/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:204/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:128/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:204/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:228/255.0 green:176/255.0 blue:74/255.0 alpha:1.0],
                      [UIColor colorWithRed:100/255.0 green:59/255.0 blue:15/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:51/255.0 blue:51/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:102/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:153/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:102/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:204/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:157/255.0 green:213/255.0 blue:42/255.0 alpha:1.0],
                      [UIColor colorWithRed:153/255.0 green:255/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:51/255.0 green:0/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:115/255.0 green:44/255.0 blue:123/255.0 alpha:1.0],
                      [UIColor colorWithRed:204/255.0 green:153/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:238/255.0 green:219/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:190/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:255/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:255/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:76/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:102/255.0 blue:204/255.0 alpha:1.0],
                      [UIColor colorWithRed:51/255.0 green:153/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:120/255.0 green:213/255.0 blue:227/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:0/255.0 blue:128/255.0 alpha:1.0],
                      [UIColor colorWithRed:242/255.0 green:0/255.0 blue:86/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:174/255.0 blue:174/255.0 alpha:1.0], // 35
                      nil];

    return array[tag];
}

@end
