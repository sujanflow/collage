//
//  Sticker.h
//  stickem2
//
//  Created by Kevin Chee on 30/05/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StickerDelegate

- (void)removeFromStickerArray;

@end

@interface Sticker : UIView
<UIGestureRecognizerDelegate>
{
    UITapGestureRecognizer *tap;
    UIPanGestureRecognizer *pan;
    UIRotationGestureRecognizer *rotate;
    UIPinchGestureRecognizer *pinch;
    UIPanGestureRecognizer *customRotate;
    CGPoint touchLocation;
    CGRect initialBounds;
    CGFloat initialDistance;
    CGFloat fontSize;
}

@property (strong, nonatomic) UIImageView *resizingControl;
@property (strong, nonatomic) UIImageView *deleteControl;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UILabel *imageLabel;
@property (nonatomic, assign) UIViewController <StickerDelegate> *delegate;
@property (nonatomic) float deltaAngle;
@property (nonatomic) BOOL isImage;
@property (nonatomic) NSString *collection;
@property (nonatomic) NSInteger pack;
@property (nonatomic) CGSize initSize;

- (void)giveImage:(UIImage *)image :(NSString *)collection :(NSInteger)pack;
- (void)changeImage:(UIImage *)image;
- (void)giveText;
- (void)prepareForTyping;
- (void)finishTyping;
- (void)typingText:(NSString *)string;
- (void)changeTextColor:(UIColor *)color;
- (void)removeGestures;
- (void)removeAllButPanGesture;
- (void)removeDelete;
- (void)byebyeLayers;
- (void)gimmeLayers;
- (void)giveGestures;
- (void)iAmChosen;
- (void)removeAllSubviews;

@end
