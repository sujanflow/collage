//
//  Sticker.m
//  stickem2
//
//  Created by Kevin Chee on 30/05/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "Sticker.h"
#import <QuartzCore/QuartzCore.h>
#import "EditViewController.h"
#import "FontPack.h"

#define INSET   36.0

CG_INLINE CGPoint CGRectGetCenter(CGRect rect)
{
    return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}

CG_INLINE CGRect CGRectScale(CGRect rect, CGFloat wScale, CGFloat hScale)
{
    return CGRectMake(rect.origin.x * wScale, rect.origin.y * hScale, rect.size.width * wScale, rect.size.height * hScale);
}

CG_INLINE CGFloat CGPointGetDistance(CGPoint point1, CGPoint point2)
{
    //Saving Variables.
    CGFloat fx = (point2.x - point1.x);
    CGFloat fy = (point2.y - point1.y);
    
    return sqrt((fx*fx + fy*fy));
}

CG_INLINE CGFloat CGAffineTransformGetAngle(CGAffineTransform t)
{
    return atan2(t.b, t.a);
}

@implementation Sticker

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panEm:)];
        pan.delegate = self;
        [self addGestureRecognizer:pan];
        
        [self removeGestures];
        [self setUserInteractionEnabled:YES];
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)addResizeButton
{
    _resizingControl = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width-INSET, self.bounds.size.height-INSET,
                                                                    INSET, INSET)];
    NSLog(@"Resize : %@ Bounds: %@", NSStringFromCGSize(self.frame.size), NSStringFromCGSize(self.bounds.size));
    [_resizingControl setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin)];
    _resizingControl.backgroundColor = [UIColor clearColor];
    _resizingControl.image = [UIImage imageNamed:@"ZDStickerView.bundle/ZDBtn2.png"];
    _resizingControl.userInteractionEnabled = YES;
    
    [self addSubview:_resizingControl];
    
    _deltaAngle = atan2(self.frame.origin.y+self.frame.size.height - self.center.y,
                        self.frame.origin.x+self.frame.size.width - self.center.x);
    
    customRotate = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resizeTranslate:)];
    [_resizingControl addGestureRecognizer:customRotate];
}

- (void)addDeleteButton
{
    _deleteControl = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, INSET, INSET)];
    [_deleteControl setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin)];
    _deleteControl.backgroundColor = [UIColor clearColor];
    _deleteControl.image = [UIImage imageNamed:@"ZDStickerView.bundle/ZDBtn3.png"];
    _deleteControl.userInteractionEnabled = YES;
    
    [self addSubview:_deleteControl];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEm:)];
    [tap setNumberOfTapsRequired:1];
    [_deleteControl addGestureRecognizer:tap];
}

- (IBAction)tapEm:(UITapGestureRecognizer *)recognizer
{
    EditViewController *del = (EditViewController *)_delegate;
    
    [self removeFromSuperview];
    [del removeFromStickerArray];
}

- (IBAction)panEm:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.superview];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.superview];
}

- (IBAction)rotateEm:(UIRotationGestureRecognizer *)recognizer
{    
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
    recognizer.rotation = 0;
}

- (IBAction)pinchEm:(UIPinchGestureRecognizer *)recognizer
{
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}

-(IBAction)resizeTranslate:(UIPanGestureRecognizer *)recognizer
{
    touchLocation = [recognizer locationInView:self.superview];
    
    CGPoint center = CGRectGetCenter(self.frame);
    
    if ([recognizer state] == UIGestureRecognizerStateBegan)
    {
        _deltaAngle = atan2(touchLocation.y-center.y, touchLocation.x-center.x)-CGAffineTransformGetAngle(self.transform);
        
        initialBounds = self.bounds;
        initialDistance = CGPointGetDistance(center, touchLocation);
    }
    else if ([recognizer state] == UIGestureRecognizerStateChanged)
    {
        float ang = atan2(touchLocation.y-center.y, touchLocation.x-center.x);
        
        // Rotation
        float angleDiff = _deltaAngle - ang;
        [self setTransform:CGAffineTransformMakeRotation(-angleDiff)];
        [self setNeedsDisplay];
        
        //Finding scale between current touchPoint and previous touchPoint
        double scale = sqrtf(CGPointGetDistance(center, touchLocation)/initialDistance);
        
        CGRect scaleRect = CGRectScale(initialBounds, scale, scale);
        
        if (!_isImage)
        {
            [self setBounds:scaleRect];
            CGRect rect = CGRectMake(0.0, 0.0, self.bounds.size.width, self.bounds.size.height);
            [_label setFrame:rect];
            [_label setNeedsDisplay];
        }
        else if (scaleRect.size.width >= (1+INSET) && scaleRect.size.height >= (1+INSET))
        {
            [self setBounds:scaleRect];
        }
        
        [self setNeedsDisplay];
    }
    else if ([recognizer state] == UIGestureRecognizerStateEnded)
    {
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

- (void)giveImage:(UIImage *)image :(NSString *)collection :(NSInteger)pack
{
    CGFloat width = self.bounds.size.width - (INSET);
    CGFloat height = self.bounds.size.height - (INSET);
    _collection = collection;
    _pack = pack;
    
    CGRect frame = CGRectMake(INSET/2.0, INSET/2.0, width, height);
    
    _imageView = [[UIImageView alloc] initWithFrame:frame];
    [_imageView setImage:image];
    [_imageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    [_imageView setUserInteractionEnabled:YES];
    [_imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self insertSubview:_imageView atIndex:0];
    [self addDeleteButton];
    [self addResizeButton];
    
    _isImage = YES;
}

- (void)changeImage:(UIImage *)image
{
    [_imageView setImage:image];
}

- (void)giveText;
{
    CGFloat height = self.bounds.size.height;
    
    // Set up label to add to subview
    _label = [[UILabel alloc] init];
    [_label setText:@"ABC"];
    [_label setTextColor:[UIColor whiteColor]];
    [_label setBackgroundColor:[UIColor clearColor]];
    [_label setAdjustsFontSizeToFitWidth:YES];
    [_label setMinimumScaleFactor:0.2];
    [_label setNumberOfLines:1];
    [_label setTextAlignment:NSTextAlignmentCenter];
    [_label setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
    // Look for current font size based on frame size
    fontSize = 500.0;
    while (fontSize > 0.0)
    {
        //CGSize size = [_label.text sizeWithFont:[UIFont fontWithName:[FontPack findFont:self.tag] size:fontSize]
        //                      constrainedToSize:CGSizeMake(CGFLOAT_MAX, height)];
        
        CGRect rect = [_label.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height)
                                                options:NSStringDrawingUsesFontLeading
                                             attributes:@{NSFontAttributeName:[UIFont fontWithName:[FontPack findFont:self.tag] size:fontSize]}
                                                context:nil];
        
        if (rect.size.height <= height)
        {
            [_label setFont:[UIFont fontWithName:[FontPack findFont:self.tag] size:fontSize]];
            [_label setFrame:CGRectMake(0.0, 0.0, rect.size.width, rect.size.height)];
            break;
        }
        
        fontSize -= 1.0;
    }
    [self insertSubview:_label atIndex:0];
    
    // Update self bounds to match labels
    [self setBounds:_label.frame];
    
    // Create hidden label to make into image
    _imageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, _label.frame.size.width*4.0, _label.frame.size.height*4.0)];
    [_imageLabel setText:_label.text];
    [_imageLabel setTextColor:[UIColor whiteColor]];
    [_imageLabel setBackgroundColor:[UIColor clearColor]];
    [_imageLabel setAdjustsFontSizeToFitWidth:YES];
    [_imageLabel setMinimumScaleFactor:0.2];
    [_imageLabel setNumberOfLines:1];
    [_imageLabel setTextAlignment:NSTextAlignmentCenter];
    [_imageLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
    // Look for current font size based on frame size
    fontSize = 500.0;
    while (fontSize > 0.0)
    {
        //CGSize size = [_label.text sizeWithFont:[UIFont fontWithName:_label.font.fontName size:fontSize]
        //                      constrainedToSize:CGSizeMake(CGFLOAT_MAX, _imageLabel.frame.size.height)];
        
        CGRect rect = [_label.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, _imageLabel.frame.size.height)
                                                options:NSStringDrawingUsesFontLeading
                                             attributes:@{NSFontAttributeName:[UIFont fontWithName:_label.font.fontName size:fontSize]}
                                                context:nil];
        
        if (rect.size.height <= _imageLabel.frame.size.height)
        {
            [_imageLabel setFont:[UIFont fontWithName:_label.font.fontName size:fontSize]];
            [_imageLabel setFrame:CGRectMake(0.0, 0.0, rect.size.width, rect.size.height)];
            break;
        }
        
        fontSize -= 1.0;
    }

    // Add label to imageview
    _imageView = [[UIImageView alloc] initWithFrame:_label.frame];
    [_imageView setImage:[self getLabelContext]];
    [_imageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    [_imageView setUserInteractionEnabled:YES];
    [_imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self insertSubview:_imageView atIndex:0];
    
    //Hide label and add delete and resize button
    [_label setHidden:YES];
    [self addDeleteButton];
    [self addResizeButton];
    
    //CGSize size = [_label.text sizeWithFont:_label.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, _label.frame.size.height)];
    
    CGRect rect = [_label.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, _imageLabel.frame.size.height)
                                            options:NSStringDrawingUsesFontLeading
                                         attributes:@{NSFontAttributeName:[UIFont fontWithName:_label.font.fontName size:fontSize]}
                                            context:nil];
    
    NSLog(@"2- %@", NSStringFromCGSize(rect.size));
    
    _isImage = NO;
}

- (void)prepareForTyping
{
    // Look for current font size based on frame size
    fontSize = 500.0;
    if ([_label.text isEqualToString:@""])
    {
        fontSize = 30.0;
    }
    else
    {
        while (fontSize > 0.0)
        {
            CGRect rect = [_label.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, _imageLabel.frame.size.height)
                                                    options:NSStringDrawingUsesFontLeading
                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:_label.font.fontName size:fontSize]}
                                                    context:nil];
            
            if (rect.size.height <= _label.frame.size.height)
            {
                [_label setFont:[UIFont fontWithName:_label.font.fontName size:fontSize]];
                [_label setBounds:CGRectMake(0.0, 0.0, rect.size.width, rect.size.height)];
                break;
            }
            
            fontSize -= 1.0;
        }
    }

    // Hide imageview and show label
    [_imageView setHidden:YES];
    [_label setHidden:NO];
    
    // Hide delete and resize
    [_deleteControl setHidden:YES];
    [_resizingControl setHidden:YES];
}

- (void)finishTyping
{
    // Create hidden label to make into image
    [_imageLabel setText:_label.text];
    // Look for current font size based on frame size
    fontSize = 500.0;
    while (fontSize > 0.0)
    {
        CGRect rect = [_label.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, _imageLabel.frame.size.height)
                                                options:NSStringDrawingUsesFontLeading
                                             attributes:@{NSFontAttributeName:[UIFont fontWithName:_label.font.fontName size:fontSize]}
                                                context:nil];
        
        if (rect.size.height <= _imageLabel.frame.size.height)
        {
            [_imageLabel setFont:[UIFont fontWithName:_label.font.fontName size:fontSize]];
            [_imageLabel setFrame:CGRectMake(0.0, 0.0, rect.size.width, rect.size.height)];
            break;
        }
        
        fontSize -= 1.0;
    }
    
    // Change image for imageview
    [_imageView setImage:[self getLabelContext]];
    
    // Hide label and show image
    [_imageView setHidden:NO];
    [_label setHidden:YES];
    
    // Show delete and resize
    [_deleteControl setHidden:NO];
    [_resizingControl setHidden:NO];
}

- (void)typingText:(NSString *)string
{
    _label.text = string;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] init];
    NSDictionary *attribute = @{NSFontAttributeName : _label.font};
    [attributeString appendAttributedString:[[NSAttributedString alloc] initWithString:string attributes:attribute]];
    
    CGRect frame = [attributeString boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, _label.frame.size.height)
                                                 options:0
                                                 context:nil];
    
    [self setBounds:CGRectMake(self.bounds.origin.x, self.bounds.origin.y, frame.size.width, self.bounds.size.height)];
    [_label setFrame:CGRectMake(0.0, 0.0, frame.size.width, _label.frame.size.height)];
}

- (void)changeTextColor:(UIColor *)color
{
    [_label setTextColor:color];
    [_imageLabel setTextColor:color];
    [_imageView setImage:[self getLabelContext]];
}

- (UIImage *)getLabelContext
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0)
    {
        // Use Retina Display
        UIGraphicsBeginImageContextWithOptions(_imageLabel.frame.size, NO, 0.0);
    }
    else
    {
        UIGraphicsBeginImageContext(_imageLabel.frame.size);
    }
    [_imageLabel.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (void)removeGestures
{
    tap.enabled = NO;
    pan.enabled = NO;
    rotate.enabled = NO;
    pinch.enabled = NO;
    customRotate.enabled = NO;
    _resizingControl.hidden = YES;
    _deleteControl.hidden = YES;
    
}

- (void)removeAllButPanGesture
{
    tap.enabled = NO;
    pan.enabled = YES;
    rotate.enabled = NO;
    pinch.enabled = NO;
    customRotate.enabled = NO;
    _resizingControl.hidden = YES;
    _deleteControl.hidden = YES;
    
}

- (void)removeDelete
{
    _deleteControl.hidden = YES;
}

- (void)byebyeLayers
{
    if ([self.layer.sublayers count] > 1)
    {
        [[self.layer.sublayers objectAtIndex:[self.layer.sublayers count]-1] removeFromSuperlayer];
    }
}

- (void)gimmeLayers
{
    CAShapeLayer *border = [CAShapeLayer layer];
    border.strokeColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0].CGColor;
    border.fillColor = nil;
    border.lineDashPattern = @[@4,@2];
    [[self layer] addSublayer:border];
    border.path = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    border.frame = self.bounds;
}

- (void)giveGestures
{
    tap.enabled = YES;
    pan.enabled = YES;
    rotate.enabled = YES;
    pinch.enabled = YES;
    customRotate.enabled = YES;
    _resizingControl.hidden = NO;
    _deleteControl.hidden = NO;

}

- (void)iAmChosen
{
    initialBounds = self.bounds;
    CGRect scaleRect = CGRectScale(initialBounds, 1.1, 1.1);
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                        [self setBounds:scaleRect];
                     }completion:^(BOOL finished){
                         CGRect scaleRect = CGRectScale(initialBounds, 1.0, 1.0);

                         [UIView animateWithDuration:0.1
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              [self setBounds:scaleRect];
                                          }completion:^(BOOL finished){
                                              
                                          }];
                     }];
    
    [self setNeedsDisplay];
}

#pragma mark -- Photo Editing Extension
- (void)removeAllSubviews
{
    [_imageView removeFromSuperview];
    [_deleteControl removeFromSuperview];
    [_resizingControl removeFromSuperview];
}


@end
