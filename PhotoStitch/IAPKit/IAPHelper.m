#import "IAPHelper.h"

@implementation IAPHelper

static IAPHelper * _sharedHelper;

+ (IAPHelper *) sharedHelper {
    
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[IAPHelper alloc] init];
    return _sharedHelper;
    
}

- (id)init
{
    return self;    
}

@end
