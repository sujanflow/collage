//
//  IAPKit.h
//  PhotoStitch
//
//  Created by Kevin Chee on 10/04/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface IAPKit : NSObject
<SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
    NSDictionary *IAPDictionary;

    NSString *currentProduct;
}

- (BOOL)isProductPurchased:(NSString *)productIdentifier;
- (void)setProductPurchased:(NSString *)productIdentifier;
- (void)giveStarterPacks;
- (void)loadDictionary:(NSDictionary *) dict;

- (void)retrieveProduct:(NSSet *)set;
- (void)runPayment:(SKProduct *)product;
- (NSString *)getProductIDPurchased;

// App specific
- (void)restorePurchase;

@end
