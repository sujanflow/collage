//
//  IAPKit.m
//  PhotoStitch
//
//  Created by Kevin Chee on 10/04/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "EditViewController.h"
#import "CollageCropViewController.h"
#import "IAPKit.h"

@implementation IAPKit

- (id)init
{
    //Load Dictionary with wood name cross refference values for image name
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"ConfigPList" ofType:@"plist"];
    IAPDictionary = [[NSDictionary alloc] initWithContentsOfFile:plistCatPath];
    
    return self;
}

- (void)loadDictionary:(NSDictionary *) dict
{
    IAPDictionary = dict;
}

#pragma mark -- NSUserDefaults
- (void)giveStarterPacks
{
    [self setProductPurchased:[NSString stringWithFormat:@"%@.Instaframe0", IAPDictionary[@"BundleIdentifier"]]];
    [self setProductPurchased:[NSString stringWithFormat:@"%@.Doodles0", IAPDictionary[@"BundleIdentifier"]]];
    [self setProductPurchased:[NSString stringWithFormat:@"%@.Quotes0", IAPDictionary[@"BundleIdentifier"]]];
}

- (BOOL)isProductPurchased:(NSString *)productIdentifier
{
    NSLog(@"Checking IAP Product: %@", productIdentifier);
    
    NSUserDefaults *userDefault = [[NSUserDefaults alloc] initWithSuiteName:IAPDictionary[@"GroupEntitlement"]];
    
    return [userDefault boolForKey:productIdentifier];
}

- (void)setProductPurchased:(NSString *)productIdentifier
{
    NSUserDefaults *userDefault = [[NSUserDefaults alloc] initWithSuiteName:IAPDictionary[@"GroupEntitlement"]];

    [userDefault setBool:YES forKey:productIdentifier];
    [userDefault synchronize];
    
    NSLog(@"IAP Product purchased: %@", productIdentifier);
}


#pragma mark -- Retrieve Product from a given set
- (void)retrieveProduct:(NSSet *)set
{
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
    request.delegate = self;
    [request start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSLog(@"Received Response");
    NSArray *products = response.products;
    // Select only 1 item
    SKProduct *processProduct = [products objectAtIndex:0];
    
    // Process payment
    [self runPayment:processProduct];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"Error retrieving product : %@", [error localizedDescription]);
    UIAlertView *tmp = [[UIAlertView alloc]
                        initWithTitle:@"An Error Occurred"
                        message:@"Please try again later."
                        delegate:self
                        cancelButtonTitle:nil
                        otherButtonTitles:@"OK", nil];
    [tmp show];
    [[EditViewController sharedLayer] purchaseFail];
    [[CollageCropViewController sharedLayer] purchaseFail];
}

#pragma mark -- Payment
- (void)runPayment:(SKProduct *)product
{
    currentProduct = product.productIdentifier;
    
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment]; // <-- KA CHING!
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchasing:
            {
                break;
            }
            case SKPaymentTransactionStatePurchased:
            {
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [[EditViewController sharedLayer] purchaseSuccess];
                [[CollageCropViewController sharedLayer] purchaseSuccess];
                [self setProductPurchased:transaction.payment.productIdentifier];
                
                UIAlertView *tmp = [[UIAlertView alloc]
                                    initWithTitle:@"Purchase completed"
                                    message:@"Pack successfully purchased!"
                                    delegate:self
                                    cancelButtonTitle:@"OK"
                                    otherButtonTitles:nil];
                [tmp show];
                
                break;
            }
            case SKPaymentTransactionStateRestored:
            {
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
            case SKPaymentTransactionStateFailed:
            {
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [[EditViewController sharedLayer] purchaseFail];
                [[CollageCropViewController sharedLayer] purchaseFail];
                
                if (transaction.error.code != SKErrorPaymentCancelled)
                {
                    UIAlertView *tmp = [[UIAlertView alloc]
                                        initWithTitle:@"An Error Occurred"
                                        message:@"Your transaction has been cancelled. Please try again later."
                                        delegate:self
                                        cancelButtonTitle:nil
                                        otherButtonTitles:@"OK", nil];
                    [tmp show];
                }
                NSLog(@"%@", transaction.error);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IAPCancelled" object:nil userInfo:nil];
                break;
            }
            default:
                break;
        }
    }    
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            // Later
            [[NSNotificationCenter defaultCenter] postNotificationName:@"IAPComplete" object:nil userInfo:nil];
            break;
           
        case 1:
            // Review
            [[NSNotificationCenter defaultCenter] postNotificationName:@"IAPReview" object:nil userInfo:nil];
            break;
            
        case 2:
            // Facebook
            [[NSNotificationCenter defaultCenter] postNotificationName:@"IAPFacebook" object:nil userInfo:nil];
            break;
            
        default:
            break;
    }
}

- (NSString *)getProductIDPurchased
{
    return currentProduct;
}

#pragma mark -- Restore purchase
- (void)restorePurchase
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    BOOL restore = NO;
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        [self setProductPurchased:transaction.payment.productIdentifier];
        restore = YES;
    }
    
    if (restore)
    {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Purchases Restored"
                            message:@"Please continue to support us by leaving us a review! Like us on Facebook for updates!"
                            delegate:self
                            cancelButtonTitle:@"OH YEAH"
                            otherButtonTitles:nil];
        [tmp show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Unable to Restore"
                              message:@"You can only restore if you have previously made a purchase"
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"IAPCancelled" object:nil userInfo:nil];
}

@end
