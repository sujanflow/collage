//
//  Languages.h
//  piceditor7
//
//  Created by Kevin Chee on 20/04/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Languages : NSObject

+ (NSString *)currentLocale;
+ (float)labelSizeDefault;
+ (float)navigatorSizeDefault;
+ (float)photoLabelSizeDecrease;
+ (float)cropTitleSizeIncrease;
+ (float)cropMessageSizeIncrease;
+ (float)editTitleSizeIncrease;
+ (float)editCollecitonTitleSizeIncrease;
+ (float)savedMessageSizeIncrease;

@end
