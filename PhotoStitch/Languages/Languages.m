//
//  Languages.m
//  piceditor7
//
//  Created by Kevin Chee on 20/04/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import "Languages.h"

@implementation Languages

#pragma mark Current locale

+ (NSString *)currentLocale
{
    NSString *locale = [[NSLocale preferredLanguages] objectAtIndex:0];
    return locale;
}

#pragma mark App Delegate

+ (float)labelSizeDefault
{
    float label = 12.0;
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        label = 12.0;
    }
    
    return label;
}

+ (float)navigatorSizeDefault
{
    float nav = 22.0;
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        nav = 22.0;
    }
    
    return nav;
}

#pragma mark Photo Album

+ (float)photoLabelSizeDecrease
{
    float increase = 0.0;
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        increase = 2.0;
    }
    
    return increase;
}


#pragma mark Crop

+ (float)cropTitleSizeIncrease
{
    float increase = 13.0;
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        increase = 13.0;
    }
    
    return increase;
}

+ (float)cropMessageSizeIncrease
{
    float increase = 0.0;
    /*
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        increase = 4.0;
    }
    else if ([lang isEqualToString:@"ja"])
    {
        increase = -2.0;
    }
    */
    
    return increase;
}


#pragma mark Edit

+ (float)editTitleSizeIncrease
{
    float increase = 13.0;
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        increase = 13.0;
    }
    
    return increase;
}

+ (float)editCollecitonTitleSizeIncrease
{
    float increase = 13.0;
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        increase = 13.0;
    }
    
    return increase;
}

#pragma mark Saved

+ (float)savedMessageSizeIncrease
{
    float increase = 5.0;
    /*
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([lang isEqualToString:@"zh-Hans"])
    {
        increase = 3.0;
    }
    */
    
    return increase;
}
@end
