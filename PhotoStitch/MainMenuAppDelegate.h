//
//  MainMenuAppDelegate.h
//  pb2free
//
//  Created by Kevin Chee on 5/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Chartboost/Chartboost.h>
#import <StoreKit/StoreKit.h>

@interface MainMenuAppDelegate : UIResponder <UIApplicationDelegate, ChartboostDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
