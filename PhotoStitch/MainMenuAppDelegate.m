//
//  MainMenuAppDelegate.m
//  pb2free
//
//  Created by Kevin Chee on 5/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "MainMenuAppDelegate.h"
#import "Ads.h"
#import "Languages.h"
#import "LocalNotification.h"
#import <Chartboost/Chartboost.h>
#import <StoreKit/StoreKit.h>


@implementation MainMenuAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    int shortestTime = 50;
    int longestTime = 100;
    int timeInterval = arc4random_uniform(longestTime - shortestTime) + shortestTime;
    
    [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(requestReview) userInfo:nil repeats:NO];

    // Determine storyboard
    UIStoryboard *mainStoryBoard = nil;
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        // Load resources for iOS 6.1 or earlier
        NSLog(@"iOS 6.1");
    }
    else
    {
        // Load resources for iOS 7 or later
        NSLog(@"iOS 7.0");
    }
    
    if (screenSize.height >= 568)
    {
        mainStoryBoard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    }
    else
    {
        mainStoryBoard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    }

    self.window.rootViewController = [mainStoryBoard instantiateInitialViewController];
    [self.window makeKeyAndVisible];
    
    // Gets list of font names available including custom font
    NSLog(@"%@", [UIFont familyNames]);
    NSLog(@"Locale: %@", [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode]);
    NSLog(@"%@", [Languages currentLocale]);

    // Adjust font size according to Locale
    float fontSize = [Languages labelSizeDefault];
    float textFont = [Languages navigatorSizeDefault];
    [[UILabel appearance] setFont:[UIFont fontWithName:@"Roboto Condensed" size:fontSize]];
    NSDictionary *textAttributes = @{NSForegroundColorAttributeName :[UIColor whiteColor],
                                     NSFontAttributeName            :[UIFont fontWithName:@"Roboto Condensed" size:textFont]};
    
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
       
    
    // Initialise AppLovin
   // [Ads appLovinInit];
    
    // Cancel all scheduled local notifications
    [LocalNotification cancel];
    
    return YES;
}
- (void)requestReview {
    [SKStoreReviewController requestReview];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [LocalNotification scheduleNotification];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //[Ads playHavenOpenCall];
    
    // Local notification
    [LocalNotification resetBadge:application];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [LocalNotification resetBadge:application];
}

@end
