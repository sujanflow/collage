//
//  MainMenuViewController.h
//  pb2free
//
//  Created by Kevin Chee on 5/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPKit.h"
#import <MessageUI/MessageUI.h>
#import "POP.h"


#import <GoogleMobileAds/GADBannerView.h>
#import "AdManager.h"



@interface MainMenuViewController : UIViewController <UIAlertViewDelegate, MFMailComposeViewControllerDelegate, POPAnimationDelegate>
{
    float menuMiddleHeight;
    NSDictionary *mainMenuDictionary;
    
    IAPKit *iap;
    BOOL nextPicUp;
    int featuredIndex;
     GADBannerView *bannerView_;
}
@property (nonatomic, assign) BOOL isSomethingEnabled;
@property (nonatomic, retain) IBOutlet UIView *featured;
@property (nonatomic, retain) IBOutlet UIImageView *menuBg;
@property (nonatomic, retain) IBOutlet UIImageView *menuLogo;
@property (nonatomic, retain) IBOutlet UIButton *buttonPhoto;
@property (nonatomic, retain) IBOutlet UIButton *buttonCollage;
@property (nonatomic, retain) IBOutlet UIButton *buttonFeedback;
@property (nonatomic, retain) IBOutlet UIButton *buttonFreeGames;
@property (nonatomic, retain) IBOutlet UIButton *buttonRestore;
@property(nonatomic, strong) IBOutlet GADBannerView *adBanner;

- (IBAction)restorePurchase:(id) sender;
- (IBAction)moreGames:(id) sender;
- (IBAction)feedback:(id) sender;

@end
