//
//  MainMenuViewController.m
//  pb2free
//
//  Created by Kevin Chee on 5/08/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "MainMenuViewController.h"
#import "Ads.h"
#import "CollectionPack.h"
#import <Chartboost/Chartboost.h>
#import <AdSupport/AdSupport.h>
#import "MainMenuAppDelegate.h"

@import GoogleMobileAds;

@interface MainMenuViewController ()
<UIActionSheetDelegate,MFMailComposeViewControllerDelegate,ChartboostDelegate,UIAlertViewDelegate,GADInterstitialDelegate>

    @property(nonatomic, strong) GADInterstitial*interstitial;
@end

@implementation MainMenuViewController
@synthesize adBanner;
UIImage *tempImage;

- (void)viewDidLoad
{
    [super viewDidLoad];

    //Load Dictionary with wood name cross refference values for image name
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"ConfigPList" ofType:@"plist"];
    mainMenuDictionary = [[NSDictionary alloc] initWithContentsOfFile:plistCatPath];
    
    
    
     CGPoint origin;
     if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
     
     //is iPhone
     
     if ([[UIScreen mainScreen] bounds].size.height == 568){
     origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, 0);
     }
     else {
     
     if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0){
     origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2,0);
     //device with Retina Display
     }
     else{
         origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2,0);
     //no Retina Display
     
     }
     }
     }else  {
     
     //is iPad
     
     if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0){
     //device with Retina Display
     origin = CGPointMake(20,0);        }
     else{
     origin = CGPointMake(20,0);                //no Retina Display
     }
     }
//     bannerView_= [[AdManager sharedInstance] adMobBannerWithAdUnitID:admob_bottom_banner andOrigin:origin];
//     bannerView_.rootViewController = self;
//     [self.view addSubview:bannerView_];
    
 
    [Chartboost startWithAppId:@"59980dad04b0167bda6e474f"
                  appSignature:@"1eec915c5650aeeeba634044b5ccd48c6ecc3e84"
                      delegate:self];
    
   
    iap = [[IAPKit alloc] init];
//    [self setForAnimation];
    [self startAnimation];
    
    // Starter packs
    [iap giveStarterPacks];
}

//edited by Monir
- (void)viewWillAppear:(BOOL)animated
{
    self.interstitial = [self createAndLoadInterstitial];
    NSLog(@"Bool value: %d",_isSomethingEnabled);
    
    if (_isSomethingEnabled == YES){
    [NSTimer scheduledTimerWithTimeInterval: 1.0
                                     target: self
                                   selector:@selector(onTick)
                                   userInfo: nil repeats:NO];
        _isSomethingEnabled = NO;
    }
    
}

-(void)onTick{
    
        if (self.interstitial.isReady) {
            [self.interstitial presentFromRootViewController:self];
        } else {
            NSLog(@"Ad wasn't ready");
        }
}


- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8340924500267413/6310811383"];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}


- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}
//


-(void)dealloc {
    bannerView_.delegate = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)setForAnimation
//{
//    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
//    featuredIndex = 0;
//    // For iPhone 5
//    if (screenSize.height >= 568)
//    {
//        [_menuLogo setFrame:CGRectMake(_menuLogo.frame.origin.x, 220.0, _menuLogo.frame.size.width, _menuLogo.frame.size.height)];
//    }
//    else
//    {
//        [_menuLogo setFrame:CGRectMake(_menuLogo.frame.origin.x, 176.0, _menuLogo.frame.size.width, _menuLogo.frame.size.height)];
//    }
//    _buttonCollage.layer.opacity = 0.0f;
//    _buttonFeedback.layer.opacity = 0.0f;
//    _buttonFreeGames.layer.opacity = 0.0f;
//    _buttonRestore.layer.opacity = 0.0f;
//}

- (void)startAnimation
{
    POPDecayAnimation *animation = [POPDecayAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    animation.name = @"LogoAnimation";
    featuredIndex = 0;
    animation.velocity = @(-280);
    animation.beginTime = (CACurrentMediaTime() + 0.5);
    animation.delegate = self;
    [_menuLogo.layer pop_addAnimation:animation forKey:@"LogoAnimation"];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.8f, 0.8f)];
    scaleAnimation.springSpeed = 7;
    scaleAnimation.springBounciness = 15;
    [_menuLogo.layer pop_addAnimation:scaleAnimation forKey:@"LogoAnimationScale"];
}

- (void)animateLogo
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         _menuLogo.alpha = 1.0;
                     }completion:^(BOOL finished){
                         nextPicUp = NO;
                     }];
}

#pragma mark IBActions

- (IBAction)restorePurchase:(id)sender
{
    [iap restorePurchase];
}

- (IBAction)moreGames:(id)sender
{
    [Chartboost showInterstitial:CBLocationHomeScreen];
}

- (IBAction)feedback:(id)sender
{
    NSString *url;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        url = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", mainMenuDictionary[@"AppID"]];
    }
    else
    {
        url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", mainMenuDictionary[@"AppID"]];
    }
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}

#pragma mark MailComposer Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
            
        case MFMailComposeResultSaved:
            NSLog(@"Saved");
            break;
            
        case MFMailComposeResultSent:
            NSLog(@"Sent");
            break;
            
        case MFMailComposeResultFailed:
            NSLog(@"Error: %@", [error localizedDescription]);
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark POP Delegate
//- (void)pop_animationDidStop:(POPAnimation *)anim finished:(BOOL)finished
//{
//    if ([anim.name isEqualToString:@"LogoAnimation"])
//    {
//        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
//        scaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
//        scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
//        scaleAnimation.springSpeed = 1;
//        scaleAnimation.springBounciness = 6;
//        scaleAnimation.delegate = self;
//        scaleAnimation.name = @"buttonCollageAnimation";
//        [_buttonCollage.layer pop_addAnimation:scaleAnimation forKey:@"buttonCollageAnimation"];
//        
//        POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
//        opacityAnimation.toValue = @(1);
//        [_buttonCollage.layer pop_addAnimation:opacityAnimation forKey:@"buttonCollageAlphaAnimation"];
//        
//        POPSpringAnimation *scaleAnimationMenu = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
//        scaleAnimationMenu.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.7f, 0.7f)];
//        scaleAnimationMenu.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
//        scaleAnimationMenu.springSpeed = 10;
//        scaleAnimationMenu.springBounciness = 20;
//        [_menuLogo.layer pop_addAnimation:scaleAnimationMenu forKey:@"LogoAnimationScale"];
//    }
//    
//    [anim pop_removeAnimationForKey:anim.name];
//}
//
//- (void)pop_animationDidReachToValue:(POPAnimation *)anim
//{
//    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
//    scaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
//    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
//    scaleAnimation.springSpeed = 1;
//    scaleAnimation.springBounciness = 18;
//    scaleAnimation.delegate = self;
//    
//    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
//    opacityAnimation.toValue = @(1);
//    
//    if ([anim.name isEqualToString:@"buttonCollageAnimation"])
//    {
//        scaleAnimation.name = @"buttonFeedbackAnimation";
//        [_buttonFeedback.layer pop_addAnimation:scaleAnimation forKey:@"buttonFeedbackAnimation"];
//        [_buttonFeedback.layer pop_addAnimation:opacityAnimation forKey:@"buttonFeedbackAlphaAnimation"];
//    }
//    else if ([anim.name isEqualToString:@"buttonFeedbackAnimation"])
//    {
//        scaleAnimation.name = @"buttonRestoreAnimation";
//        [_buttonRestore.layer pop_addAnimation:scaleAnimation forKey:@"buttonRestoreAnimation"];
//        [_buttonRestore.layer pop_addAnimation:opacityAnimation forKey:@"buttonRestoreAlphaAnimation"];
//    }
//    else if ([anim.name isEqualToString:@"buttonRestoreAnimation"])
//    {
//        scaleAnimation.name = @"buttonFreeGameskAnimation";
//        [_buttonFreeGames.layer pop_addAnimation:scaleAnimation forKey:@"buttonFreeGameskAnimation"];
//        [_buttonFreeGames.layer pop_addAnimation:opacityAnimation forKey:@"buttonFreeGamesAlphaAnimation"];
//    }
//
//}
@end
