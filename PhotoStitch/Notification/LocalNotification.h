//
//  LocalNotification.h
//  piceditor7
//
//  Created by Kevin Chee on 27/03/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalNotification : NSObject

+ (void)scheduleNotification;
+ (void)resetBadge:(UIApplication *)application;
+ (void)cancel;

@end
