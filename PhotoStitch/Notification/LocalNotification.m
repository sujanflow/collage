//
//  LocalNotification.m
//  piceditor7
//
//  Created by Kevin Chee on 27/03/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import "LocalNotification.h"

@implementation LocalNotification

+ (void)scheduleNotification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60 * 60 * 24 * 7];
    localNotification.alertBody = [self getBody];
    localNotification.alertAction = @"Upload Selfie";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.repeatInterval = NSDayCalendarUnit;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    NSLog(@"Local Notification - Scheduled");
}

+ (void)resetBadge:(UIApplication *)application
{
    application.applicationIconBadgeNumber = 0;
    
    NSLog(@"Local Notification - Badge reset");
}

+ (void)cancel
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

+ (NSString *)getBody
{
    //Load Dictionary with wood name cross refference values for image name
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"ConfigPList" ofType:@"plist"];
    NSDictionary *notificationDictionary = [[NSDictionary alloc] initWithContentsOfFile:plistCatPath];
    
    NSArray *contents = [NSArray arrayWithObjects:notificationDictionary[@"NotificationMessage"],nil];
    
    return [contents objectAtIndex:arc4random()%[contents count]];
}

@end
