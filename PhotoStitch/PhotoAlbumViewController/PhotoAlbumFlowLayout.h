//
//  MyFlowLayout.h
//  stickem3
//
//  Created by Kevin Chee on 31/07/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoAlbumFlowLayout : UICollectionViewFlowLayout

@property (strong, nonatomic) NSIndexPath *currentCellPath;
@property (nonatomic) CGPoint stackCenter;
@property (nonatomic) CGFloat stackFactor;
@property (nonatomic) CGFloat stackSection;
@property (nonatomic) BOOL moveOut;

@end
