//
//  MyFlowLayout.m
//  stickem3
//
//  Created by Kevin Chee on 31/07/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "PhotoAlbumFlowLayout.h"

@implementation PhotoAlbumFlowLayout

-(id)init
{
    if (!(self = [super init])) return nil;
    
    self.itemSize = CGSizeMake(130.0, 125.0);

    self.sectionInset = UIEdgeInsetsMake(60, 20, 15, 20);
    self.minimumInteritemSpacing = 15.0f;
    self.minimumLineSpacing = 30.0f;
    
    return self;
}

// Custom setter for redrawing the layout
- (void)setStackFactor:(CGFloat)stackFactor {
    _stackFactor = stackFactor;
    
    [self invalidateLayout];
}

- (void)setStackCenter:(CGPoint)stackCenter {
    _stackCenter = stackCenter;
    
    [self invalidateLayout];
}

// Animation of cells only works WITHIN the bounds of the contentView.
// Enlarge the contentView to the size of the collectionView if needed
-(CGSize)collectionViewContentSize {
    CGSize contentSize = [super collectionViewContentSize];
    
    if (self.collectionView.bounds.size.width > contentSize.width)
        contentSize.width = self.collectionView.bounds.size.width;
    
    if (self.collectionView.bounds.size.height > contentSize.height)
        contentSize.height = self.collectionView.bounds.size.height;
    
    return contentSize;
}


-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray* attributesArray = [super layoutAttributesForElementsInRect:rect];
    
    if (_moveOut)
    {
        // Calculate the new position of each cell based on stackFactor and stackCenter
        for (UICollectionViewLayoutAttributes *attributes in attributesArray)
        {
            attributes.center = self.stackCenter;
            
            if (attributes.indexPath.row == 0) {
                attributes.alpha = 1.0;
                attributes.zIndex = 1.0; // Put the first cell on top of the stack
            } else {
                attributes.alpha = self.stackFactor; // fade the other cells out
                attributes.zIndex = 0.0; //Other cells below the first one
            }
        }
    }
    else
    {
        // Calculate the new position of each cell based on stackFactor and stackCenter
        for (UICollectionViewLayoutAttributes *attributes in attributesArray)
        {
            CGFloat xPosition = self.stackCenter.x + (attributes.center.x - self.stackCenter.x) * self.stackFactor;
            CGFloat yPosition = self.stackCenter.y + (attributes.center.y - self.stackCenter.y) * self.stackFactor;
            
            attributes.center = CGPointMake(xPosition, yPosition);

            if (attributes.indexPath.row == 0) {
                attributes.alpha = 1.0;
                attributes.zIndex = 1.0; // Put the first cell on top of the stack
            } else {
                attributes.alpha = self.stackFactor; // fade the other cells out
                attributes.zIndex = 0.0; //Other cells below the first one
            }
        }
    }
    
    return attributesArray;
}

@end
