//
//  MainMenuViewController.h
//  stickem3
//
//  Created by Kevin Chee on 30/07/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoAlbumFlowLayout.h"

@interface PhotoAlbumViewController : UIViewController
<UICollectionViewDataSource, UICollectionViewDelegate>
{
    NSMutableArray *photos;
    NSMutableArray *album;
    NSMutableArray *pics;
    NSMutableArray *timestamp;
    NSMutableArray *date;
    
    BOOL isAlbum;
    NSInteger selectedSection;
    PhotoAlbumFlowLayout *myFlowLayout;
    UIImage *selectedPhoto;
    float fontDecrease;
    NSString *lang;
}

@property (nonatomic, retain) IBOutlet UICollectionView *collection;
@property (nonatomic, retain) IBOutlet UINavigationBar *albumNavBar;
@property (nonatomic, retain) IBOutlet UINavigationBar *photoNavBar;
@property (nonatomic, retain) IBOutlet UIImageView *albumBottomBar;
@property (nonatomic, retain) IBOutlet UIImageView *photoBottomBar;

- (IBAction)goAlbum:(id) sender;
- (IBAction)dismissMyself:(id) sender;

@end
