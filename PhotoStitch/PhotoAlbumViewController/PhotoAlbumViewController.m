//
//  MainMenuViewController.m
//  stickem3
//
//  Created by Kevin Chee on 30/07/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "PhotoAlbumViewController.h"
#import "CropViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Languages.h"

#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)
@interface PhotoAlbumViewController ()

@end

@implementation PhotoAlbumViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isAlbum = YES;
    lang = [Languages currentLocale];
    fontDecrease = [Languages photoLabelSizeDecrease];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self getAllPictures];
    [self initImages];
    
    myFlowLayout = [[PhotoAlbumFlowLayout alloc] init];
    [myFlowLayout setStackFactor:1.0];
    [myFlowLayout setStackCenter:CGPointMake(57.5, 57.5)];
    
    [_collection setCollectionViewLayout:myFlowLayout animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initImages
{
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    // For iPhone 5
    if (screenSize.height == 568)
    {
        [_collection setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"photoalbum_bg-568h.png"]]];
    }
    else
    {
        [_collection setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"photoalbum_bg.png"]]];
    }
    
    [_albumNavBar setBackgroundImage:[UIImage imageNamed:@"album_top.png"] forBarMetrics:UIBarMetricsDefault];
    [_albumNavBar setShadowImage:[UIImage new]];
    
    [_photoNavBar setBackgroundImage:[UIImage imageNamed:@"photo_top.png"] forBarMetrics:UIBarMetricsDefault];
    [_photoNavBar setShadowImage:[UIImage new]];
    
    // Hide Photo Nav Bar
    _photoNavBar.alpha = 0.0;
    _photoBottomBar.alpha = 0.0;
}

- (void)getAllPictures
{
    //__block NSString *albumName;
    photos = [[NSMutableArray alloc] init];
    album = [[NSMutableArray alloc] init];
    pics = [[NSMutableArray alloc] init];
    timestamp = [[NSMutableArray alloc] init];
    date = [[NSMutableArray alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:lang]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    __block NSString *picDate;
    ALAssetsLibrary *library = [PhotoAlbumViewController defaultAssetsLibrary];
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            //NSLog(@"%@ See Asset : %@", album, result);
            [pics addObject:result];
            picDate = [dateFormatter stringFromDate:[result valueForProperty:ALAssetPropertyDate]];
            [timestamp addObject:picDate];
        }
        else
        {
            // Moving to next album
            if ([pics count] != 0)
            {
                [photos addObject:pics];
                [date addObject:timestamp];
                //[album removeAllObjects];
                pics = [[NSMutableArray alloc] init];
                timestamp = [[NSMutableArray alloc] init];
            }
            else
            {
                // No picture in album, remove album from collection
                [album removeLastObject];
            }
        }
    };
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            // Get Album Names
            NSLog(@"Album found - %@",[group valueForProperty:ALAssetsGroupPropertyName]);
            [album addObject:[group valueForProperty:ALAssetsGroupPropertyName]];
            [group enumerateAssetsUsingBlock:assetEnumerator];
            [_collection reloadData];
        }
    };
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {NSLog(@"There is an error");}];
}

+ (ALAssetsLibrary *)defaultAssetsLibrary
{
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (isAlbum)
    {
        return [photos count];
    }
    else
    {
        return [[photos objectAtIndex:selectedSection] count];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *picture = (UIImageView *)[cell viewWithTag:100];
    UILabel *title = (UILabel *)[cell viewWithTag:99];
    UIView *canvas = (UIView *)[cell viewWithTag:98];
    UIView *canvas2 = (UIView *)[cell viewWithTag:95];
    
    if (isAlbum)
    {
        [[canvas layer] setHidden:NO];
        [canvas2 setHidden:NO];
        [canvas2 setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2f]];
        
        NSInteger albumIndex = [album count] - indexPath.row - 1;
        NSInteger index = [photos[albumIndex] count] - 1;
        ALAsset *photo = [photos[albumIndex] objectAtIndex:index];
        
        [picture setImage:[UIImage imageWithCGImage:photo.thumbnail]];
        [picture setFrame:CGRectMake(23.0, 15.0, 85.0, 85.0)];
        [canvas setBackgroundColor:[UIColor whiteColor]];
        
        title.text = [album objectAtIndex:albumIndex];
        [title setFont:[UIFont systemFontOfSize:[Languages labelSizeDefault]]];
        [title setFrame:CGRectMake(0.0, cell.frame.size.height * 0.83, 130.0, 20.0)];
        
        cell.backgroundColor = [UIColor clearColor];
    }
    else
    {
        [[canvas layer] setHidden:YES];
        [canvas2 setHidden:YES];
        
        NSInteger index = ([photos[selectedSection] count] - 1) - indexPath.row;
        ALAsset *photo = [photos[selectedSection] objectAtIndex:index];
        [picture setImage:[UIImage imageWithCGImage:photo.thumbnail]];
        [picture setFrame:CGRectMake(5.0, 5.0, 75.0, 75.0)];
        [canvas setBackgroundColor:[UIColor clearColor]];
        
        title.text = [date[selectedSection] objectAtIndex:index];
        [title setFont:[UIFont systemFontOfSize:[Languages labelSizeDefault] - fontDecrease]];
        [title setFrame:CGRectMake(0.0, cell.frame.size.height * 0.82, 85.0, 20.0)];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did deselect : %zd", indexPath.row);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (isAlbum)
    {
        [_collection performBatchUpdates:^{
            // First move albums away
            myFlowLayout.stackFactor = 0.0;
            myFlowLayout.stackCenter = CGPointMake(150.0, -100.0);
            myFlowLayout.moveOut = YES;
        } completion:
         ^(BOOL finished)
         {
             // Reload sections to photos
             isAlbum = NO;
             selectedSection = [album count] - indexPath.row - 1;
             myFlowLayout.minimumInteritemSpacing = 10.0f;
             myFlowLayout.sectionInset = UIEdgeInsetsMake(60, 20, 15, 20);
             myFlowLayout.itemSize = CGSizeMake(85.0, 100.0);
             [_collection reloadSections:[NSIndexSet indexSetWithIndex:0]];
             
             // Move in Photos
             [_collection performBatchUpdates:^{
                 myFlowLayout.stackFactor = 1.0;
                 myFlowLayout.stackCenter = CGPointMake(57.5, 57.5);
                 myFlowLayout.moveOut = NO;
                 
                 [UIView animateWithDuration:0.2
                                       delay:0
                                     options:UIViewAnimationOptionCurveEaseOut
                                  animations:^{
                                      _albumNavBar.alpha = 0.0;
                                      _albumBottomBar.alpha = 0.0;
                                  }completion:^(BOOL finished){
                                      [UIView animateWithDuration:0.4
                                                            delay:0
                                                          options:UIViewAnimationOptionCurveEaseOut
                                                       animations:^{
                                                           _photoNavBar.alpha = 1.0;
                                                           _photoBottomBar.alpha = 1.0;
                                                       }completion:^(BOOL finished){
                                                       }];
                                  }];
                 
             } completion:nil];
         }];
    }
    else
    {
        // Selects a photo and send for editing
        NSLog(@"Album - Selected Section: %zd, Row: %zd", selectedSection, indexPath.row);
        NSInteger index = ([photos[selectedSection] count] - 1) - indexPath.row;
        ALAsset *photo = [photos[selectedSection] objectAtIndex:index];
        // Get the right orientation
        UIImageOrientation orientation = UIImageOrientationUp;
        NSNumber *orientationValue = [photo valueForProperty:@"ALAssetPropertyOrientation"];
        if (orientationValue != nil)
        {
            orientation = [orientationValue intValue];
        }
        selectedPhoto = [UIImage imageWithCGImage:photo.defaultRepresentation.fullResolutionImage
                         scale:1.0 orientation:orientation];
        [self performSegueWithIdentifier:@"cropSegue" sender:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [segue.destinationViewController gimmeCropPhoto:selectedPhoto];
}

#pragma IBAction
- (IBAction)goAlbum:(id) sender
{
    [_collection performBatchUpdates:^{
        // First move albums away
        myFlowLayout.stackFactor = 0.0;
        myFlowLayout.stackCenter = CGPointMake(150.0, -150.0);
        myFlowLayout.moveOut = YES;
    } completion:
     ^(BOOL finished)
     {
         isAlbum = YES;
         myFlowLayout.minimumInteritemSpacing = 15.0f;
         myFlowLayout.sectionInset = UIEdgeInsetsMake(60, 20, 15, 20);
         myFlowLayout.itemSize = CGSizeMake(130.0, 125.0);
         [_collection reloadSections:[NSIndexSet indexSetWithIndex:0]];
         
         [_collection performBatchUpdates:^{
             myFlowLayout.stackFactor = 1.0;
             myFlowLayout.stackCenter = CGPointMake(57.5, 57.5);
             myFlowLayout.moveOut = NO;
             
             [UIView animateWithDuration:0.2
                                   delay:0
                                 options:UIViewAnimationOptionCurveEaseOut
                              animations:^{
                                  _photoNavBar.alpha = 0.0;
                                  _photoBottomBar.alpha = 0.0;
                              }completion:^(BOOL finished){
                                  [UIView animateWithDuration:0.4
                                                        delay:0
                                                      options:UIViewAnimationOptionCurveEaseOut
                                                   animations:^{
                                                       _albumNavBar.alpha = 1.0;
                                                       _albumBottomBar.alpha = 1.0;
                                                   }completion:^(BOOL finished){
                                                   }];
                              }];
         } completion:nil];
     }];
}

- (IBAction)dismissMyself:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
