//
//  SavedViewController.h
//  pb2free
//
//  Created by Kevin Chee on 28/09/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <iAd/iAd.h>


#import <GoogleMobileAds/GADBannerView.h>
#import "AdManager.h"


@interface SavedViewController : UIViewController
<UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate, ADBannerViewDelegate,GADBannerViewDelegate>
{
    NSDictionary *savedViewDictionary;
    UIImageView *pic;
}

#pragma Save
@property (nonatomic, retain) IBOutlet UIView *photoCanvas;
@property (nonatomic, retain) IBOutlet UIView *dropShadow;
@property (nonatomic, retain) IBOutlet UIImageView *photo;
@property (nonatomic, retain) IBOutlet UILabel *photoLabel;
@property (nonatomic, retain) IBOutlet UIButton *startAgain;
@property (nonatomic, retain) IBOutlet UIButton *share;
@property (nonatomic, retain) IBOutlet UIButton *facebook;
@property (nonatomic, retain) IBOutlet UIButton *instagram;
@property (nonatomic, retain) IBOutlet UIButton *twitter;
@property (nonatomic, retain) IBOutlet ADBannerView *banner;
@property (nonatomic, strong) UIDocumentInteractionController *docController;
@property (nonatomic, retain) IBOutlet UIImageView *savedBG;
@property(nonatomic, retain) IBOutlet GADBannerView *saveAView;

#pragma IBAction
- (IBAction)bringShareList:(id)sender;
- (IBAction)socialPressed:(id)sender;
- (IBAction)startAgainPressed:(id)sender;

#pragma Public
- (void)gimmePhoto:(UIImageView *)image;

@end
