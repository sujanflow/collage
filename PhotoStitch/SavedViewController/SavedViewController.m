//
//  SavedViewController.m
//  pb2free
//
//  Created by Kevin Chee on 28/09/13.
//  Copyright (c) 2013 Ace Mind Apps. All rights reserved.
//

#import "SavedViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>
#import "Ads.h"
#import "CollectionPack.h"
#import "Languages.h"
#import "POP.h"
#import "MainMenuViewController.h"
@import GoogleMobileAds;

@interface SavedViewController ()<GADInterstitialDelegate>
@end

@implementation SavedViewController
@synthesize saveAView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //Load Dictionary with wood name cross refference values for image name
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"ConfigPList" ofType:@"plist"];
    savedViewDictionary = [[NSDictionary alloc] initWithContentsOfFile:plistCatPath];

    
//  edited by Monir
//    _banner.delegate = self;
//    [_banner setFrame:CGRectMake(0.0, self.view.frame.size.height - _banner.frame.size.height,
//                                 _banner.frame.size.width, _banner.frame.size.height)];
//
    
    [self initPhoto];
    [self setForAnimation];
    
    [self animateIn];
    
    [_photoLabel setFont:[UIFont fontWithName:@"Roboto Condensed" size:[Languages labelSizeDefault] + [Languages savedMessageSizeIncrease]]];
    
    
    
    CGPoint origin;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        //is iPhone
        
        if ([[UIScreen mainScreen] bounds].size.height == 568){
            origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2, [UIScreen mainScreen].bounds.size.height-50);
        }
        else {
            
            if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0){
                origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2,[UIScreen mainScreen].bounds.size.height-50);
                //device with Retina Display
            }
            else{origin = CGPointMake(([UIScreen mainScreen].bounds.size.width-CGSizeFromGADAdSize(kGADAdSizeBanner).width)/2,[UIScreen mainScreen].bounds.size.height-50);
                //no Retina Display
                
            }
        }
    }else  {
        
        //is iPad
        
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0){
            //device with Retina Display
            origin = CGPointMake(20,0);        }
        else{
            origin = CGPointMake(20,0);                //no Retina Display
        }
    }

    
//    edited by Monir
//    saveAView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:admob_bottom_banner andOrigin:origin];
    saveAView = [[AdManager sharedInstance] adMobBannerWithAdUnitID:@"ca-app-pub-8340924500267413/5572444788" andOrigin:origin];
//
    
    saveAView.rootViewController = self;
    [self.view addSubview:saveAView];

//    edited by Monir
//    [[AdManager sharedInstance] showAdmobFullscreen];
//
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setForAnimation
{
    [_photoCanvas.layer setOpacity:0.0];
    [_dropShadow.layer setOpacity:0.0];
    [_photoLabel.layer setOpacity:0.0];
    [_startAgain.layer setOpacity:0.0];
    [_share.layer setOpacity:0.0];
    [_facebook.layer setOpacity:0.0];
    [_instagram.layer setOpacity:0.0];
    [_twitter.layer setOpacity:0.0];
}

#pragma Initialisation
- (void)initPhoto
{
    [_photo setImage:pic.image];
    [_dropShadow setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2f]];
}

- (void)initShareScroll
{
    // Facebook
    UIButton *facebook = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    facebook.tag = 1;
    [facebook addTarget:self action:@selector(socialPressed:) forControlEvents:UIControlEventTouchUpInside];
    [facebook setFrame:CGRectMake(30.0, 20.0, 32.0, 50.0)];
    [facebook setBackgroundImage:[UIImage imageNamed:@"saved_facebook.png"] forState:UIControlStateNormal];
    
    // Twitter
    UIButton *twitter = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    twitter.tag = 2;
    [twitter addTarget:self action:@selector(socialPressed:) forControlEvents:UIControlEventTouchUpInside];
    [twitter setFrame:CGRectMake(95.0, 20.0, 40.0, 50.0)];
    [twitter setBackgroundImage:[UIImage imageNamed:@"saved_twitter.png"] forState:UIControlStateNormal];
    
    // Instagram
    UIButton *instagram = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    instagram.tag = 3;
    [instagram addTarget:self action:@selector(socialPressed:) forControlEvents:UIControlEventTouchUpInside];
    [instagram setFrame:CGRectMake(171.0, 20.0, 43.0, 53.0)];
    [instagram setBackgroundImage:[UIImage imageNamed:@"saved_instagram.png"] forState:UIControlStateNormal];
    
    // Weibo
    UIButton *weibo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    weibo.tag = 4;
    [weibo addTarget:self action:@selector(socialPressed:) forControlEvents:UIControlEventTouchUpInside];
    [weibo setFrame:CGRectMake(251.0, 20.0, 41.0, 50.0)];
    [weibo setBackgroundImage:[UIImage imageNamed:@"saved_weibo.png"] forState:UIControlStateNormal];
    
    // Mail
    UIButton *mail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    mail.tag = 5;
    [mail addTarget:self action:@selector(socialPressed:) forControlEvents:UIControlEventTouchUpInside];
    [mail setFrame:CGRectMake(251.0, 20.0, 41.0, 50.0)];
    [mail setBackgroundImage:[UIImage imageNamed:@"saved_mail.png"] forState:UIControlStateNormal];
    
    if ([[Languages currentLocale] isEqualToString:@"zh-Hans"])
    {
        [mail setHidden:YES];
    }
    else
    {
        [weibo setHidden:YES];
    }
    
}

- (void)animateIn
{
    float time = 0.5;
    
    POPSpringAnimation *photoScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    photoScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    photoScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    photoScaleAnimation.springSpeed = 3;
    photoScaleAnimation.springBounciness = 10;
    photoScaleAnimation.beginTime = (CACurrentMediaTime() + time);
    [_photoCanvas.layer pop_addAnimation:photoScaleAnimation forKey:@"PhotoScaleAnimation"];
    
    POPBasicAnimation *photoOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    photoOpacityAnimation.toValue = @(1);
    photoOpacityAnimation.beginTime = (CACurrentMediaTime() + time);
    [_photoCanvas.layer pop_addAnimation:photoOpacityAnimation forKey:@"PhotoOpacityAnimation"];
    
    POPSpringAnimation *shadowScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    shadowScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    shadowScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    shadowScaleAnimation.springSpeed = 3;
    shadowScaleAnimation.springBounciness = 10;
    shadowScaleAnimation.beginTime = (CACurrentMediaTime() + time);
    [_dropShadow.layer pop_addAnimation:shadowScaleAnimation forKey:@"ShadowScaleAnimation"];
    
    POPBasicAnimation *shadowOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    shadowOpacityAnimation.toValue = @(1);
    shadowOpacityAnimation.beginTime = (CACurrentMediaTime() + time);
    [_dropShadow.layer pop_addAnimation:shadowOpacityAnimation forKey:@"ShadowOpacityAnimation"];
    
    POPSpringAnimation *restartScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    restartScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    restartScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    restartScaleAnimation.springSpeed = 5;
    restartScaleAnimation.springBounciness = 15;
    restartScaleAnimation.beginTime = (CACurrentMediaTime() + time + 1.0);
    [_startAgain.layer pop_addAnimation:restartScaleAnimation forKey:@"RestartScaleAnimation"];
    
    POPBasicAnimation *restartOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    restartOpacityAnimation.toValue = @(1);
    restartOpacityAnimation.beginTime = (CACurrentMediaTime() + time + 1.0);
    [_startAgain.layer pop_addAnimation:restartOpacityAnimation forKey:@"RestartOpacityAnimation"];
    
    POPSpringAnimation *shareScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    shareScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    shareScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    shareScaleAnimation.springSpeed = 5;
    shareScaleAnimation.springBounciness = 15;
    shareScaleAnimation.beginTime = (CACurrentMediaTime() + time + 2.0);
    [_share.layer pop_addAnimation:shareScaleAnimation forKey:@"ShareScaleAnimation"];
    
    POPBasicAnimation *shareOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    shareOpacityAnimation.toValue = @(1);
    shareOpacityAnimation.beginTime = (CACurrentMediaTime() + time + 2.0);
    [_share.layer pop_addAnimation:shareOpacityAnimation forKey:@"ShareOpacityAnimation"];
    
    POPSpringAnimation *labelScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    labelScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    labelScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    labelScaleAnimation.springSpeed = 8;
    labelScaleAnimation.springBounciness = 18;
    labelScaleAnimation.beginTime = (CACurrentMediaTime() + time + 3.0);
    [_photoLabel.layer pop_addAnimation:labelScaleAnimation forKey:@"LabelScaleAnimation"];
    
    POPBasicAnimation *labelOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    labelOpacityAnimation.toValue = @(1);
    labelOpacityAnimation.beginTime = (CACurrentMediaTime() + time + 3.0);
    [_photoLabel.layer pop_addAnimation:labelOpacityAnimation forKey:@"LabelOpacityAnimation"];
    
    POPSpringAnimation *labelPositionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    labelPositionAnimation.toValue = @(_photoLabel.layer.position.y + _photoLabel.frame.size.height);
    labelPositionAnimation.springSpeed = 8;
    labelPositionAnimation.springBounciness = 18;
    labelPositionAnimation.beginTime = (CACurrentMediaTime() + time + 3.0);
    [_photoLabel.layer pop_addAnimation:labelPositionAnimation forKey:@"LabelPositionAnimation"];
    
    NSLog(@"Saved - Contents Animated");
}

- (IBAction)socialPressed:(id)sender;
{
    UIButton *button = (UIButton *)sender;
    SLComposeViewController *composer;
    NSURL *instagramURL;
    
    switch (button.tag)
    {
        case 1:
            composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [composer setInitialText:savedViewDictionary[@"Hashtag"]];
            [composer addImage:_photo.image];
            [self presentViewController:composer animated:YES completion:nil];
            [composer setCompletionHandler:^(SLComposeViewControllerResult result)
             {
                 switch (result) {
                     case SLComposeViewControllerResultCancelled:
                         NSLog(@"Saved - Facebook Post cancelled");
                         break;
                         
                     case SLComposeViewControllerResultDone:
                         NSLog(@"Saved - Facebook Post successful");
                         break;
                         
                     default:
                         break;
                 }
             }];
            break;
        
        case 2:
            composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [composer setInitialText:savedViewDictionary[@"Hashtag"]];
            [composer addImage:_photo.image];
            [self presentViewController:composer animated:YES completion:nil];
            [composer setCompletionHandler:^(SLComposeViewControllerResult result)
            {
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        NSLog(@"Saved - Twitter Post cancelled");
                        break;
                        
                    case SLComposeViewControllerResultDone:
                        NSLog(@"Saved - Twitter Post successful");
                        break;
                        
                    default:
                        break;
                }
            }];
            break;
        
        case 3:
            instagramURL = [NSURL URLWithString:@"instagram://app"];
            if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
            {   
                NSString *savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Image.igo"];
                [UIImageJPEGRepresentation(_photo.image, 1.0) writeToFile:savePath atomically:YES];
                
                NSURL *imageURL = [[NSURL alloc] initFileURLWithPath:savePath];
                
                _docController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
                _docController.delegate = self;
                _docController.UTI = @"com.instagram.exclusivegram";
                _docController.URL = imageURL;
                _docController.annotation = [NSDictionary dictionaryWithObject:savedViewDictionary[@"Hashtag"] forKey:@"InstagramCaption"];
                [_docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                                message:@"Instagram App required"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK", nil];
                
                [alert show];
            }
            break;
            
        case 4:
        {
            composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeSinaWeibo];
            [composer setInitialText:savedViewDictionary[@"Hashtag"]];
            [composer addImage:_photo.image];
            [self presentViewController:composer animated:YES completion:nil];
            [composer setCompletionHandler:^(SLComposeViewControllerResult result)
             {
                 switch (result) {
                     case SLComposeViewControllerResultCancelled:
                         NSLog(@"Saved - Weibo Post cancelled");
                         break;
                         
                     case SLComposeViewControllerResultDone:
                         NSLog(@"Saved - Weibo Post successful");
                         break;
                         
                     default:
                         break;
                 }
             }];
            break;
        }
            
        default:
            break;
    }
    
    NSLog(@"Saved - Pressed %zd", button.tag);
}

# pragma Document Controller Delegate

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
    NSLog(@"Saved - Instagram launched");
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application
{
    NSLog(@"SENT");
}

# pragma Mail Compose Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        
        case MFMailComposeResultSaved:
            NSLog(@"Saved");
            break;
            
        case MFMailComposeResultSent:
            NSLog(@"Sent");
            break;
            
        case MFMailComposeResultFailed:
            NSLog(@"Error: %@", [error localizedDescription]);
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

# pragma Public function
- (void)gimmePhoto:(UIImageView *)image
{
    pic = image;
    NSLog(@"Saved - Photo passed in");
}


- (IBAction)startAgainPressed:(id)sender
{
//    edited by Monir
    [self performSegueWithIdentifier:@"MainMenuViewController" sender:sender];
//
}

//    edited by Monir
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"MainMenuViewController"]){
        MainMenuViewController *controller = (MainMenuViewController *)segue.destinationViewController;
        controller.isSomethingEnabled = YES;
    }
}
//


# pragma IBAction function
- (IBAction)bringShareList:(id)sender
{
    
    // Airdrop sharing
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    [sharingItems addObject:_photo.image];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:controller animated:YES completion:nil];
    
    /*POPSpringAnimation *facebookScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    facebookScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    facebookScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    facebookScaleAnimation.springSpeed = 8;
    facebookScaleAnimation.springBounciness = 20;
    [_facebook.layer pop_addAnimation:facebookScaleAnimation forKey:@"FacebookScaleAnimation"];
    
    POPBasicAnimation *facebookOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    facebookOpacityAnimation.toValue = @(1);
    [_facebook.layer pop_addAnimation:facebookOpacityAnimation forKey:@"FacebookOpacityAnimation"];
    
    POPSpringAnimation *instagramScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    instagramScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    instagramScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    instagramScaleAnimation.springSpeed = 8;
    instagramScaleAnimation.springBounciness = 20;
    instagramScaleAnimation.beginTime = (CACurrentMediaTime() + 0.3);
    [_instagram.layer pop_addAnimation:instagramScaleAnimation forKey:@"InstagramScaleAnimation"];
    
    POPBasicAnimation *instagramOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    instagramOpacityAnimation.toValue = @(1);
    instagramOpacityAnimation.beginTime = (CACurrentMediaTime() + 0.3);
    [_instagram.layer pop_addAnimation:instagramOpacityAnimation forKey:@"InstagramOpacityAnimation"];
    
    POPSpringAnimation *twitterScaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    twitterScaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.0f, 0.0f)];
    twitterScaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    twitterScaleAnimation.springSpeed = 8;
    twitterScaleAnimation.springBounciness = 20;
    twitterScaleAnimation.beginTime = (CACurrentMediaTime() + 0.6);
    [_twitter.layer pop_addAnimation:twitterScaleAnimation forKey:@"TwitterScaleAnimation"];
    
    POPBasicAnimation *twitterOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    twitterOpacityAnimation.toValue = @(1);
    twitterOpacityAnimation.beginTime = (CACurrentMediaTime() + 0.6);
    [_twitter.layer pop_addAnimation:twitterOpacityAnimation forKey:@"TwitterOpacityAnimation"];*/
}

@end
