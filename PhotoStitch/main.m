        //
//  main.m
//  PhotoStitch
//
//  Created by Kevin Chee on 5/06/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainMenuAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MainMenuAppDelegate class]));
    }
}
