//
//  PhotoEditingViewController.h
//  PhotoStitchExtension
//
//  Created by Kevin Chee on 19/09/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sticker.h"

@interface PhotoEditingViewController : UIViewController
<UICollectionViewDataSource, StickerDelegate>
{
    UIImageView *finalPhoto;
    UIImageView *photo;
    UIImage *fullSizePhoto;
    Sticker *selectedSticker;
    NSMutableArray *purchases;
}

@property (nonatomic, retain) IBOutlet UIScrollView *colours;
@property (nonatomic, retain) IBOutlet UICollectionView *quotes;
@property (nonatomic, retain) IBOutlet UIButton *swap;

- (IBAction)swapView:(id)sender;

@end
