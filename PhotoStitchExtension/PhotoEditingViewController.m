//
//  PhotoEditingViewController.m
//  PhotoStitchExtension
//
//  Created by Kevin Chee on 19/09/2014.
//  Copyright (c) 2014 Ace Mind Apps. All rights reserved.
//


#import "PhotoEditingViewController.h"
#import <Photos/Photos.h>
#import <PhotosUI/PhotosUI.h>
#import <QuartzCore/QuartzCore.h>
#import "FontPack.h"
#import "CollectionPack.h"

#define INSET           36.0

#define kBundleIdentifier   @"com.iftikher.collage1"
#define kPurchase           @"Purchase pack from App first"
#define kEntitlement        @"group.iftikher.collage1"
#define kPack1              18
#define kPack2              103
#define kPack3              36
#define kPack4              0
#define kPack5              0
#define kPack6              0

@interface PhotoEditingViewController () <PHContentEditingController>
@property (strong) PHContentEditingInput *input;
@end

@implementation PhotoEditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initColours];
    [self initPurchases];
    selectedSticker = [[Sticker alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PHContentEditingController

- (BOOL)canHandleAdjustmentData:(PHAdjustmentData *)adjustmentData {
    // Inspect the adjustmentData to determine whether your extension can work with past edits.
    // (Typically, you use its formatIdentifier and formatVersion properties to do this.)
    BOOL result = [adjustmentData.formatIdentifier isEqualToString:kBundleIdentifier];
    result &= [adjustmentData.formatVersion isEqualToString:@"1.0"];
    
    return result;
}

- (void)startContentEditingWithInput:(PHContentEditingInput *)contentEditingInput placeholderImage:(UIImage *)placeholderImage {
    // Present content for editing, and keep the contentEditingInput for use when closing the edit session.
    // If you returned YES from canHandleAdjustmentData:, contentEditingInput has the original image and adjustment data.
    // If you returned NO, the contentEditingInput has past edits "baked in".
    
    if (contentEditingInput.mediaType == PHAssetMediaTypeImage)
    {
        UIImage *image = contentEditingInput.displaySizeImage;
        fullSizePhoto = [UIImage imageWithData:[NSData dataWithContentsOfURL:contentEditingInput.fullSizeImageURL]];
        NSLog(@"%@", NSStringFromCGSize(image.size));
        NSLog(@"Full: %@, Scale: %f", NSStringFromCGSize(fullSizePhoto.size), fullSizePhoto.scale);
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = (width*image.size.height)/image.size.width;
        CGFloat availableHeight = [UIScreen mainScreen].bounds.size.height - height - _quotes.frame.size.height - 50.0;
        CGFloat y = 50.0 + (availableHeight/2.0);
        
        photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, y, width, height)];
        [photo setContentMode:UIViewContentModeScaleAspectFit];
        [photo setImage:image];
        [photo setUserInteractionEnabled:YES];
        [self.view addSubview:photo];
        
        [self.view bringSubviewToFront:_colours];
        [self.view bringSubviewToFront:_quotes];
        [self.view bringSubviewToFront:_swap];
        
        NSLog(@"%@", NSStringFromCGRect(self.view.frame));
        
        @try {
            PHAdjustmentData *adjustmentData = contentEditingInput.adjustmentData;
            if (adjustmentData)
            {
                Sticker *data = [NSKeyedUnarchiver unarchiveObjectWithData:adjustmentData.data];
                UIImage *image = [self imageNamed:[NSString stringWithFormat:@"Pack%zdQuotes_%zd.png", data.pack, data.tag+1]
                                        withColor:[UIColor whiteColor]];
                
                [selectedSticker setFrame:data.frame];
                [selectedSticker giveImage:image :@"Quotes" :1];
                [selectedSticker giveGestures];
                [selectedSticker removeDelete];
                [selectedSticker setInitSize:CGSizeMake(data.imageView.image.size.width, data.imageView.image.size.height)];
                selectedSticker.tag = data.tag;
                selectedSticker.delegate = self;
                [photo addSubview:selectedSticker];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception decoding adjustment data: %@", exception);
        }
        
        self.input = contentEditingInput;
    }
}

- (void)finishContentEditingWithCompletionHandler:(void (^)(PHContentEditingOutput *))completionHandler {
    // Update UI to reflect that editing has finished and output is being rendered.
    
    // Render and provide output on a background queue.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Create editing output from the editing input.
        PHContentEditingOutput *output = [[PHContentEditingOutput alloc] initWithContentEditingInput:self.input];
        
        // Provide new adjustments and render output to given location.
        NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:selectedSticker];
        output.adjustmentData = [[PHAdjustmentData alloc] initWithFormatIdentifier:kBundleIdentifier
                                                                     formatVersion:@"1.0"
                                                                              data:archivedData];
        
        NSData *renderedJPEGData = UIImageJPEGRepresentation([self gimmeMyInstaPictureImage], 1.0f);
        [renderedJPEGData writeToURL:output.renderedContentURL atomically:YES];
        
        // Call completion handler to commit edit to Photos.
        completionHandler(output);
        
        // Clean up temporary files, etc.
    });
}

- (BOOL)shouldShowCancelConfirmation {
    // Returns whether a confirmation to discard changes should be shown to the user on cancel.
    // (Typically, you should return YES if there are any unsaved changes.)
    return NO;
}

- (void)cancelContentEditing {
    // Clean up temporary files, etc.
    // May be called after finishContentEditingWithCompletionHandler: while you prepare output.
}

#pragma mark - Colours

- (void)initColours
{
    int i = 0;
    
    while (i <= [FontPack totalColour])
    {
        [_colours addSubview:[self createColourButton:[FontPack findColour:i] :i]];
        i++;
    }
    
    [_colours setContentSize:CGSizeMake(50.0*i, 50.0)];
    [_colours setHidden:YES];
}

- (UIButton *)createColourButton:(UIColor *)colour :(int)num
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(50.0*num, 0.0, 50.0, 50.0)];
    [button setBackgroundColor:colour];
    [button addTarget:self action:@selector(changeColour:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (IBAction)changeColour:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if ([selectedSticker isImage])
    {
        [selectedSticker changeImage:[self imageNamed:[self getQuote:selectedSticker.tag]
                                            withColor:button.backgroundColor]];
    }
}

#pragma mark -- Collection View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return kPack1+kPack2+kPack3+kPack4+kPack5+kPack6;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *image = (UIImageView *)[cell viewWithTag:99];
    NSString *imageName = [self getQuote:indexPath.row];
    [image setImage:[self imageNamed:imageName
                           withColor:[self getColour:imageName]]];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Pressed: %zd", indexPath.row);
    
    CGFloat width, height;
    CGFloat centerX = photo.frame.size.width/2.0, centerY = photo.frame.size.height/2.0;
    
    NSString *imageName = [self getQuote:indexPath.row];
    
    if ([self isPurchased:imageName])
    {
        UIImage *image = [self imageNamed:imageName
                                withColor:[UIColor whiteColor]];
        
        width = image.size.width;
        height = image.size.height;
        NSLog(@"%f %f", width, height);
        if ((width >= 200.0 && width <= 300.0) || (height >= 200.0 && height <= 300.0))
        {
            width = width/2.0;
            height = height/2.0;
        }
        else if (width >= 300.0 || height >= 300.0)
        {
            width = width/3.0;
            height = height/3.0;
        }
        
        if (selectedSticker.imageView.image != nil)
        {
            selectedSticker.imageView.image = nil;
            selectedSticker.imageView = nil;
            [selectedSticker.imageView removeFromSuperview];
            [selectedSticker removeFromSuperview];
            selectedSticker = nil;
            selectedSticker = [[Sticker alloc] initWithFrame:CGRectZero];
        }
        [selectedSticker setFrame:CGRectMake(centerX - (width/2.0), centerY - (height/2.0), width, height)];
        [selectedSticker giveImage:image :@"Quotes" :1];
        [selectedSticker giveGestures];
        [selectedSticker removeDelete];
        [selectedSticker setInitSize:CGSizeMake(width, height)];
        selectedSticker.tag = indexPath.row;
        selectedSticker.delegate = self;
        [photo addSubview:selectedSticker];
    }
    else
    {
        UILabel *label = [[UILabel alloc] init];
        [label setText:kPurchase];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor:[UIColor redColor]];
        [label setFrame:CGRectMake(0.0, centerY, photo.frame.size.width, 50.0)];
        [label setFont:[UIFont systemFontOfSize:19.0]];
        [photo addSubview:label];
        [UIView animateWithDuration:3.0
                         animations:^{
                             [label setAlpha:0.0];
                         }
                         completion:^(BOOL finished){
                             [label removeFromSuperview];
                         }];
    }
}

- (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color
{
    UIImage *image = [UIImage imageNamed:name];
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0)
    {
        // Use Retina Display
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, image.scale);
    }
    else
    {
        UIGraphicsBeginImageContext(rect.size);
    }
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, image.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *flippedImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:UIImageOrientationDownMirrored];
    
    return flippedImage;
}

- (NSString *)getQuote:(NSInteger)index
{
    NSString *imageName;
    NSInteger pack;
    
    if (index < kPack1)
    {
        pack = 0;
        index = index + 1;
    }
    else if (index >= kPack1 && index < kPack1+kPack2)
    {
        pack = 1;
        index = index - kPack1 + 1;
    }
    else if (index >= kPack1+kPack2 && index < kPack1+kPack2+kPack3)
    {
        pack = 2;
        index = index - (kPack1+kPack2) + 1;
    }
    else if (index >= kPack1+kPack2+kPack3 && index < kPack1+kPack2+kPack3+kPack4)
    {
        pack = 3;
        index = index - (kPack1+kPack2+kPack3) + 1;
    }
    else if (index >= kPack1+kPack2+kPack3+kPack4 && index < kPack1+kPack2+kPack3+kPack4+kPack5)
    {
        pack = 4;
        index = index - (kPack1+kPack2+kPack3+kPack4) + 1;
    }
    else if (index >= kPack1+kPack2+kPack3+kPack4+kPack5 && index < kPack1+kPack2+kPack3+kPack4+kPack5+kPack6)
    {
        pack = 5;
        index = index - (kPack1+kPack2+kPack3+kPack4+kPack5) + 1;
    }
    
    imageName = [NSString stringWithFormat:@"Pack%zdQuotes_%zd.png", pack, index];
    
    return imageName;
}

- (UIColor *)getColour:(NSString *)imageName
{
    NSString *packString = [imageName substringWithRange:NSMakeRange(4, 1)];
    NSInteger pack = [packString integerValue];
    
    UIColor *colour = [UIColor darkGrayColor];
    
    if ([purchases containsObject:[NSNumber numberWithInteger:pack]])
    {
        colour = [UIColor whiteColor];
    }
    
    return colour;
}

#pragma mark -- Purchases
- (BOOL)isProductPurchased:(NSString *)productIdentifier
{
    NSLog(@"Checking IAP Product: %@", productIdentifier);
    NSUserDefaults *userDefault = [[NSUserDefaults alloc] initWithSuiteName:kEntitlement];
    
    return [userDefault boolForKey:productIdentifier];
}

- (BOOL)isPurchased:(NSString *)imageName
{
    NSString *packString = [imageName substringWithRange:NSMakeRange(4, 1)];
    NSInteger pack = [packString integerValue];
    
    return [purchases containsObject:[NSNumber numberWithInteger:pack]];
}

- (void)initPurchases
{
    // Init once to use NSMutableArray instead
    purchases = [[NSMutableArray alloc] init];
    
    NSInteger quotesPacks = 6;
    NSString *key;
    
    for (NSInteger i = 0; i < quotesPacks; i++)
    {
        key = [NSString stringWithFormat:@"%@.Quotes%zd", kBundleIdentifier, i];
        if ([self isProductPurchased:key]) {
            [purchases addObject:[NSNumber numberWithInteger:i]];
        }
    }    
}

#pragma mark -- Swap button
- (IBAction)swapView:(id)sender
{
    if ([_quotes isHidden])
    {
        [_quotes setHidden:NO];
        [_colours setHidden:YES];
        [_swap setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:212.0/255.0 blue:64.0/255.0 alpha:1.0]];
        [_swap setBackgroundImage:nil forState:UIControlStateNormal];
    }
    else
    {
        [_quotes setHidden:YES];
        [_colours setHidden:NO];
        [_swap setBackgroundColor:[UIColor clearColor]];
        [_swap setBackgroundImage:[UIImage imageNamed:@"Pack0Quotes_1.png"] forState:UIControlStateNormal];
    }
}

#pragma mark -- Sticker Delegate
- (void)removeFromStickerArray
{
    selectedSticker = nil;
}

#pragma mark -- Photo
- (UIImage *)gimmeMyInstaPictureImage
{
    UIImage *image;
    UIImageView *savePhoto = [[UIImageView alloc] initWithImage:fullSizePhoto];
    CGFloat scaleX = savePhoto.frame.size.width/photo.frame.size.width;
    CGFloat scaleY = savePhoto.frame.size.height/photo.frame.size.height;
    CGFloat x,y,width,height;
    
    x = selectedSticker.frame.origin.x * scaleX;
    y = selectedSticker.frame.origin.y * scaleY;
    width = selectedSticker.frame.size.width * scaleX;
    height = selectedSticker.frame.size.height * scaleY;
    [selectedSticker removeGestures];
    [selectedSticker setFrame:CGRectMake(x , y , width, height)];
    
    
    [savePhoto addSubview:selectedSticker];
    UIGraphicsBeginImageContextWithOptions(savePhoto.bounds.size, NO, 1.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [savePhoto.layer renderInContext:context];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
